#!/bin/bash

rel="1.1"
arch="linux64"
dir="qfis-$rel-$arch"

mkdir $dir
cp -r bin/ COPYING README examples/ prelude/ ./$dir/
mkdir ./$dir/grammar
cp grammar/qfis.lang ./$dir/grammar/
tar cvf - $dir | gzip -9 > "$dir.tgz"
rm -rf "$dir/"
echo "Distribution in $dir.tgz"

