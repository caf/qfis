#!/bin/bash

# build qfis
ec -batch -config qfis.ecf -finalize -c_compile

# move executable in current directory
mv EIFGENs/qfis/F_code/qfis ./bin/
# clean up
rm -rf EIFGENs/

