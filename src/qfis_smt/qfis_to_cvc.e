-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_TO_CVC

inherit
	QFIS_ITERATOR
	redefine process_and_formula, process_arguments, process_arithmetic_eq, process_arithmetic_gt,
		process_arithmetic_gte, process_arithmetic_lt, process_arithmetic_lte, process_arithmetic_neq,
		process_boolean_constant, process_boolean_variable, process_cat_compound, process_constant, process_divide_compound, process_exist_formula,
		process_forall_formula, process_function, process_iff_formula, process_implies_formula, process_integer_function, process_integer_variable,
		process_length, process_quantified_formula, process_position_integer_function, process_minus_compound, process_not_formula, process_or_formula,
		process_plus_compound, process_predicate_formula, process_range, process_sequence_empty, process_sequence_eq,
		process_sequence_neq, process_sequence_singleton, process_times_compound, process_variable
	end
	QFIS_CVC_NAMES

create
	make

feature  -- Result

	formula: STRING
		-- Translation into CVC form


feature  -- Node processing

	process_and_formula (el: QFIS_AND_FORMULA)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" AND ")
			el.right.process (Current)
			formula.append (")")
		end

	process_arguments (el: QFIS_ARGUMENTS)
		do
			from el.start
			until el.after
			loop
				el.item.process (Current)
				if not el.islast then
					formula.append (", ")
				end
				el.forth
			end
		end

	process_arithmetic_eq (el: QFIS_ARITHMETIC_EQ)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" = ")
			el.right.process (Current)
			formula.append (")")
		end

	process_arithmetic_gt (el: QFIS_ARITHMETIC_GT)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" > ")
			el.right.process (Current)
			formula.append (")")
		end

	process_arithmetic_gte (el: QFIS_ARITHMETIC_GTE)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" >= ")
			el.right.process (Current)
			formula.append (")")
		end

	process_arithmetic_lt (el: QFIS_ARITHMETIC_LT)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" < ")
			el.right.process (Current)
			formula.append (")")
		end

	process_arithmetic_lte (el: QFIS_ARITHMETIC_LTE)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" <= ")
			el.right.process (Current)
			formula.append (")")
		end

	process_arithmetic_neq (el: QFIS_ARITHMETIC_NEQ)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" /= ")
			el.right.process (Current)
			formula.append (")")
		end

	process_boolean_constant (el: QFIS_BOOLEAN_CONSTANT)
		do
			if el.constant then
				formula.append ("TRUE")
			else
				formula.append ("FALSE")
			end
		end

	process_boolean_variable (el: QFIS_BOOLEAN_VARIABLE)
		do
			formula.append (el.identifier)
		end

	process_cat_compound (el: QFIS_CAT_COMPOUND)
		do
			formula.append (cat_f)
			formula.append ("(")
			el.left.process (Current)
			formula.append (", ")
			el.right.process (Current)
			formula.append (")")
		end

	process_constant (el: QFIS_CONSTANT)
		do
			formula.append (el.value.out)
		end

	process_divide_compound (el: QFIS_DIVIDE_COMPOUND)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" / ")
			el.right.process (Current)
			formula.append (")")
		end

	process_exist_formula (el: QFIS_EXIST_FORMULA)
		do
			formula.append ("(EXIST ")
			Precursor (el)
			formula.append (")")
		end

	process_forall_formula (el: QFIS_FORALL_FORMULA)
		do
			formula.append ("(FORALL ")
			Precursor (el)
			formula.append (")")
		end

	process_function (el: QFIS_FUNCTION)
		do
			formula.append (el.function)
			formula.append ("(")
			el.arguments.process (Current)
			formula.append (")")
		end

	process_iff_formula (el: QFIS_IFF_FORMULA)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" <=> ")
			el.right.process (Current)
			formula.append (")")
		end

	process_implies_formula (el: QFIS_IMPLIES_FORMULA)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" => ")
			el.right.process (Current)
			formula.append (")")
		end

	process_integer_function (el: QFIS_INTEGER_FUNCTION)
		local pos: INTEGER
		do
			if el.is_position then
				pos := el.position
				if pos > 0 then
					formula.append (position_f)
				else
					formula.append (negposition_f)
					pos := -1 * pos
				end
				formula.append (pos.out)
				formula.append ("(")
				formula.append (el.function)
				formula.append (")")
			else
				formula.append (el.function)
				formula.append ("(")
				el.arguments.process (Current)
				formula.append (")")
			end
		end

	process_integer_variable (el: QFIS_INTEGER_VARIABLE)
		do
			formula.append (el.identifier)
		end

	process_length (el: QFIS_LENGTH)
		do
			formula.append (length_f)
			formula.append ("(")
			el.target.process (Current)
			formula.append (")")
		end

	process_minus_compound (el: QFIS_MINUS_COMPOUND)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" - ")
			el.right.process (Current)
			formula.append (")")
		end

	process_not_formula (el: QFIS_NOT_FORMULA)
		do
			formula.append ("(")
			formula.append ("NOT ")
			el.target.process (Current)
			formula.append (")")
		end

	process_or_formula (el: QFIS_OR_FORMULA)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" OR ")
			el.right.process (Current)
			formula.append (")")
		end

	process_position_integer_function (el: QFIS_POSITION_INTEGER_FUNCTION)
		local pos: INTEGER
		do
			pos := el.position
			if pos > 0 then
				formula.append (position_f)
			else
				formula.append (negposition_f)
				pos := -1 * pos
			end
			formula.append (pos.out)
			formula.append ("(")
			el.target.process (Current)
			formula.append (")")
		end

	process_plus_compound (el: QFIS_PLUS_COMPOUND)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" + ")
			el.right.process (Current)
			formula.append (")")
		end

	process_predicate_formula (el: QFIS_PREDICATE_FORMULA)
		do
			formula.append (el.predicate)
			formula.append ("(")
			el.arguments.process (Current)
			formula.append (")")
		end

	process_quantified_formula (el: QFIS_QUANTIFIED_FORMULA)
		local
			d: QFIS_LIST_VARIABLES
			t: LINKED_LIST [STRING]
		do
			formula.append ("(")
			d := el.locals.ordered
			t := el.locals.types_list
			from
				d.start
				t.start
			until
				d.after and t.after
			loop
				if not d.isfirst then
					formula.append (", ")
				end
				formula.append (d.item)
				formula.append (": ")
				if t.item.is_equal ("SEQUENCE") then
					formula.append (type_name (sequence_type))
				elseif t.item.is_equal ("INTEGER") then
					formula.append (type_name (integer_type))
				elseif t.item.is_equal ("BOOLEAN") then
					formula.append (type_name (formula_type))
				end
				d.forth
				t.forth
			end
			formula.append ("): (")
			el.target.process (Current)
			formula.append (")")
		end

	process_range (el: QFIS_RANGE)
		local
			f: QFIS_INTEGER_FUNCTION
			pos: INTEGER
			r: STRING
		do
			if el.is_range_position then
				pos := el.position
				if pos > 0 then
					formula.append (position_f)
				else
					formula.append (negposition_f)
					pos := -1 * pos
				end
				formula.append (pos.out)
				formula.append ("(")
				el.sequence.process (Current)
				formula.append (")")
			else
				if supported_range (el.lower, el.upper) then
					formula.append (range_of (el.lower, el.upper))
					formula.append ("(")
					el.sequence.process (Current)
					formula.append (")")
				else
					-- Unsupported range
					formula.append ("TRUE")
				end
			end
		end

	process_sequence_empty (el: QFIS_SEQUENCE_EMPTY)
		do
			formula.append (empty_sequence)
		end

	process_sequence_eq (el: QFIS_SEQUENCE_EQ)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" = ")
			el.right.process (Current)
			formula.append (")")
		end

	process_sequence_neq (el: QFIS_SEQUENCE_NEQ)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" /= ")
			el.right.process (Current)
			formula.append (")")
		end

	process_sequence_singleton (el: QFIS_SEQUENCE_SINGLETON)
		do
			formula.append (singleton_pre)
			el.target.process (Current)
			formula.append (singleton_post)
		end

	process_times_compound (el: QFIS_TIMES_COMPOUND)
		do
			formula.append ("(")
			el.left.process (Current)
			formula.append (" * ")
			el.right.process (Current)
			formula.append (")")
		end

	process_variable (el: QFIS_VARIABLE)
		do
			formula.append (el.identifier)
		end



feature {NONE} -- Initialization

	make
		do
			create formula.make_empty
		end

end
