-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_CONTEXT_BUILDER

inherit
	QFIS_ITERATOR
	redefine process_boolean_variable, process_cat_compound, process_function, process_integer_function, process_length,
				process_position_integer_function, process_integer_variable, process_predicate_formula, process_quantified_formula,
				process_range, process_variable end
	QFIS_CVC_NAMES

create
	make


feature -- Attributes

	context: QFIS_CONTEXT


feature -- Node processing

	process_boolean_variable (el: QFIS_BOOLEAN_VARIABLE)
		do
			Precursor (el)
			context.add_boolean_variable (el)
			check_error (el.as_text)
		end

	process_cat_compound (el: QFIS_CAT_COMPOUND)
		local args: QFIS_ARGUMENTS
		do
			Precursor (el)
			create args.make_empty
			args.extend (el.left)
			args.extend (el.right)
			context.add_sequence_function (create {QFIS_FUNCTION}.make (cat_f, args))
			check_error (el.as_text)
		end

	process_function (el: QFIS_FUNCTION)
		do
			Precursor (el)
			context.add_sequence_function (el)
			check_error (el.as_text)
		end

	process_integer_function (el: QFIS_INTEGER_FUNCTION)
		local v: QFIS_VARIABLE
		do
			Precursor (el)
			if el.is_position then
				context.add_position (el.position)
				process_variable (create {QFIS_VARIABLE}.make (el.function))
			else
				context.add_integer_function (el)
			end
			check_error (el.as_text)
		end

	process_integer_variable (el: QFIS_INTEGER_VARIABLE)
		do
			Precursor (el)
			context.add_integer_variable (el)
			check_error (el.as_text)
		end

	process_length (el: QFIS_LENGTH)
		local args: QFIS_ARGUMENTS
		do
			Precursor (el)
			create args.make_empty
			args.extend (el.target)
			process_integer_function (create {QFIS_INTEGER_FUNCTION}.make (length_f, args))
		end

	process_position_integer_function (el: QFIS_POSITION_INTEGER_FUNCTION)
		do
			Precursor (el)
			context.add_position (el.position)
			check_error (el.as_text)
		end

	process_predicate_formula (el: QFIS_PREDICATE_FORMULA)
		do
			Precursor (el)
			context.add_predicate (el)
			check_error (el.as_text)
		end

	process_quantified_formula (el: QFIS_QUANTIFIED_FORMULA)
		local d: QFIS_LIST_VARIABLES
		do
			Precursor (el)
			d := el.locals.ordered
			from d.start
			until d.after
			loop
				context.functions.remove (d.item)
				d.forth
			end
		end

	process_range (el: QFIS_RANGE)
		do
			Precursor (el)
			if el.is_range_position then
				context.add_position (el.position)
			elseif supported_range (el.lower, el.upper) then
				context.add_range (el)
			else
				context.errors.extend ("Range with mixed values is unsupported: " + el.as_text)
			end
			check_error (el.as_text)
		end

	process_variable (el: QFIS_VARIABLE)
		do
			Precursor (el)
			context.add_sequence_variable (el)
			check_error (el.as_text)
		end


feature  -- Utilities



feature {NONE}

	make (c: QFIS_CONTEXT)
		require attached c
		do
			context := c
		end


	check_error (node_as_text: STRING)
		do
			if context.typecheck_error then
				context.errors.extend ("Error when building context for node: " + node_as_text)
				context.reset
			end
		end

end
