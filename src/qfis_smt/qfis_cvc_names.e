-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_CVC_NAMES


feature

	sequence_type: INTEGER = 0
	formula_type: INTEGER = 1
	integer_type: INTEGER = 2


	type_name (code: INTEGER): STRING
		do
			if code = sequence_type then
				Result := list_t
			elseif code = formula_type then
				Result := "BOOLEAN"
			elseif code = integer_type then
				Result := "INT"
			else
				Result := "TYPE"
			end
		end

	list_t: STRING = "IntList"

	cat_f: STRING = "cat"

	length_f: STRING = "length"

	range_f: STRING = "range"
	negrange_f: STRING = "range_bottom"
	range_of (lower, upper: INTEGER): STRING
		require supported_range (lower, upper)
		local l, u: INTEGER
		do
			if lower > 0 and (upper = 0 or upper >= lower) then
				Result := range_f + lower.out + "_" + upper.out
			else
				check lower <= 0 and upper <= lower end
				l := -1 * lower
				u := -1 * upper
				Result := negrange_f + l.out + "_" + u.out
			end
		end

	position_f: STRING = "at"

	negposition_f: STRING = "at_bottom"

	empty_sequence: STRING = "nil"
	car_f: STRING = "head"
	cdr_f: STRING = "tail"


	singleton_pre: STRING = "cons("
	singleton_post: STRING
		once
			Result := ", " + empty_sequence + ")"
		end

	is_cvc_filename (filename: STRING): BOOLEAN
		do
			Result := filename.ends_with (".cvc")
		end


	default_filename: STRING = "./__qfis_solver_temp.cvc"
	default_filename_prefix: STRING = "__qfis_temp"

	cvc_commandline: STRING = "cvc3"
	cvc_timeout_flag: STRING = "-stimeout"

feature -- Utilities

	supported_range (lower, upper: INTEGER): BOOLEAN
		-- Is the range <<lower:upper>> supported?
		do
			Result := (lower > 0 and (upper = 0 or upper >= lower)) or (lower <= 0 and upper <= lower)
		end

	declarations_in_file (f: PLAIN_TEXT_FILE): HASH_TABLE [LINKED_LIST [INTEGER], STRING]
		-- Set of declarations in `f'
		-- Currently, relies on the filename
		-- TODO: parse cvc files for declarations
		require attached f
		local
			filename: STRING
			name_head: STRING
			a_token: STRING
			tokens: LINKED_LIST [STRING]
			numbers: LINKED_LIST [INTEGER]
			name: STRING
			p, old_p: INTEGER
		do
			create Result.make (1)
			name_head :=  "declaration_"
			create filename.make_from_string (f.name)
			-- Remove the leading path
			filename.keep_tail (filename.count - filename.last_index_of ('/', filename.count))
			if filename.starts_with (name_head) then
				filename.keep_tail (filename.count - name_head.count)
				create tokens.make
				from
					p := filename.index_of ('_', 1)
					old_p := 0
				until p = 0
				loop
					a_token := filename.substring (old_p + 1, p - 1)
					if a_token.count > 0 then
						tokens.extend (a_token)
					end
					old_p := p
					p := filename.index_of ('_', old_p + 1)
				end
				if not tokens.is_empty and then not tokens.first.is_integer then
					name := tokens.first
					tokens.start ; tokens.remove
					create numbers.make
					from tokens.start
					until tokens.after
					loop
						if tokens.item.is_integer then
							numbers.extend (tokens.item.to_integer)
						end
						tokens.forth
					end
					if not numbers.is_empty then
						Result.put (numbers, name)
					end
				end
			end
		ensure attached Result
		end


	valid_output: STRING = "Valid."
	unknown_output: STRING = "Unknown."
	invalid_output: STRING = "Invalid."

	successful_output (s: STRING): INTEGER
		-- -1 if `s' represents a completely successful output of CVC3
		-- 0 if CVC3 presents no output
		-- i > 0 if the i-th result (i-th query) denotes a failure
		local
			l: LIST [STRING]
			cur_line: STRING
			failure: BOOLEAN
		do
			l := s.split ('%N')
			Result := 0
			failure := False
			from l.start
			until l.after or failure
			loop
				cur_line := l.item
				cur_line.left_adjust
				cur_line.right_adjust
				if cur_line.is_equal (unknown_output) or cur_line.is_equal (invalid_output) then
					Result := Result + 1
					failure := True
				elseif cur_line.is_equal (valid_output) then
					Result := Result + 1
				end
				l.forth
			end
			if not failure and Result > 0 then
				Result := -1
			end
		ensure
			Result >= -1
		end


	normalized_name (s: STRING): STRING
		-- Normalized version for `s'
		do
			Result := s
		end

end
