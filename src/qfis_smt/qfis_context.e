-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_CONTEXT

inherit
	QFIS_CVC_NAMES

create
	make

feature  -- Attributes

	functions: HASH_TABLE [LINKED_LIST [INTEGER], STRING]
		-- Function names --> Codomain x Arg1_type x Arg2_type x ...
		-- A variable is a function with no arguments
		-- A predicate is a variable with boolean codomain

	positions: LINKED_SET [INTEGER]
		-- Elements at positions p1, p2, ...

	ranges: LINKED_SET [TUPLE[INTEGER, INTEGER]]
		-- Ranges for [l1, u1], [l2, u2], ...

	typecheck_error: BOOLEAN

	errors: LINKED_LIST [STRING]
		-- Errors encountered in generating the context

	has_errors: BOOLEAN
		do
			Result := errors.count > 0
		end


feature  -- Modification

	add_boolean_variable (v: QFIS_BOOLEAN_VARIABLE)
		-- Ensure `functions' includes data for `v'
		local args: LINKED_LIST [INTEGER]
		do
			create args.make
			args.compare_objects
			args.extend (formula_type)
			if functions.has (v.identifier) then
				if not functions.at (v.identifier).is_equal (args) then
					typecheck_error := True
				end
			else
				functions.extend (args, v.identifier)
			end
		ensure
			functions.has (v.identifier)
		end

	add_predicate (p: QFIS_PREDICATE_FORMULA)
		-- Ensure `functions' includes data for `p'
		-- Set `typecheck_error' if a predicate with the same name and a different signature exists
		local
			args: LINKED_LIST [INTEGER]
		do
			args := parse_arguments (p.arguments)
			args.put_front (formula_type)
			check attached args end
			if functions.has (p.predicate) then
				if not functions.at (p.predicate).is_equal (args) then
					typecheck_error := True
				end
			else
				functions.extend (args, p.predicate)
			end
		ensure
			functions.has (p.predicate)
		end


	add_integer_function (f: QFIS_INTEGER_FUNCTION)
		-- Ensure `functions' includes data for `f'
		-- Set `typecheck_error' if a function with the same name and a different signature exists
		local
			args: LINKED_LIST [INTEGER]
		do
			args := parse_arguments (f.arguments)
			args.put_front (integer_type)
			if functions.has (f.function) then
				if not functions.at (f.function).is_equal (args) then
					typecheck_error := True
				end
			else
				functions.extend (args, f.function)
			end
		ensure
			functions.has (f.function)
		end


	add_sequence_function (f: QFIS_FUNCTION)
		-- Ensure `functions' includes data for `f'
		-- Set `typecheck_error' if a function with the same name and a different signature exists
		local
			args: LINKED_LIST [INTEGER]
		do
			args := parse_arguments (f.arguments)
			args.put_front (sequence_type)
			if functions.has (f.function) then
				if not functions.at (f.function).is_equal (args) then
					typecheck_error := True
				end
			else
				functions.extend (args, f.function)
			end
		ensure
			functions.has (f.function)
		end


	add_position (p: INTEGER)
		-- Ensure `positions' includes `p'
		do
			positions.extend (p)
		end


	add_range (r: QFIS_RANGE)
		-- Ensure `ranges' includes data for `r'
		local t: TUPLE [INTEGER, INTEGER]
		do
			create t
			t.put (r.lower, 1)
			t.put (r.upper, 2)
			ranges.extend (t)
		end


	add_integer_variable (v: QFIS_INTEGER_VARIABLE)
		-- Ensure `functions' includes data for `v'
		local args: LINKED_LIST [INTEGER]
		do
			create args.make
			args.extend (integer_type)
			if functions.has (v.identifier) then
				if not functions.at (v.identifier).is_equal (args) then
					typecheck_error := True
				end
			else
				functions.extend (args, v.identifier)
			end
		ensure
			functions.has (v.identifier)
		end


	add_sequence_variable (v: QFIS_VARIABLE)
		-- Ensure `functions' includes data for `v'
		local args: LINKED_LIST [INTEGER]
		do
			create args.make
			args.compare_objects
			args.extend (sequence_type)
			if functions.has (v.identifier) then
				if not functions.at (v.identifier).is_equal (args) then
					typecheck_error := True
				end
			else
				functions.extend (args, v.identifier)
			end
		ensure
			functions.has (v.identifier)
		end


	reset
		require typecheck_error
		do
			typecheck_error := False
		ensure not typecheck_error
		end


feature {NONE} -- Initialization

	make
		do
			create functions.make (0)
			create positions.make
			positions.compare_objects
			create ranges.make
			ranges.compare_objects
			typecheck_error := False
			create errors.make
		end


feature {NONE} -- Implementation

	parse_arguments (args: QFIS_ARGUMENTS): LINKED_LIST [INTEGER]
		do
			create Result.make
			from args.start
			until args.after
			loop
				if attached {QFIS_FORMULA} args.item then
					Result.extend (formula_type)
				elseif attached {QFIS_INTEGER_TERM} args.item then
					Result.extend (integer_type)
				elseif attached {QFIS_SEQUENCE} args.item then
					Result.extend (sequence_type)
				else
					-- Argument type unrecognized
					errors.extend ("Argument " + args.item.as_text + ": unrecognized type.")
				end
				args.forth
			end
		end

end
