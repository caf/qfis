-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_PREAMBLE

inherit
	QFIS_CVC_NAMES

create
	make_with_context


feature  -- Main interface

	preamble: STRING

	build_preamble
		local
			fun: ARRAYED_LIST [STRING]
			ll: LINKED_LIST [INTEGER]
		do
			generate_list_datatype
			-- Generate "special" functions first
			create fun.make_from_array (context.functions.current_keys)
			fun.compare_objects
			if not fun.has (cat_f) and fun.has (length_f) then
				-- The length function requires the cat function
				create ll.make
				ll.extend (sequence_type)
				ll.extend (sequence_type)
				ll.extend (sequence_type)
				context.functions.extend (ll, cat_f)
				fun.extend (cat_f)
			end
			if fun.has (cat_f) then
				generate_function (cat_f, context.functions.at (cat_f))
				fun.prune_all (cat_f)
			end
			if fun.has (length_f) then
				generate_function (length_f, context.functions.at (length_f))
				fun.prune_all (length_f)
			end

			-- Generate positions
			from context.positions.start
			until context.positions.after
			loop
				generate_position (context.positions.item)
				context.positions.forth
			end

			-- Generate ranges
			from context.ranges.start
			until context.ranges.after
			loop
				if attached {TUPLE [INTEGER, INTEGER]} context.ranges.item as r then
					if attached {INTEGER} r.at (1) as l then
						if attached {INTEGER} r.at (2) as u then
							generate_range (l, u)
						end
					end
				end
				context.ranges.forth
			end

			-- Generate all remaining functions
			from fun.start
			until fun.after
			loop
				check context.functions.has (fun.item) end
				generate_function (fun.item, context.functions.at (fun.item))
				fun.forth
			end
		end


feature  -- Commands

	generate_list_datatype
		-- Generate a declaration for a list datatype with the current names and add it to `preamble'
		do
			add_declaration ("DATATYPE " + list_t + " = " + empty_sequence + " | " +
								singleton_pre + car_f + ": " + "INT" + ", " + cdr_f + ": " + list_t + ")" + " END")
		end

	generate_position (p: INTEGER)
		-- Generate declaration and axioms for position `p' and add them to `preamble'
		local
			sig, ff,ff_q: STRING
			axioms: LINKED_LIST [STRING]
			pp, q: INTEGER
		do
			if not positions_generated.has (p) then
				positions_generated.extend (p)
				create axioms.make
				if p > 0 then
					pp := p
					ff := position_f + pp.out
					axioms.extend ("FORALL (_x: " + list_t + "): " + length_gte ("_x", pp) + " => " + ff + "(_x)" + " = " + car_f + "(" + nth_tail("_x", pp - 1) + ")")
					if context.functions.has (cat_f) then
						axioms.extend ("FORALL (_x, _y: " + list_t + "): " + length_gte ("_x", pp) + " => " + ff + "(" + cat_f + "(_x, _y)" + ")" + " = " + ff + "(_x)")
						from q := 1
						until q = pp
						loop
							if not context.positions.has (q) then
								-- Definition depends on other definition
								generate_position (q)
							end
							axioms.extend ("FORALL (_x, _y: " + list_t + "): " +
								length_eq ("_x", pp - q) + " AND " + length_gte ("_y", q) +
									" => " + ff + "(" + cat_f + "(_x, _y)" + ")" + " = " + position_f + q.out + "(_y)")
							q := q + 1
						end
					end
				else
					pp := -1 * p
					ff := negposition_f + pp.out
					axioms.extend ("FORALL (_x: " + list_t + "): " + length_eq ("_x", pp+1) + " => " + ff + "(_x)" + " = " + car_f + "(_x)")
					axioms.extend ("FORALL (_x: " + list_t + "): " + length_gte ("_x", pp+2) + " => " + ff + "(_x)" + " = " + ff + "(" + cdr_f + "(_x)" + ")")
					if context.functions.has (cat_f) then
						axioms.extend ("FORALL (_x, _y: " + list_t + "): " + length_gte ("_y", pp+1) + " => " + ff + "(" + cat_f + "(_x, _y)" + ")" + " = " + ff + "(_y)")
						from q := 0
						until q = pp
						loop
							if q > 0 then
								ff_q := position_f + q.out
							else
								ff_q := negposition_f + q.out

							end
							if not context.positions.has (-1 * q) then
								-- Definition depends on other definition
								generate_position (-1 * q)
							end
							axioms.extend ("FORALL (_x, _y: " + list_t + "): " +
								length_gte ("_x", q + 1) + " AND " + length_eq ("_y", pp - q) +
									" => " + ff + "(" + cat_f + "(_x, _y)" + ")" + " = " + ff_q + "(_x)")
							q := q + 1
						end
					end
				end
				sig := ff + ": " + list_t + " -> " + "INT"
				add_declaration (sig)
				from axioms.start
				until axioms.after
				loop
					add_assert (axioms.item)
					axioms.forth
				end
			end
		end


	generate_range (lower, upper: INTEGER)
		-- Generate declaration and axioms for range `[l, u]' and add them to `preamble'
		require supported_range (lower, upper)
		local
			sig, ff: STRING
			axioms: LINKED_LIST [STRING]
			l, u: INTEGER
		do
			ff := range_of (lower, upper)

			create axioms.make
			if lower > 0 and upper = 0 then
				l := lower
				u := upper
				axioms.extend ("FORALL (_x: " + list_t + "): " +
					length_lte ("_x", l - 1) + " => " + ff + "(_x)" + " = " + empty_sequence)
				axioms.extend ("FORALL (_x: " + list_t + "): " +
					length_gte ("_x", l) + " => " + ff + "(_x)" + " = " + nth_tail ("_x", l - 1))
			elseif 0 < lower and lower < upper then
				l := lower
				u := upper
				axioms.extend ("FORALL (_x: " + list_t + "): " +
					length_lte ("_x", u - 1) + " => " + ff + "(_x)" + " = " + empty_sequence)
				axioms.extend ("FORALL (_x: " + list_t + "): " +
					length_gte ("_x", u) + " => " + ff + "(_x)" + " = " + subsequence ("_x", l, u))
			elseif upper < lower and lower <= 0 then
				l := -1 * lower
				u := -1 * upper
				axioms.extend ("FORALL (_x: " + list_t + "): " +
					length_lte ("_x", u) + " => " + ff + "(_x)" + " = " + empty_sequence)
				axioms.extend ("FORALL (_x: " + list_t + "): " +
					length_eq ("_x", u + 1) + " => " + ff + "(_x)" + " = " + subsequence ("_x", 1, u - l + 1))
				axioms.extend ("FORALL (_x: " + list_t + "): " +
					length_gte ("_x", u + 2) + " => " + ff + "(_x)" + " = " + ff + "(" + nth_tail ("_x", 1) + ")")
			else
				context.errors.extend ("Range <<" + lower.out + ":" + upper.out + ">>" + " is not supported.")
				debug
					check False end
				end
			end

			sig := ff + ": " + list_t + " -> " + list_t
			add_declaration (sig)
			from axioms.start
			until axioms.after
			loop
				add_assert(axioms.item)
				axioms.forth
			end
		end

	generate_function (name: STRING; signature: LINKED_LIST [INTEGER])
		-- Generate declaration and axioms for function with given name and signature and add them to `preamble'
		require
			attached name and then not name.is_empty
			attached signature and then signature.count > 0
		local
			decl: STRING
		do
			if used_file_declarations.has (name) then
				if not included_files.has (used_file_declarations.at (name)) then
					add_declaration ("INCLUDE %"" + used_file_declarations.at (name) + "%"")
					included_files.extend (used_file_declarations.at (name))
				end
			else
				-- Generate declaration
				create decl.make_empty
				decl.append (name + ": ")
				if signature.count = 1 then
					-- Variable
					decl.append (type_name (signature.first))
				elseif signature.count = 2 then
					-- 1-argument function
					decl.append (type_name (signature.at (2)) + " -> " + type_name (signature.at (1)))
				else
					check signature.count > 2 end
					decl.append ("(")
					from signature.go_i_th (2)
					until signature.after
					loop
						decl.append (type_name (signature.item))
						if not signature.islast then
							decl.append (", ")
						end
						signature.forth
					end
					decl.append (")" + " -> " + type_name (signature.first))
				end
				add_declaration (decl)
			end
		end

	add_assert (formula: STRING)
		-- Add assertion for `formula' to `preamble'
		do
			preamble.append ("%N" + "ASSERT " + formula + ";")
		end

	add_query (formula: STRING)
		-- Add query for `formula' to `preamble'
		do
			preamble.append ("%N" + "QUERY " + formula + ";")
		end

	add_declaration (declaration: STRING)
		-- Add declaration for `declaration' to `preamble'
		do
			preamble.append ("%N" + declaration + ";")
		end



feature  {NONE} -- Implementation

	positions_generated: LINKED_SET [INTEGER]

	included_files: LINKED_SET [STRING]

	length_gte (v: STRING; n: INTEGER): STRING
		-- Formula for: variable `v' has at least `n' elements
		require n >= 0
		do
			if n = 0 then
				Result := "TRUE"
			else
				Result := length_upto (v, n, True)
			end
		end


	length_eq (v: STRING; n: INTEGER): STRING
		-- Formula for: variable `v' has exactly `n' elements
		require n >= 0
		do
			if n = 0 then
				create Result.make_from_string (v + " = " + empty_sequence)
			else
				Result := length_gte (v, n) + " AND " + nth_tail (v, n) + " = " + empty_sequence
			end
		end


	length_lte (v: STRING; n: INTEGER): STRING
		-- Formula for: variable `v' has at most `n' elements
		require n >= 0
		do
			if n = 0 then
				Result := v + " = " + empty_sequence
			else
				Result := length_upto (v, n + 1, False)
			end
		end


	length_upto (v: STRING; n: INTEGER; conjunction: BOOLEAN): STRING
		-- Formula for: `v' has 1 element connector `v' has 2 elements connector ... `v' has `n' elements
		-- connector is AND iff `conjunction'
		require n > 0
		local i: INTEGER
		do
			create Result.make_empty
			from i := 0
			until i = n
			loop
				Result.append (nth_tail(v, i))
				if conjunction then
					Result.append (" /= ")
				else
					Result.append (" = ")
				end
				Result.append (empty_sequence)
				if i < n - 1 then
					if conjunction then
						Result.append (" AND ")
					else
						Result.append (" OR ")
					end
				end
				i := i + 1
			end
		end



	nth_tail (v: STRING; n: INTEGER): STRING
		-- Term for: cdr_f(cdr_f(...(`v'))), `n' times
		require n >= 0
		do
			if n = 0 then
				create Result.make_from_string (v)
			else
				Result := cdr_f + "(" + nth_tail(v, n-1) + ")"
			end
		end


	subsequence (v: STRING; l, u: INTEGER): STRING
		-- Term for: v[l:u] with explicit enumeration
		require 0 < l and l <= u + 1
		do
			if l = u + 1 then
				Result := empty_sequence
			else
				Result := "cons(" + car_f + "(" + nth_tail (v, l - 1) + "), " + subsequence (v, l + 1, u) + ")"
			end
		end


feature -- Pre-defined theories

	used_file_declarations: HASH_TABLE [STRING, STRING]
		-- Maps function names -> (fully qualified) filenames with correct declaration


	load_declarations (path: STRING)
		local
			dir: DIRECTORY
			filename: STRING
			full_filename: STRING
			current_file: PLAIN_TEXT_FILE
			l_decl: HASH_TABLE [LINKED_LIST [INTEGER], STRING]
		do
			create dir.make (path)
			if dir.exists then
				dir.open_read
				if not dir.is_closed then
					from
						dir.start
						dir.readentry
					until dir.lastentry = Void
					loop
						filename := dir.lastentry
						if is_cvc_filename (filename) then
							if path.ends_with ("/") then
								full_filename := path + filename
							else
								full_filename := path + "/" + filename
							end
							create current_file.make (full_filename)
							if current_file.exists and then current_file.is_plain_text and then current_file.is_access_readable then
								l_decl := declarations_in_file (current_file)
								from l_decl.start
								until l_decl.after
								loop
									if context.functions.has (l_decl.key_for_iteration) then
										if not context.functions.at (l_decl.key_for_iteration).is_equal (l_decl.item_for_iteration) then
											-- Conflict
											context.errors.extend ("A declaration for function " + l_decl.key_for_iteration +
																	" in file " + current_file.name + " conflicts with the usage of " + l_decl.key_for_iteration +
																	" in the current context. Not using declaration on file.")
										else
											used_file_declarations.put (full_filename, l_decl.key_for_iteration)
										end
									end
									l_decl.forth
								end
							else
								context.errors.extend ("Could not read file: " + filename + " at " + path + ".")
							end
						end
						dir.readentry
					end
				else -- Cannot access directory
					context.errors.extend ("Could not load declarations from path: " + path + "  -- cannot access directory.")
				end
			else -- Directory does not exist
				context.errors.extend ("Could not load declarations from path: " + path + "  -- directory does not exist.")
			end
		end


	add_declaration_file (filename: STRING; function_name: STRING)
		-- Add `filename' as theory file for the function `function_name'
		local
			current_file: PLAIN_TEXT_FILE
		do
			create current_file.make (filename)
			if current_file.exists and then current_file.is_plain_text and then current_file.is_access_readable then
				used_file_declarations.put (filename, function_name)
			else
				context.errors.extend ("Could not read file: " + filename + ".")
			end
		end



feature {NONE} --Initialization

	context: QFIS_CONTEXT

	make_with_context (c: QFIS_CONTEXT)
		require attached c
		do
			context := c
			create preamble.make_empty
			create used_file_declarations.make (1)
			create included_files.make
			create positions_generated.make
		end

end
