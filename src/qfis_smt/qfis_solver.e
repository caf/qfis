-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_SOLVER

inherit
	QFIS_PREAMBLE
	export {QFIS_PREAMBLE} all ; {ANY} add_declaration_file end

create
	make


feature

	assert (formula: QFIS_FORMULA)
		require attached formula
		do
			add_to_context (formula)
			commands.append ("%N" + "ASSERT " + to_cvc (formula) + ";")
		end


	query (formula: QFIS_FORMULA)
		require attached formula
		do
			add_to_context (formula)
			commands.append ("%N" + "QUERY " + to_cvc (formula) + ";")
		end

	comment (str: STRING)
		require attached str
		do
			if not comments.is_empty then
				comments.extend ('%N')
			end
			comments.append ("%% " + str)
		end

	pop
		do
			commands.append ("%N" + "POP" + ";")
		end


	push
		do
			commands.append ("%N" + "PUSH" + ";")
		end


	solve
		local
			f: PLAIN_TEXT_FILE
			ph: PROCESS_HELPER
			cl, cvc_result: STRING
		do
			create cvc_file.make_empty
			if attached declarations_dir then
				load_declarations (declarations_dir)
			end
			build_preamble
			cvc_file.append (comments + "%N%N")
			cvc_file.append ("%%%%%%%%%% BEGIN PREAMBLE %%%%%%%%%%")
			cvc_file.append (preamble)
			cvc_file.append ("%N" + "%%%%%%%%%% END PREAMBLE %%%%%%%%%%")
			cvc_file.append ("%N%N")
			cvc_file.append (commands)

			if not attached work_file then
				work_file := default_filename
			end
			create f.make_open_write (work_file)
			if f.exists and f.is_open_write then
				f.put_string (cvc_file)
			else
				context.errors.extend ("Could not open work file at: " + work_file + ".")
			end
			f.close

			create ph
			if not {PLATFORM}.is_windows then
				cl := "/bin/sh -c %'" + cvc_commandline + " " + timeout_string + " " + work_file + "%'"
			else
				cl := cvc_commandline + " " + timeout_string + " " + work_file
			end
			cvc_result := ph.output_from_program (cl, Void)
			debug
				print ("%N%N" + "QFIS_SOLVER.solve -- output of  " + cl + "%N")
				print (cvc_result)
			end
			success := successful_output (cvc_result)
		end


	success: INTEGER


	set_declarations_dir (dir: STRING)
		require attached dir
		do
			declarations_dir := dir.twin
		end

	declarations_dir: STRING assign set_declarations_dir
		-- Directory where to search for useful declaration files


	set_work_file (filename: STRING)
		require attached filename
		do
			work_file := filename.twin
		end

	work_file: STRING assign set_work_file
		-- Filename where the CVC file solved is stored


	set_timeout (t: INTEGER)
		do
			if t <= 0 then
				timeout := 0
			else
				timeout := t*10
			end
		end

	timeout: INTEGER
		-- Stop `solve' for each query after `timeout' tenths of second
		-- If `t' <= 0, no timeout is applied



feature {NONE} -- Implementation

	commands: STRING

	comments: STRING

	timeout_string: STRING
		do
			if timeout = 0 then
				Result := ""
			else
				Result := cvc_timeout_flag + " " + timeout.out
			end
		end

	cvc_file: STRING

	add_to_context (formula: QFIS_FORMULA)
		-- Extend `context' so that it includes data about `formula'
		require attached formula
		local context_builder: QFIS_CONTEXT_BUILDER
		do
			create context_builder.make (context)
			formula.process (context_builder)
		end

	to_cvc (formula: QFIS_FORMULA): STRING
		-- CVC representation of `formula'
		require attached formula
		local to_cvc_iterator: QFIS_TO_CVC
		do
			create to_cvc_iterator.make
			formula.process (to_cvc_iterator)
			Result := to_cvc_iterator.formula
		end


feature {NONE} -- Initialization

	make
		do
			create context.make
			make_with_context (context)
			create commands.make_empty
			create comments.make_empty
			set_declarations_dir (".")
			set_timeout (10)
		end

end
