# Wrapper to be able to specify absolute paths to input files under Windows
# Call it with python.exe qfis-win.py [arguments as for qfis.exe]
import sys
import os, os.path
from subprocess import call

curdir = os.getcwd()

path = sys.argv[-1]
if len(sys.argv) > 1 and os.path.isfile(path):
    dir, fn = os.path.split(os.path.abspath(path))
    # cd dir
    os.chdir(dir)
    # qfis.exe [arguments] fn
    args = sys.argv[1:-1] + [fn]
else:
    # qfis.exe [arguments]
    args = sys.argv[1:]

call(["qfis.exe"] + args)
os.chdir(curdir)
