-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
  QFIS_OR_FORMULA

inherit
	QFIS_BOOLEAN_FORMULA
	redefine as_text, precedence, process end

create
	make

feature

	as_text: STRING
		do
			Result := left.as_text_parenthesized (Current) + " or " + right.as_text_parenthesized (Current)
		end

	precedence: INTEGER = 4

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_or_formula (Current)
		end

feature

	is_dnf: BOOLEAN
		do
			Result := left.is_dnf and right.is_dnf
		end

	is_disjunctive: BOOLEAN
		do
			Result := left.is_disjunctive and right.is_disjunctive
		end

	is_cnf: BOOLEAN
		do
			Result := is_disjunctive
		end

	is_conjunctive: BOOLEAN
		do
			Result := False
		end

feature {NONE}

	make (l, r: QFIS_FORMULA)
		do
			set (l, r)
		end
end
