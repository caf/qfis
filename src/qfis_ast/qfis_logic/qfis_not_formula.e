-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
  QFIS_NOT_FORMULA

inherit
	QFIS_FORMULA
	redefine as_text, precedence, process end

create
	make

feature

	target: QFIS_FORMULA

	set (t: QFIS_FORMULA)
		do
			target := t
		end

	as_text: STRING
		do
			Result := "not " + target.as_text_parenthesized (Current)
		end

	precedence: INTEGER = 8

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_not_formula (Current)
		end

feature

	is_quantifier_free: BOOLEAN
		do
			Result := target.is_quantifier_free
		end

	is_dnf: BOOLEAN
		do
			Result := False
		end

	is_disjunctive: BOOLEAN
		do
			Result := False
		end

	is_cnf: BOOLEAN
		do
			Result := False
		end

	is_conjunctive: BOOLEAN
		do
			Result := False
		end

feature {NONE}

	make (t: QFIS_FORMULA)
		do
			set (t)
		end

end
