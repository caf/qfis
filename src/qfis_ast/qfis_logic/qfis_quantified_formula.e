-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

deferred class
	QFIS_QUANTIFIED_FORMULA

inherit
	QFIS_FORMULA
	redefine as_text, precedence, process end

feature

	locals: QFIS_LIST_DECLARATIONS

	target: QFIS_FORMULA

	set (l: QFIS_LIST_DECLARATIONS; t: QFIS_FORMULA)
		do
			locals := l
			target := t
		end

	as_text: STRING
		do
			Result := "(" + locals.as_text + ")" + " " + "(" +  target.as_text + ")"
			Result := indented (Result, 0)
		end

	precedence: INTEGER = 3

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_quantified_formula (Current)
		end

feature

	is_quantifier_free: BOOLEAN
		do
			Result := False
		end

	is_dnf: BOOLEAN
		do
			Result := False
		end

	is_disjunctive: BOOLEAN
		do
			Result := False
		end

	is_cnf: BOOLEAN
		do
			Result := False
		end

	is_conjunctive: BOOLEAN
		do
			Result := False
		end

feature {NONE}

	make (l: QFIS_LIST_DECLARATIONS; t: QFIS_FORMULA)
		do
			set (l, t)
		end

end
