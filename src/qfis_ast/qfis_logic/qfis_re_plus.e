-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
  QFIS_RE_PLUS

inherit
	QFIS_RE_UNARY
	redefine as_text, precedence, process end

create
	make

feature

	as_text: STRING
		do
			Result := target.as_text_parenthesized (Current) + "**"
		end

	precedence: INTEGER = 28

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_re_plus (Current)
		end

feature {NONE}

	make (t: QFIS_REGEXP)
		do
			set (t)
		end

end
