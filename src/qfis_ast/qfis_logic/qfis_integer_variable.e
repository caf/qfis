-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_INTEGER_VARIABLE

inherit
	QFIS_INTEGER_TERM
	redefine as_text, process end

create
	make

feature

	identifier: STRING assign set

	set (id: STRING)
	  do
	  	identifier := id
	  end

	 as_text: STRING
	 	do
	 		Result := identifier
	 	end

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_integer_variable (Current)
		end

feature {NONE}

	make (id: STRING)
		do
			set (id)
		end

end
