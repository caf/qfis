-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

deferred class
  QFIS_FORMULA

inherit
	QFIS_PRINTABLE; QFIS_VISITABLE
	QFIS_ARGUMENT

feature
	label: STRING assign set_label

	set_label (a_label: STRING)
		do
			label := a_label
		end

	annotation: STRING assign set_annotation

	set_annotation (an_annotation: STRING)
		do
			annotation := an_annotation
		end

feature

	is_old: BOOLEAN

	set_old
		do
			is_old := True
		end

	unset_old
		do
			is_old := False
		end

feature -- Visitor

feature

	is_quantifier_free: BOOLEAN
		-- Is the formula without quantifiers?
		deferred end

	is_dnf: BOOLEAN
		-- Is the formula in disjunctive normal form?
		deferred end

	is_disjunctive: BOOLEAN
		-- Is the formula in purely disjunctive form?
		deferred end

	is_cnf: BOOLEAN
		-- Is the formula in conjunctive normal form?
		deferred end

	is_conjunctive: BOOLEAN
		-- Is the formula in purely conjunctive form?
		deferred end

end
