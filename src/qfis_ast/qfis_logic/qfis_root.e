-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_ROOT

inherit
	QFIS_FORMULA
	redefine as_text, precedence, process end

create
	make

feature

	root: QFIS_FORMULA

	set (r: QFIS_FORMULA)
		do
			root := r
		end

	precedence: INTEGER = -10

	as_text: STRING
		do
			Result := root.as_text
		end

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_root (Current)
		end

feature

	is_quantifier_free: BOOLEAN
		do
			Result := root.is_quantifier_free
		end

	is_dnf: BOOLEAN
		do
			Result := root.is_dnf
		end

	is_disjunctive: BOOLEAN
		do
			Result := root.is_disjunctive
		end

	is_cnf: BOOLEAN
		do
			Result := root.is_cnf
		end

	is_conjunctive: BOOLEAN
		do
			Result := root.is_conjunctive
		end


feature {NONE}

	make (r: QFIS_FORMULA)
		do
			set (r)
		end
end
