-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_SEQUENCE_SINGLETON

inherit
	QFIS_ATOMIC_SEQUENCE
	redefine as_text, process end

create
	make

feature

	target: QFIS_INTEGER_TERM

	set (a_value: QFIS_INTEGER_TERM)
	  do
	  	target := a_value
	  end

	 as_text: STRING
	 	do
	 		Result := target.as_text
	 	end

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_sequence_singleton (Current)
		end

feature {NONE}

	make (a_value: QFIS_INTEGER_TERM)
		do
			set (a_value)
		end


end
