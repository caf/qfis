-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

deferred class
  QFIS_BOOLEAN_FORMULA

inherit
	QFIS_FORMULA
	redefine process end

feature

	left, right: QFIS_FORMULA

	set (l, r: QFIS_FORMULA)
		do
			left := l
			right := r
		end

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_boolean_formula (Current)
		end

feature

	is_quantifier_free: BOOLEAN
		do
			Result := left.is_quantifier_free and right.is_quantifier_free
		end

end
