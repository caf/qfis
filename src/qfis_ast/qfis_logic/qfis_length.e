-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
  QFIS_LENGTH

inherit
	QFIS_INTEGER_TERM
	redefine as_text, precedence, process end

create
	make

feature

	target: QFIS_SEQUENCE

	set (a_target: QFIS_SEQUENCE)
		do
			target := a_target
		end

	as_text: STRING
		do
			Result := "|" + target.as_text + "|"
		end

	precedence: INTEGER = 11

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_length (Current)
		end


feature {NONE}

	make (a_target: QFIS_SEQUENCE)
		do
			set (a_target)
		end

end
