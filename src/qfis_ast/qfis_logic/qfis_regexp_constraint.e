-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
  QFIS_REGEXP_CONSTRAINT

inherit
	QFIS_ATOMIC_FORMULA
	redefine as_text, precedence, process end

create
	make

feature

	sequence: QFIS_SEQUENCE

	regexp: QFIS_REGEXP

	in: BOOLEAN
		-- Is `sequence' constrained to be in `regexp'?
		-- (If False, `sequence' must be not in `regexp')

	set (s: QFIS_SEQUENCE ; r: QFIS_REGEXP ; i: BOOLEAN)
		do
			sequence := s
			regexp := r
			in := i
		end

	as_text: STRING
		local
			connective: STRING
		do
			if in then
				connective := " in "
			else
				connective := " not in "
			end
			Result := sequence.as_text + connective + "{" + regexp.as_text + "}"
		end

	precedence: INTEGER = 18

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_regexp_constraint (Current)
		end

feature

	is_dnf: BOOLEAN
		do
			Result := True
		end

	is_disjunctive: BOOLEAN
		do
			Result := True
		end

	is_cnf: BOOLEAN
		do
			Result := True
		end

	is_conjunctive: BOOLEAN
		do
			Result := True
		end

feature {NONE}

	make (s: QFIS_SEQUENCE ; r: QFIS_REGEXP ; i: BOOLEAN)
		do
			set (s, r, i)
		end

end
