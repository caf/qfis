-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
  QFIS_RANGE

inherit
	QFIS_DERIVED_SEQUENCE
	redefine as_text, precedence, process end

create
	make

feature

	sequence: QFIS_SEQUENCE

	lower, upper: INTEGER

	set (s: QFIS_SEQUENCE ; l, u: INTEGER)
		do
			sequence := s
			lower := l
			upper := u
		end

	as_text: STRING
		do
			Result := sequence.as_text + "<<" + lower.out + ":" + upper.out + ">>"
		end

	precedence: INTEGER = 16

feature

	is_range_position: BOOLEAN
		-- Is `Current' of the form seq <<i, i>> for some sequence seq?
		-- If True, `position' stores `i'
		do
			if lower = upper then
				Result := True
				position := lower
			else
				Result := False
			end
		end

	position: INTEGER

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_range (Current)
		end

feature {NONE}

	make (s: QFIS_SEQUENCE ; l, u: INTEGER)
		do
			set (s, l, u)
		end

end
