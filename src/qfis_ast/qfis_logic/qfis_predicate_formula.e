-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
  QFIS_PREDICATE_FORMULA

inherit
	QFIS_FORMULA
	redefine as_text, precedence, process end

create
	make

feature

	arguments: QFIS_ARGUMENTS

	predicate: STRING

	set (a_predicate: STRING; args: QFIS_ARGUMENTS)
		do
			predicate := a_predicate
			arguments := args
		end

	as_text: STRING
		do
			Result := predicate + "(" + arguments.as_text + ")"
		end

	precedence: INTEGER = 7

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_predicate_formula (Current)
		end

feature

	is_quantifier_free: BOOLEAN
		do
			Result := True
		end

	is_dnf: BOOLEAN
		do
			Result := True
		end

	is_disjunctive: BOOLEAN
		do
			Result := True
		end

	is_cnf: BOOLEAN
		do
			Result := True
		end

	is_conjunctive: BOOLEAN
		do
			Result := True
		end

feature {NONE}

	make (a_predicate: STRING; args: QFIS_ARGUMENTS)
		do
			set (a_predicate, args)
		end

end
