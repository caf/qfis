-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
  QFIS_ARITHMETIC_LTE

inherit
	QFIS_ARITHMETIC_COMPARISON
	redefine as_text, process end

create
	make

feature

	as_text: STRING
	 do
	 	Result := left.as_text_parenthesized (Current) + " <= " + right.as_text_parenthesized (Current)
	 end

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_arithmetic_lte (Current)
		end

feature {NONE}

	make (l, r: QFIS_INTEGER_TERM)
		do
			set (l, r)
		end

end
