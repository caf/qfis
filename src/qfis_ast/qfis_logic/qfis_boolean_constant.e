-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_BOOLEAN_CONSTANT

inherit
	QFIS_ATOMIC_FORMULA
	redefine as_text, process, precedence end

create
	make

feature

	constant: BOOLEAN assign set

	set (c: BOOLEAN)
	  do
	  	constant := c
	  end

	 as_text: STRING
	 	do
	 		Result := constant.out
	 	end

	precedence: INTEGER = 100

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_boolean_constant (Current)
		end

feature

	is_dnf: BOOLEAN
		do
			Result := True
		end

	is_disjunctive: BOOLEAN
		do
			Result := True
		end

	is_cnf: BOOLEAN
		do
			Result := True
		end

	is_conjunctive: BOOLEAN
		do
			Result := True
		end


feature {NONE}

	make (c: BOOLEAN)
		do
			set (c)
		end

end
