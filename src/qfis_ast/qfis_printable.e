-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

deferred class
	QFIS_PRINTABLE

inherit
	HASHABLE
	redefine is_equal end

feature

	is_domain (d: STRING): BOOLEAN
		do
			Result := attached d and then (d.is_equal ("BOOLEAN") or d.is_equal ("SEQUENCE") or d.is_equal ("INTEGER"))
		end

	is_indented: BOOLEAN assign set_indent

	set_indent (b: BOOLEAN)
		do
			is_indented := b
		end

	indent_text
		-- `as_text' will indent text
		do
			set_indent (True)
		end

	unindent_text
		-- `as_text' will not indent text	
		do
			set_indent (False)
		end

	as_text_indented (n: INTEGER): STRING
		-- Text with `n' levels of base indentation
		-- Indented regardless of the value of `is_indented'
		local ind: BOOLEAN
		do
			ind := is_indented
			indent_text
			Result := indented (as_text, n)
			if not ind then
				unindent_text
			end
		end

	as_text: STRING
		deferred end

	precedence: INTEGER
		-- Precedence of the formula, with smaller numbers denoting weaker bindings
		do
			Result := precedence_unset
		end

	as_text_parenthesized (other: QFIS_PRINTABLE): STRING
		-- Textual version of `Current' enclosed in parentheses if `other' has stronger or equal precedence
		do
			if other.precedence >= precedence then
				Result := "(" + as_text + ")"
			else
				Result := as_text
			end
		end

	hash_code: INTEGER
		do
			Result := as_text.hash_code
		end

	as_text_simplified: STRING
		-- `as_text' with every character other than [A-Za-z0-9_#] replaced by `#'
		local
			i: INTEGER
			c: CHARACTER
			s: STRING
		do
			from
				create Result.make_empty
				i := 0
				s := as_text
			until i = s.count
			loop
				i := i + 1
				c := s.at (i)
				if c.is_alpha_numeric or c = '_' or c = '#' or c = '-' or c = "+" then
					Result.extend (c)
				else
					Result.extend ('#')
				end
			end
		end

	is_equal (other: like Current): BOOLEAN
			-- Is `other' attached to an object considered
			-- equal to current object?
		do
			Result := hash_code = other.hash_code
		end

	line_number: INTEGER assign set_line
		-- Line number in the original file parsed

	set_line (n: INTEGER)
		do
			line_number := n
		end

	lineof: STRING
		do
			create Result.make_from_string ("Line " + line_number.out + ": ")
		end

feature {QFIS_PRINTABLE}

	precedence_unset: INTEGER = -100

	indented (s: STRING; n: INTEGER): STRING
		local
			lines: LIST [STRING]
		do
			if n > 0 and is_indented then
				create Result.make_empty
				lines := s.split ('%N')
				from lines.start
				until lines.after
				loop
					if not lines.isfirst then
						Result.extend ('%N')
					end
					Result.append (indentation_string (n) + lines.item)
					lines.forth
				end
			else
				Result := s
			end
		end


	indentation_string (n: INTEGER): STRING
		local i: INTEGER
		do
			create Result.make_empty
			from i := 0
			until i = n
			loop
				Result.extend ('%T')
				i := i + 1
			end
		ensure
			attached Result
		end


end
