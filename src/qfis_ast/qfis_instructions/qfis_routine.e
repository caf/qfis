-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_ROUTINE

inherit
	QFIS_DECLARATION
	redefine set_indent end
	QFIS_VERIFIABLE
	redefine generate_vc end

create
	make

feature

	signature: QFIS_SIGNATURE

	precondition: QFIS_PRECONDITION

	modify: QFIS_LIST_VARIABLES

	locals: QFIS_LOCAL

	body: QFIS_BODY

	postcondition: QFIS_POSTCONDITION


feature

	set_indent (b: BOOLEAN)
		do
			Precursor (b)
			signature.set_indent (b)
			precondition.set_indent (b)
			locals.set_indent (b)
			body.set_indent (b)
			postcondition.set_indent (b)
		end

	as_text: STRING
		do
			Result := indented (signature.as_text + "%N" + indented (precondition.as_text, 1) + "%N" + indented (locals.as_text, 1) + "%N"
									+ indented (body.as_text, 1) + "%N" + indented (postcondition.as_text, 1) + "%N" + indented ("end", 1), 0)
		end


	set (a_sig: QFIS_SIGNATURE; a_pre: QFIS_PRECONDITION; a_mod: QFIS_LIST_VARIABLES; a_loc: QFIS_LOCAL; a_b: QFIS_BODY; a_post: QFIS_POSTCONDITION)
		require
			attached a_sig
			attached a_pre
			attached a_mod
			attached a_loc
			attached a_b
			attached a_post
		do
			signature := a_sig
			precondition := a_pre
			modify := a_mod
			locals := a_loc
			body := a_b
			postcondition := a_post
		end

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_routine (Current)
		end

feature {NONE} -- Initialization

	make (a_sig: QFIS_SIGNATURE; a_pre: QFIS_PRECONDITION; a_mod: QFIS_LIST_VARIABLES; a_loc: QFIS_LOCAL; a_b: QFIS_BODY; a_post: QFIS_POSTCONDITION)
		require
			attached a_sig
			attached a_pre
			attached a_mod
			attached a_loc
			attached a_b
			attached a_post
		do
			set (a_sig, a_pre, a_mod, a_loc, a_b, a_post)
		end

feature -- Verifiable

	compute_pre
	    require else
   			attached fresh
			attached symbols
		do
			create post.make
			if postcondition.is_empty then
				post.extend (create {QFIS_BOOLEAN_CONSTANT}.make (True))
			else
				post.append (postcondition)
			end
			body.set_fresh (fresh)
			body.set_symbols (symbols)
			body.set_post (post)
			body.compute_pre
			pre := body.pre
		ensure then
			attached post
			not post.is_empty
		end

	generate_vc
		require else
   			attached fresh
			attached symbols
		local
			vc: QFIS_VC
			oldies: LINKED_SET [QFIS_ARGUMENT]
			inst: QFIS_INSTRUCTION
		do
			if symbols.old_variables.has (signature.name) then
				oldies := symbols.old_variables.at (signature.name)
				if not oldies.is_empty then
					from oldies.start
					until oldies.after
					loop
						if attached {QFIS_VARIABLE} oldies.item as seq then
							create {QFIS_ASSIGN_SEQUENCE} inst.make ("OLD_" + seq.identifier, seq)
						elseif attached {QFIS_INTEGER_VARIABLE} oldies.item as int then
							create {QFIS_ASSIGN_INTEGER} inst.make ("old_" + int.identifier, int)
						elseif attached {QFIS_BOOLEAN_VARIABLE} oldies.item as bool then
							create {QFIS_ASSIGN_BOOLEAN} inst.make ("old_" + bool.identifier, bool)
						else
							-- Shouldn't be of any other type
							check False end
						end
						body.put_front (inst)
						oldies.forth
					end
				end
			end

			if not attached pre or else pre.is_empty then
				compute_pre
			end
			check attached pre and then not pre.is_empty end
			Precursor
			body.generate_vc
			verification_conditions.append (body.verification_conditions)

			create vc.make
			vc.assume.append (precondition)
			vc.prove.append (pre)
			verification_conditions.extend (vc)
		ensure then
			attached post
			not post.is_empty
			attached pre
			not pre.is_empty
		end

end
