-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_CALL_COMMAND

inherit
	QFIS_CALL
	redefine process end

create
	make

feature

	as_text: STRING
		do
			create Result.make_from_string ("call ")
			Result.append (routine)
			if not arguments.is_empty then
				Result.append (" (")
				Result.append (arguments.as_text)
				Result.append (")")
			end
		end

feature {NONE} -- Initialization

	make (a_routine: STRING; args: QFIS_LIST_VARIABLES)
		require
			attached a_routine
		do
			set_routine (a_routine, args)
		end

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_call_command (Current)
		end


end
