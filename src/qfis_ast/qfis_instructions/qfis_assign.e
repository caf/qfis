-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_ASSIGN [G -> {QFIS_PRINTABLE, QFIS_VISITABLE}]

inherit
	QFIS_INSTRUCTION

create
	make

feature

	target: STRING

	value: G

	set (a_target: STRING; a_value: G)
		require
			attached a_target
			attached a_value
		do
			target := a_target
			value := a_value
		end


feature

	as_text: STRING
		do
			Result := indented (target + " := " + value.as_text, 0)
		end


feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			value.process (v)
		end


feature {NONE} -- Initialization

	make (a_target: STRING; a_value: G)
		require
			attached a_target
			attached a_value
		do
			set (a_target, a_value)
		end

feature -- Verifiable

	compute_pre
		local sub: QFIS_SUBSTITUTE [G]
		do
			pre := post.deep_twin
			create sub.make (target, value)
			from pre.start
			until pre.after
			loop
				pre.item.process (sub)
				pre.forth
			end
		end


end
