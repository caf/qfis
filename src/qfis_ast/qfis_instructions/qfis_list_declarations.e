class
	QFIS_LIST_DECLARATIONS

inherit
	QFIS_DECLARATION
	redefine set_indent, process end

create
	make

feature

	ordered: QFIS_LIST_VARIABLES

	booleans, sequences, integers: QFIS_LIST_VARIABLES

	add_boolean (v: STRING)
		do
			booleans.extend (v)
			ordered.extend (v)
		end

	add_booleans (l: QFIS_LIST_VARIABLES)
		do
			booleans.append (l)
			ordered.append (l)
		end

	add_sequence (v: STRING)
		do
			sequences.extend (v)
			ordered.extend (v)
		end

	add_sequences (l: QFIS_LIST_VARIABLES)
		do
			sequences.append (l)
			ordered.append (l)
		end

	add_integer (v: STRING)
		do
			integers.extend (v)
			ordered.extend (v)
		end

	add_integers (l: QFIS_LIST_VARIABLES)
		do
			integers.append (l)
			ordered.append (l)
		end

	types_list: LINKED_LIST [STRING]
		-- List with all ordered types of declarations in `d'	
		do
			create Result.make
			Result.compare_objects
			from ordered.start
			until ordered.after
			loop
				if booleans.has (ordered.item) then
					Result.extend ("BOOLEAN")
				elseif integers.has (ordered.item) then
					Result.extend ("INTEGER")
				else
					Result.extend ("SEQUENCE")
				end
				ordered.forth
			end
		ensure
			Result.count = ordered.count
		end

feature

	set_indent (b: BOOLEAN)
		do
			Precursor (b)
			booleans.set_indent (b)
			sequences.set_indent (b)
			integers.set_indent (b)
		end

	as_text: STRING
		local t: LINKED_LIST [STRING]
		do
			create Result.make_empty
			t := types_list
			from t.start ; ordered.start
			until t.after
			loop
				Result.append (ordered.item + ": " + t.item)
				if not t.islast then
					Result.append ("; ")
				end
				t.forth
				ordered.forth
			end
			Result := indented (Result, 0)
		end


feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_list_declarations (Current)
		end


feature {NONE} -- Initialization

	make
		do
			create booleans.make
			create sequences.make
			create integers.make
			create ordered.make
		end

end
