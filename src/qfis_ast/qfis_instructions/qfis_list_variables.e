-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_LIST_VARIABLES

inherit
	QFIS_PRINTABLE
	rename
		copy as printable_copy, is_equal as printable_is_equal
	export
		{NONE} printable_copy, printable_is_equal
	end

	QFIS_VISITABLE
	undefine
		copy
	end

	LINKED_LIST [STRING]
	rename
		copy as list_copy, is_equal as list_is_equal, make as list_make
	export
		{NONE} all
		{ANY} start, after, forth, extend, append, remove, go_i_th, index, count, item, islast, isfirst, is_empty, put_front, has
	select
		list_copy, list_is_equal
	end

create
	make

feature

	copy (other: like Current)
		do
			list_copy (other)
			line_number := other.line_number
		end

	is_equal (other: like Current): BOOLEAN
		do
			compare_objects
			Result := list_is_equal (other)
		end


feature

	process (v: QFIS_ITERATOR)
		do
			v.process_list_variables (Current)
		end

feature

	as_text: STRING
		do
			create Result.make_empty
			from start
			until after
			loop
				Result.append (item)
				if not islast then
					Result.append (", ")
				end
				forth
			end
			Result := indented (Result, 0)
		end

feature {NONE} -- Initialization

	make
		do
			list_make
			compare_objects
		end

end
