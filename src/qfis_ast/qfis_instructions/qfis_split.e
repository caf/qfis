-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_SPLIT

inherit
	QFIS_INSTRUCTION

create
	make

feature

	sequence: QFIS_SEQUENCE

	into: QFIS_LIST_VARIABLES

	set (a_sequence: QFIS_SEQUENCE; an_into: QFIS_LIST_VARIABLES)
		require
			attached a_sequence
			attached an_into
		do
			sequence := a_sequence
			into := an_into
		end


feature

	as_text: STRING
		do
			create Result.make_from_string ("split ")
			Result.append (sequence.as_text)
			Result.append (" into ")
			Result.append (into.as_text)
			Result := indented (Result, 0)
		end


feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_split (Current)
		end

feature {NONE} -- Initialization

	make (a_sequence: QFIS_SEQUENCE; an_into: QFIS_LIST_VARIABLES)
		require
			attached a_sequence
			attached an_into
		do
			set (a_sequence, an_into)
		end

feature -- Verifiable

	compute_pre
		local
			h: QFIS_HAVOC
			a: QFIS_ASSUME
			s: STRING
			prev, cur: QFIS_CAT_COMPOUND
			top: QFIS_SEQUENCE
		do
			if into.count = 1 then
				into.start
				create {QFIS_VARIABLE} top.make (into.item)
			else
				check into.count > 1 end
				from into.start
				until into.after
				loop
					if not into.islast then
						create cur.make (create {QFIS_VARIABLE}.make (into.item), Void)
					else
						prev.set (prev.left, create {QFIS_VARIABLE}.make (into.item))
					end
					if not into.isfirst and not into.islast then
						prev.set (prev.left, cur)
					end
					prev := cur
					if into.isfirst then
						top := cur
					end
					into.forth
				end
			end

			check attached top end
			create a.make (create {QFIS_SEQUENCE_EQ}.make (top, sequence))
			a.set_symbols (symbols)
			a.set_fresh (fresh)
			a.set_post (post)
			a.compute_pre
			create h.make (into)
			h.set_symbols (symbols)
			h.set_fresh (fresh)
			h.set_post (a.pre)
			h.compute_pre
			pre := h.pre
		end

end
