-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_PROGRAM

inherit
	QFIS_LIST [QFIS_DECLARATION]
	redefine as_text end

create
	make

feature

	as_text: STRING
		do
			create Result.make_empty
			from start
			until after
			loop
				Result.append ("%N%N")
				if attached {QFIS_LIST_DECLARATIONS} item as dec then
					Result.append ("global ")
				end
				Result.append (indented (item.as_text, 0))
				forth
			end
		end

end
