-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_ASSERT

inherit
	QFIS_INSTRUCTION

create
	make

feature

	formula: QFIS_FORMULA

	set (a_formula: QFIS_FORMULA)
		require attached a_formula
		do
			formula := a_formula
		end

feature

	as_text: STRING
		do
			Result := indented("assert " + formula.as_text, 0)
		end


feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_assert (Current)
		end

feature {NONE} -- Initialization

	make (a_formula: QFIS_FORMULA)
		require attached a_formula
		do
			set (a_formula)
		end

feature -- Verifiable

	compute_pre
		do
			pre := post.deep_twin
			formula.set_annotation ("assert on line " + line_number.out)
			pre.extend (formula)
		end

end
