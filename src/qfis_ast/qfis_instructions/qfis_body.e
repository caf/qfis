-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_BODY

inherit
	QFIS_LIST [QFIS_INSTRUCTION]
	redefine as_text
	select list_copy, list_is_equal end
	QFIS_VERIFIABLE
	redefine generate_vc end

create
	make, make_with_instructions

feature

	as_text: STRING
		do
			create Result.make_from_string ("do")
			Result.extend ('%N')
			Result.append (indented (Precursor, 1))
			Result := indented (Result, 0)
		end


feature {NONE} -- Initialization

	make_with_instructions (inst: QFIS_LIST [QFIS_INSTRUCTION])
		do
			make
			from inst.start
			until inst.after
			loop
				extend (inst.item)
				inst.forth
			end
		end

feature -- Verifiable

	compute_pre
		local l_next: QFIS_INSTRUCTION
		do
			if not is_empty then
				from finish
				until before
				loop
					if islast then
						item.set_post (post)
					else
						item.set_post (l_next.pre.deep_twin)
					end
					l_next := item
					item.set_fresh (fresh)
					item.set_symbols (symbols)
					item.compute_pre
					back
				end
				forth
				pre := item.pre
			else
				pre := post
			end
		end

	generate_vc
		do
			Precursor
			from start
			until after
			loop
				if attached item.post and then not item.post.is_empty then
					item.generate_vc
					verification_conditions.append (item.verification_conditions)
				end
				forth
			end
		end

end
