-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_HAVOC

inherit
	QFIS_INSTRUCTION

create
	make

feature

	target: QFIS_LIST_VARIABLES

	set (a_target: QFIS_LIST_VARIABLES)
		require attached a_target
		do
			target := a_target
		end


feature

	as_text: STRING
		do
			Result := indented("havoc " + target.as_text, 0)
		end


feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_havoc (Current)
		end


feature {NONE} -- Initialization

	make (a_target: QFIS_LIST_VARIABLES)
		require attached a_target
		do
			set (a_target)
		end

feature -- Verifiable

	compute_pre
		local sub: QFIS_ITERATOR_MODIFIER
		do
			pre := post.deep_twin
			from target.start
			until target.after
			loop
				sub := build_substitute (target.item, Void, Void)
				if attached sub then
					from pre.start
					until pre.after
					loop
						pre.item.process (sub)
						pre.forth
					end
				end
				target.forth
			end
		end

end
