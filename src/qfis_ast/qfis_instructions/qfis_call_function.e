-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_CALL_FUNCTION

inherit
	QFIS_CALL
	redefine process, compute_pre end

create
	make

feature

	targets: QFIS_LIST_VARIABLES

	set_targets (targs: QFIS_LIST_VARIABLES)
		do
			if attached targs then
				targets := targs
			else
				create targets.make
			end
		end


feature

	as_text: STRING
		do
			create Result.make_from_string ("call ")
			Result.append (targets.as_text)
			Result.append (" := ")
			Result.append (routine)
			if not arguments.is_empty then
				Result.append (" (")
				Result.append (arguments.as_text)
				Result.append (")")
			end
		end


feature {NONE} -- Initialization

	make (a_routine: STRING; args: QFIS_LIST_VARIABLES; targs: QFIS_LIST_VARIABLES)
		require
			attached a_routine
		do
			set_routine (a_routine, args)
			set_targets (targs)
		end

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_call_function (Current)
		end


feature -- Verifiable

	compute_pre
		local
			formals, actuals, mods: LINKED_LIST [STRING]
			fresh_actuals: LINKED_LIST [QFIS_ARGUMENT]
			sub: QFIS_ITERATOR_MODIFIER
			pos_first_post: INTEGER
			h: QFIS_HAVOC
			fresh_targets: QFIS_LIST_VARIABLES
			a_sequence: QFIS_ASSIGN [QFIS_SEQUENCE]
			a_boolean: QFIS_ASSIGN [QFIS_FORMULA]
			a_integer: QFIS_ASSIGN [QFIS_INTEGER_TERM]
		do
			Precursor
			body_for_pre.go_i_th (symbols.routine_preconditions.at (routine).count)
			pos_first_post := symbols.routine_preconditions.at (routine).count + 1
					-- # preconditions + 1 havoc of modifies
			check pos_first_post <= body_for_pre.count end

			from
				formals := symbols.routine_signature_codomain.at (routine)
				formals.start
				create fresh_actuals.make
				create fresh_targets.make
			until
				formals.after
			loop
					-- Generate with fresh variable
				sub := build_substitute (formals.item, Void, symbols.type_of_codomain (routine, formals.index))
				fresh_actuals.extend (fresh.item)
				fresh_targets.extend (fresh.item.as_text)
				from body_for_pre.go_i_th (pos_first_post)
				until body_for_pre.after
				loop
					if attached {QFIS_ASSUME} body_for_pre.item as l_as then
						l_as.formula.process (sub)
					end
					body_for_pre.forth
				end
				formals.forth
			end

				-- Every fresh variable is now assigned to the corresponding actual argument
			check targets.count = fresh_actuals.count end
			from
				targets.start
				fresh_actuals.start
			until targets.after
			loop
				if attached {QFIS_FORMULA} fresh_actuals.item as b then
					create a_boolean.make (targets.item, b)
					a_boolean.set_line (line_number)
					body_for_pre.extend (a_boolean)
				elseif attached {QFIS_SEQUENCE} fresh_actuals.item as s then
					create a_sequence.make (targets.item, s)
					a_sequence.set_line (line_number)
					body_for_pre.extend (a_sequence)
				elseif attached {QFIS_INTEGER_TERM} fresh_actuals.item as i then
					create a_integer.make (targets.item, i)
					a_integer.set_line (line_number)
					body_for_pre.extend (a_integer)
				end
				targets.forth
				fresh_actuals.forth
			end

			create h.make (fresh_targets)
			h.set_line (line_number)
			body_for_pre.go_i_th (pos_first_post - 1)
			body_for_pre.put_right (h)

			body_for_pre.pre.wipe_out
			body_for_pre.set_line (line_number)
			body_for_pre.compute_pre
			pre := body_for_pre.pre
		end

end
