-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

deferred class
	QFIS_CALL

inherit
	QFIS_INSTRUCTION

feature

	routine: STRING

	arguments: QFIS_LIST_VARIABLES


	set_routine (a_routine: STRING; args: QFIS_LIST_VARIABLES)
		require
			attached a_routine
		do
			routine := a_routine
			if attached args then
				arguments := args
			else
				create arguments.make
			end
		end


feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_call (Current)
		end

feature -- Verifiable

	compute_pre
		local
			l_pre, l_post: QFIS_LIST_ASSERTIONS
			a_pre: QFIS_ASSERT
			a_post: QFIS_ASSUME
			h: QFIS_HAVOC
			formals, actuals, mods: LINKED_LIST [STRING]
			sub: QFIS_ITERATOR_MODIFIER
			targets: QFIS_LIST_VARIABLES
			l_body: QFIS_BODY
			oldies: LINKED_SET [QFIS_ARGUMENT]
			inst: QFIS_INSTRUCTION
		do
			check symbols.routine_preconditions.has (routine) end
			l_pre := symbols.routine_preconditions.at (routine).deep_twin
			check symbols.routine_postconditions.has (routine) end
			l_post := symbols.routine_postconditions.at (routine).deep_twin

			from
				formals := symbols.routine_signature_domain.at (routine)
				formals.start
				actuals := arguments
				actuals.start
			until
				formals.after
			loop
				sub := build_substitute (formals.item, actuals.item, symbols.type_of_domain (routine, formals.index))
				from l_pre.start
				until l_pre.after
				loop
					l_pre.item.process (sub)
					l_pre.forth
				end
				from l_post.start
				until l_post.after
				loop
					l_post.item.process (sub)
					l_post.forth
				end
				formals.forth
				actuals.forth
			end

			create targets.make
			mods := symbols.modify.at (routine)
			from mods.start
			until mods.after
			loop
				targets.extend (mods.item)
				mods.forth
			end
			create h.make (targets)
			h.set_line (line_number)

			create l_body.make
			l_body.set_fresh (fresh)
			l_body.set_symbols (symbols)

			from l_pre.start
			until l_pre.after
			loop
				create a_pre.make (l_pre.item)
				l_body.extend (a_pre)
				l_pre.forth
			end
			if symbols.old_variables.has (routine) then
				oldies := symbols.old_variables.at (routine)
				from oldies.start
				until oldies.after
				loop
					if attached {QFIS_VARIABLE} oldies.item as seq then
						create {QFIS_ASSIGN_SEQUENCE} inst.make ("OLD_local_" + seq.identifier, seq)
						sub := build_substitute ("OLD_" + seq.identifier, "OLD_local_" + seq.identifier, "SEQUENCE")
					elseif attached {QFIS_INTEGER_VARIABLE} oldies.item as int then
						create {QFIS_ASSIGN_INTEGER} inst.make ("old_local_" + int.identifier, int)
						sub := build_substitute ("old_" + int.identifier, "old_local_" + int.identifier, "INTEGER")
					elseif attached {QFIS_BOOLEAN_VARIABLE} oldies.item as bool then
						create {QFIS_ASSIGN_BOOLEAN} inst.make ("old_local_" + bool.identifier, bool)
						sub := build_substitute ("old_" + bool.identifier, "old_local_" + bool.identifier, "BOOLEAN")
					else
						-- Shouldn't be of any other type
						check False end
					end
					l_body.extend (inst)

					-- Replace every occurrence of references to "old" with the local old variable				
					from l_post.start
					until l_post.after
					loop
						l_post.item.process (sub)
						l_post.forth
					end

					oldies.forth
				end
			end
			l_body.extend (h)
			from l_post.start
			until l_post.after
			loop
				create a_post.make (l_post.item)
				l_body.extend (a_post)
				l_post.forth
			end

			l_body.set_post (post)
			l_body.set_line (line_number)
			l_body.compute_pre
			body_for_pre := l_body
			pre := l_body.pre
		end


feature {QFIS_CALL} -- Implementation

	body_for_pre: QFIS_BODY

end
