-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
   QFIS_ARGUMENTS

inherit
	QFIS_PRINTABLE; QFIS_VISITABLE
	ARRAYED_LIST [QFIS_ARGUMENT]
	rename make as make_list
	undefine is_equal, copy end

create
	make_empty

feature

	as_text: STRING
		do
			from
				create Result.make_empty
				start
			until after
			loop
				Result.append (item.as_text)
				if not islast then
					Result.append (", ")
				end
				forth
			end
		end


feature

	set (a: LIST [QFIS_ARGUMENT])
		require a.count > 1
		do
			make_list (a.count)
			from a.start
			until a.after
			loop
				extend (a.item)
				a.forth
			end
		end

feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_arguments (Current)
		end


feature {NONE} -- Initialization

	make_empty
		do
			make_list (1)
		end


end
