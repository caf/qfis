-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_LIST_ASSERTIONS

inherit
	QFIS_LIST [QFIS_FORMULA]
	redefine as_text end

create
	make, make_from_list

feature

	as_text: STRING
		do
			create Result.make_empty
			from start
			until after
			loop
				if not isfirst then
					Result.extend ('%N')
				end
				if attached item.label as l and then not l.is_empty then
					Result.append (l + " ")
				end
				Result.append (item.as_text)
				forth
			end
		end


feature {NONE} -- Initialization

	make_from_list (l: QFIS_LIST [QFIS_FORMULA])
		do
			make
			from l.start
			until l.after
			loop
				extend (l.item)
				l.forth
			end
		end

end
