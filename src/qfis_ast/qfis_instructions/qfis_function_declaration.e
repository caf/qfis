-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_FUNCTION_DECLARATION

inherit
	QFIS_DECLARATION
	redefine set_indent end

create
	make

feature

	name: STRING assign set_name

	set_name (n: STRING)
		require
			attached n
			not n.is_empty
		do
			name := n
		end

	codomain: STRING assign set_codomain

	set_codomain (s: STRING)
		require is_domain (s)
		do
			codomain := s
		end

	domain: QFIS_LIST_VARIABLES assign set_domain

	set_domain (d: QFIS_LIST_VARIABLES)
		do
			domain := d
		end

	file_name: STRING assign set_file_name

	set_file_name (f: STRING)
		require attached f
		do
			file_name := f
		end


feature

	set_indent (b: BOOLEAN)
		do
			Precursor (b)
			domain.set_indent (b)
		end

	as_text: STRING
		do
			create Result.make_from_string ("function " + name)
			if not domain.is_empty then
				Result.append (" (" + domain.as_text + ")")
			end
			check is_domain (codomain) end
			Result.append (": " + codomain)
			if not file_name.is_empty then
				Result.append (" in " + "%"" + file_name + "%"")
			end
			Result := (indented (Result, 0))
		end

feature

	process (v: QFIS_ITERATOR)
		do
			v.process_function_declaration (Current)
		end



feature {NONE} -- Initialization

	make (a_name: STRING; a_codomain: STRING)
		require
			attached a_name
			is_domain (a_codomain)
		do
			create domain.make
			create name.make_empty
			create codomain.make_empty
			create file_name.make_empty
			set_name (a_name)
			set_codomain (a_codomain)
		end

end
