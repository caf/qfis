-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_AXIOM

inherit
	QFIS_DECLARATION
	redefine set_indent end

create
	make

feature

	formula: QFIS_FORMULA assign set_formula

	set_formula (f: QFIS_FORMULA)
		require
			attached f
		do
			formula := f
		end


feature

	set_indent (b: BOOLEAN)
		do
			Precursor (b)
			formula.set_indent (b)
		end

	as_text: STRING
		do
			create Result.make_from_string ("axiom " + formula.as_text)
			Result := indented (Result, 0)
		end

feature

	process (v: QFIS_ITERATOR)
		do
			v.process_axiom (Current)
		end



feature {NONE} -- Initialization

	make (a_formula: QFIS_FORMULA)
		require
			attached a_formula
		do
			set_formula (a_formula)
		end

end
