-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_CONDITIONAL

inherit
	QFIS_INSTRUCTION
	redefine set_indent, process, generate_vc end

create
	make

feature

	condition: QFIS_FORMULA

	then_branch: QFIS_LIST [QFIS_INSTRUCTION]

	else_branch: QFIS_LIST [QFIS_INSTRUCTION]

	nondeterministic: BOOLEAN
		do
			Result := not attached condition
		end

	set (a_condition: QFIS_FORMULA; a_then: QFIS_LIST [QFIS_INSTRUCTION]; an_else: QFIS_LIST [QFIS_INSTRUCTION])
		do
			condition := a_condition
			if attached a_then then
				then_branch := a_then
			else
				create then_branch.make
			end
			if attached an_else then
				else_branch := an_else
			else
				create else_branch.make
			end
		end

feature

	set_indent (b: BOOLEAN)
		do
			Precursor (b)
			if not nondeterministic then
				condition.set_indent (b)
			end
			then_branch.set_indent (b)
			else_branch.set_indent (b)
		end

	as_text: STRING
		do
			create Result.make_from_string ("if ")
			if nondeterministic then
				Result.append ("<?>")
			else
				Result.append (condition.as_text)
			end
			Result.append (" then")
			Result.append ("%N" + indented (then_branch.as_text, 1))
			if not else_branch.is_empty  then
				Result.append ("%N" + "else" + "%N")
				Result.append (indented (else_branch.as_text, 1))
			end
			Result.append ("%N" + "end")
			Result := indented (Result, 0)
		end


feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_conditional (Current)
		end

feature {NONE} -- Initialization

	make (a_condition: QFIS_FORMULA; a_then: QFIS_LIST [QFIS_INSTRUCTION]; an_else: QFIS_LIST [QFIS_INSTRUCTION])
		do
			set (a_condition, a_then, an_else)
		end

feature -- Verifiable

	compute_pre
		local
			l_then, l_else: QFIS_BODY
			not_cond: QFIS_NOT_FORMULA
			impl: QFIS_IMPLIES_FORMULA
		do
			create l_then.make_with_instructions (then_branch)
			l_then.set_fresh (fresh)
			l_then.set_symbols (symbols)
			l_then.set_post (post.deep_twin)
			if l_then.is_empty then
				l_then.extend (create {QFIS_SKIP})
				l_then.set_line (line_number)
			else
				then_branch.start
				l_then.set_line (then_branch.item.line_number)
			end
			create l_else.make_with_instructions (else_branch)
			l_else.set_fresh (fresh)
			l_else.set_symbols (symbols)
			l_else.set_post (post.deep_twin)
			if l_else.is_empty then
				l_else.extend (create {QFIS_SKIP})
				l_else.set_line (l_then.line_number)
			else
				else_branch.start
				l_else.set_line (else_branch.item.line_number)
			end

			l_then.compute_pre
			l_else.compute_pre

			if not nondeterministic then
				from l_then.pre.start
				until l_then.pre.after
				loop
					create impl.make (condition.deep_twin, l_then.pre.item)
					impl.set_line (l_then.pre.item.line_number)
					l_then.pre.replace (impl)
					l_then.pre.forth
				end
				create not_cond.make (condition)
				from l_else.pre.start
				until l_else.pre.after
				loop
					create impl.make (not_cond.deep_twin, l_else.pre.item)
					impl.set_line (l_else.pre.item.line_number)
					l_else.pre.replace (impl)
					l_else.pre.forth
				end
			end

			create pre.make
			pre.append (l_then.pre)
			pre.append (l_else.pre)
		end

	generate_vc
		do
			Precursor
			from then_branch.start
			until then_branch.after
			loop
				if attached then_branch.item.post and then not then_branch.item.post.is_empty then
					then_branch.item.generate_vc
					verification_conditions.append (then_branch.item.verification_conditions)
				end
				then_branch.forth
			end
			from else_branch.start
			until else_branch.after
			loop
				if attached else_branch.item.post and then not else_branch.item.post.is_empty then
					else_branch.item.generate_vc
					verification_conditions.append (else_branch.item.verification_conditions)
				end
				else_branch.forth
			end
		end

end
