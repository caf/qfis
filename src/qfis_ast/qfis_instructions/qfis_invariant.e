-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_INVARIANT

inherit
	QFIS_LIST_ASSERTIONS
	redefine as_text end

create
	make, make_from_list

feature

	as_text: STRING
		local p: STRING
		do
			create Result.make_from_string ("invariant")
			p := Precursor
			if not p.is_empty then
				Result.extend ('%N')
			end
			Result.append (indented (p, 1))
			Result := indented (Result, 0)
		end


end
