-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_LOOP

inherit
	QFIS_INSTRUCTION
	redefine set_indent, generate_vc end

create
	make

feature

	exit: QFIS_FORMULA

	loop_invariant: QFIS_INVARIANT

	body: QFIS_LIST [QFIS_INSTRUCTION]

	nondeterministic: BOOLEAN
		do
			Result := not attached exit
		end

	set (exit_condition: QFIS_FORMULA; inv: QFIS_INVARIANT; b: QFIS_LIST [QFIS_INSTRUCTION])
		require
			attached inv
			attached b
		do
			exit := exit_condition
			loop_invariant := inv
			body := b
		end

feature

	set_indent (b: BOOLEAN)
		do
			Precursor (b)
			if not nondeterministic then
				exit.set_indent (b)
			end
			loop_invariant.set_indent (b)
			body.set_indent (b)
		end

	as_text: STRING
		do
			create Result.make_from_string ("until ")
			if nondeterministic then
				Result.append ("<?>")
			else
				Result.append (exit.as_text)
			end
			if not loop_invariant.is_empty  then
				Result.extend ('%N')
				Result.append (loop_invariant.as_text)
			end
			Result.append ("%N" + "loop")
			if not body.is_empty then
				Result.extend ('%N')
			end
			Result.append (indented (body.as_text, 1))
			Result.append ("%N" + "end")
			Result := indented (Result, 0)
		end


feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_loop (Current)
		end


feature {NONE} -- Initialization

	make (exit_condition: QFIS_FORMULA; inv: QFIS_INVARIANT; b: QFIS_LIST [QFIS_INSTRUCTION])
		require
			attached inv
			attached b
		do
			set (exit_condition, inv, b)
		end

feature -- Verifiable

	compute_pre
		local inv_clause: QFIS_FORMULA
		do
			create pre.make
			if loop_invariant.is_empty then
				pre.extend (create {QFIS_BOOLEAN_CONSTANT}.make (True))
			end
			from loop_invariant.start
			until loop_invariant.after
			loop
				inv_clause := loop_invariant.item.deep_twin
				pre.extend (inv_clause)
				loop_invariant.forth
			end
		end

	generate_vc
		local
			l_body: QFIS_BODY
			l_post: LINKED_SET [QFIS_FORMULA]
			vc_consecution, vc_closing: QFIS_VC
		do
			Precursor

			create l_body.make_with_instructions (body)
			l_body.set_fresh (fresh)
			l_body.set_symbols (symbols)
			l_body.set_line (line_number)
			if l_body.is_empty then
				l_body.extend (create {QFIS_SKIP})
			end
			create l_post.make
			l_post.compare_objects
			l_post.append (loop_invariant)
			l_body.set_post (l_post)
			l_body.compute_pre

			create vc_consecution.make
			vc_consecution.description.append ("consecution of loop on line " + line_number.out)
			vc_consecution.assume.append (loop_invariant)
			if not nondeterministic then
				vc_consecution.put_assume (create {QFIS_NOT_FORMULA}.make (exit))
			end
			vc_consecution.prove.append (l_body.pre)
			verification_conditions.extend (vc_consecution)

			create vc_closing.make
			vc_closing.description.append ("closing of loop on line " + line_number.out)
			vc_closing.assume.append (loop_invariant)
			if not nondeterministic then
				vc_closing.put_assume (exit)
			end
			vc_closing.prove.append (post)
			verification_conditions.extend (vc_closing)

			from body.start
			until body.after
			loop
				if attached body.item.post and then not body.item.post.is_empty then
					body.item.generate_vc
					verification_conditions.append (body.item.verification_conditions)
				end
				body.forth
			end
		end

end
