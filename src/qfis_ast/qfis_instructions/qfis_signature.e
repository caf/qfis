-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_SIGNATURE

inherit
	QFIS_VISITABLE
	QFIS_PRINTABLE
	redefine set_indent end

create
	make

feature

	name: STRING

	input, output: QFIS_LIST_DECLARATIONS

	set (a_name: STRING; input_params: QFIS_LIST_DECLARATIONS; output_params: QFIS_LIST_DECLARATIONS)
		require attached a_name
		do
			name := a_name
			if attached input_params then
				input := input_params
			else
				create input.make
			end
			if attached output_params then
				output := output_params
			else
				create output.make
			end
		end


feature

	set_indent (b: BOOLEAN)
		do
			Precursor (b)
			input.set_indent (b)
			output.set_indent (b)
		end

	as_text: STRING
		local i, o: STRING
		do
		   create Result.make_from_string ("routine ")
			Result.append (name)
			i := input.as_text
			o := output.as_text
			if not i.is_empty then
				Result.append (" (")
				Result.append (i)
				Result.append (")")
			end
			if not o.is_empty then
				Result.append (": " + "(")
				Result.append (o)
				Result.append (")")
			end
			Result := indented (Result, 0)
		end


feature -- Visitor

	process (v: QFIS_ITERATOR)
		do
			v.process_signature (Current)
		end

feature {NONE} -- Initialization

	make (a_name: STRING; input_params: QFIS_LIST_DECLARATIONS; output_params: QFIS_LIST_DECLARATIONS)
		require attached a_name
		do
			set (a_name, input_params, output_params)
		end


end
