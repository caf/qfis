-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

note
	description: "Iterator over QFIS_FORMULA."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	QFIS_ITERATOR

feature -- Node processing

	process_and_formula (el: QFIS_AND_FORMULA)
		require attached el
		do
			process_boolean_formula (el)
		end

	process_arguments (el: QFIS_ARGUMENTS)
		require attached el
		do
			from el.start
			until el.after
			loop
				el.item.process (Current)
				el.forth
			end
		end

	process_arithmetic_comparison (el: QFIS_ARITHMETIC_COMPARISON)
		require attached el
		do
			el.left.process (Current)
			el.right.process (Current)
		end

	process_arithmetic_compound (el: QFIS_ARITHMETIC_COMPOUND)
		require attached el
		do
			el.left.process (Current)
			el.right.process (Current)
		end

	process_arithmetic_eq (el: QFIS_ARITHMETIC_EQ)
		require attached el
		do
			process_arithmetic_comparison (el)
		end

	process_arithmetic_gt (el: QFIS_ARITHMETIC_GT)
		require attached el
		do
			process_arithmetic_comparison (el)
		end

	process_arithmetic_gte (el: QFIS_ARITHMETIC_GTE)
		require attached el
		do
			process_arithmetic_comparison (el)
		end

	process_arithmetic_lt (el: QFIS_ARITHMETIC_LT)
		require attached el
		do
			process_arithmetic_comparison (el)
		end

	process_arithmetic_lte (el: QFIS_ARITHMETIC_LTE)
		require attached el
		do
			process_arithmetic_comparison (el)
		end

	process_arithmetic_neq (el: QFIS_ARITHMETIC_NEQ)
		require attached el
		do
			process_arithmetic_comparison (el)
		end

	process_assert (el: QFIS_ASSERT)
		require attached el
		do
			el.formula.process (Current)
		end

	process_assign_boolean (el: QFIS_ASSIGN_BOOLEAN)
		require attached el
		do
			el.value.process (Current)
		end

	process_assign_integer (el: QFIS_ASSIGN_INTEGER)
		require attached el
		do
			el.value.process (Current)
		end

	process_assign_sequence (el: QFIS_ASSIGN_SEQUENCE)
		require attached el
		do
			el.value.process (Current)
		end

	process_assume (el: QFIS_ASSUME)
		require attached el
		do
			el.formula.process (Current)
		end

	process_atomic_formula (el: QFIS_ATOMIC_FORMULA)
		require attached el
		do
		end

	process_atomic_sequence (el: QFIS_ATOMIC_SEQUENCE)
		require attached el
		do
		end

	process_axiom (el: QFIS_AXIOM)
		require attached el
		do
			el.formula.process (Current)
		end

	process_boolean_constant (el: QFIS_BOOLEAN_CONSTANT)
		require attached el
		do
		end

	process_boolean_formula (el: QFIS_BOOLEAN_FORMULA)
		require attached el
		do
			el.left.process (Current)
			el.right.process (Current)
		end

	process_boolean_variable (el: QFIS_BOOLEAN_VARIABLE)
		require attached el
		do
		end

	process_call (el: QFIS_CALL)
		require attached el
		do
			el.arguments.process (Current)
		end

	process_call_command (el: QFIS_CALL_COMMAND)
		require attached el
		do
			process_call (el)
		end

	process_call_function (el: QFIS_CALL_FUNCTION)
		require attached el
		do
			el.targets.process (Current)
			process_call (el)
		end

	process_cat_compound (el: QFIS_CAT_COMPOUND)
		require attached el
		do
			process_compound (el)
		end

	process_character (el: QFIS_CHARACTER)
		require attached el
		do
		end

	process_compound (el: QFIS_COMPOUND)
		require attached el
		do
			el.left.process (Current)
			el.right.process (Current)
		end

	process_conditional (el: QFIS_CONDITIONAL)
		require attached el
		do
			if not el.nondeterministic then
				el.condition.process (Current)
			end
			el.then_branch.process (Current)
			el.else_branch.process (Current)
		end

	process_constant (el: QFIS_CONSTANT)
		require attached el
		do
		end

	process_derived_sequence (el: QFIS_DERIVED_SEQUENCE)
		require attached el
		do
			process_sequence (el)
		end

	process_divide_compound (el: QFIS_DIVIDE_COMPOUND)
		require attached el
		do
			process_arithmetic_compound (el)
		end

	process_exist_formula (el: QFIS_EXIST_FORMULA)
		require attached el
		do
			process_quantified_formula (el)
		end

	process_forall_formula (el: QFIS_FORALL_FORMULA)
		require attached el
		do
			process_quantified_formula (el)
		end

	process_formula (el: QFIS_FORMULA)
		require attached el
		do
		end

	process_function (el: QFIS_FUNCTION)
		require attached el
		do
			el.arguments.process (Current)
		end

	process_function_declaration (el: QFIS_FUNCTION_DECLARATION)
		require attached el
		do
			el.domain.process (Current)
		end

	process_havoc (el: QFIS_HAVOC)
		require attached el
		do
			el.target.process (Current)
		end

	process_iff_formula (el: QFIS_IFF_FORMULA)
		require attached el
		do
			process_boolean_formula (el)
		end

	process_implies_formula (el: QFIS_IMPLIES_FORMULA)
		require attached el
		do
			process_boolean_formula (el)
		end

	process_integer_term (el: QFIS_INTEGER_TERM)
		require attached el
		do
		end

	process_integer_function (el: QFIS_INTEGER_FUNCTION)
		require attached el
		do
			el.arguments.process (Current)
		end

	process_integer_variable (el: QFIS_INTEGER_VARIABLE)
		require attached el
		do
		end

	process_length (el: QFIS_LENGTH)
		require attached el
		do
			el.target.process (Current)
		end

	process_list_assertions (el: QFIS_LIST_ASSERTIONS)
		require attached el
		do
			from el.start
			until el.after
			loop
				el.item.process (Current)
				el.forth
			end
		end

	process_list_declarations (el: QFIS_LIST_DECLARATIONS)
		require attached el
		do
			el.booleans.process (Current)
			el.sequences.process (Current)
			el.integers.process (Current)
		end

	process_list_variables (el: QFIS_LIST_VARIABLES)
		require attached el
		do
		end

	process_local (el: QFIS_LOCAL)
		require attached el
		do
			process_list_declarations (el)
		end

	process_loop (el: QFIS_LOOP)
		require attached el
		do
			if not el.nondeterministic then
				el.exit.process (Current)
			end
			el.loop_invariant.process (Current)
			el.body.process (Current)
		end

	process_minus_compound (el: QFIS_MINUS_COMPOUND)
		require attached el
		do
			process_arithmetic_compound (el)
		end

	process_not_formula (el: QFIS_NOT_FORMULA)
		require attached el
		do
			el.target.process (Current)
		end

	process_old_formula (el: QFIS_OLD_FORMULA)
		require attached el
		do
			el.target.process (Current)
		end

	process_old_sequence (el: QFIS_OLD_SEQUENCE)
		require attached el
		do
			el.target.process (Current)
		end

	process_or_formula (el: QFIS_OR_FORMULA)
		require attached el
		do
			process_boolean_formula (el)
		end

	process_plus_compound (el: QFIS_PLUS_COMPOUND)
		require attached el
		do
			process_arithmetic_compound (el)
		end

	process_position_integer_function (el: QFIS_POSITION_INTEGER_FUNCTION)
		require attached el
		do
			el.target.process (Current)
		end

	process_predicate_formula (el: QFIS_PREDICATE_FORMULA)
		require attached el
		do
			el.arguments.process (Current)
		end

	process_quantified_formula (el: QFIS_QUANTIFIED_FORMULA)
		require attached el
		do
			el.locals.process (Current)
			el.target.process (Current)
		end

	process_range (el: QFIS_RANGE)
		require attached el
		do
			el.sequence.process (Current)
		end

	process_regexp (el: QFIS_REGEXP)
		require attached el
		do
		end

	process_regexp_constraint (el: QFIS_REGEXP_CONSTRAINT)
		require attached el
		do
			el.sequence.process (Current)
			el.regexp.process (Current)
		end

	process_re_and (el: QFIS_RE_AND)
		require attached el
		do
			process_re_boolean (el)
		end

	process_re_atom (el: QFIS_RE_ATOM)
		require attached el
		do
		end

	process_re_boolean (el: QFIS_RE_BOOLEAN)
		require attached el
		do
			process_re_compound (el)
		end

	process_re_cat (el: QFIS_RE_CAT)
		require attached el
		do
			process_re_compound (el)
		end

	process_re_character (el: QFIS_RE_CHARACTER)
		require attached el
		do
			process_re_atom (el)
		end

	process_re_compound (el: QFIS_RE_COMPOUND)
		require attached el
		do
			el.left.process (Current)
			el.right.process (Current)
		end

	process_re_epsilon (el: QFIS_RE_EPSILON)
		require attached el
		do
			process_re_atom (el)
		end

	process_re_integer (el: QFIS_RE_INTEGER)
		require attached el
		do
			process_re_atom (el)
		end

	process_re_not (el: QFIS_RE_NOT)
		require attached el
		do
			process_re_unary (el)
		end

	process_re_or (el: QFIS_RE_OR)
		require attached el
		do
			process_re_boolean (el)
		end

	process_re_plus (el: QFIS_RE_PLUS)
		require attached el
		do
			process_re_unary (el)
		end

	process_re_star (el: QFIS_RE_STAR)
		require attached el
		do
			process_re_unary (el)
		end

	process_re_unary (el: QFIS_RE_UNARY)
		require attached el
		do
			el.target.process (Current)
		end

	process_re_universe (el: QFIS_RE_UNIVERSE)
		require attached el
		do
			process_re_atom (el)
		end

	process_root (el: QFIS_ROOT)
		require attached el
		do
			el.root.process (Current)
		end

	process_routine (el: QFIS_ROUTINE)
		require attached el
		do
			el.signature.process (Current)
			el.precondition.process (Current)
			el.modify.process (Current)
			el.locals.process (Current)
			el.body.process (Current)
			el.postcondition.process (Current)
		end

	process_sequence (el: QFIS_SEQUENCE)
		require attached el
		do
		end

	process_sequence_comparison (el: QFIS_SEQUENCE_COMPARISON)
		require attached el
		do
			el.left.process (Current)
			el.right.process (Current)
		end

	process_sequence_empty (el: QFIS_SEQUENCE_EMPTY)
		require attached el
		do
		end

	process_sequence_eq (el: QFIS_SEQUENCE_EQ)
		require attached el
		do
			process_sequence_comparison (el)
		end

	process_sequence_neq (el: QFIS_SEQUENCE_NEQ)
		require attached el
		do
			process_sequence_comparison (el)
		end

	process_sequence_singleton (el: QFIS_SEQUENCE_SINGLETON)
		require attached el
		do
			el.target.process (Current)
		end

	process_signature (el: QFIS_SIGNATURE)
		require attached el
		do
			el.input.process (Current)
			el.output.process (Current)
		end

	process_skip (el: QFIS_SKIP)
		require attached el
		do
		end

	process_split (el: QFIS_SPLIT)
		require attached el
		do
			el.sequence.process (Current)
			el.into.process (Current)
		end

	process_times_compound (el: QFIS_TIMES_COMPOUND)
		require attached el
		do
			process_arithmetic_compound (el)
		end

	process_variable (el: QFIS_VARIABLE)
		require attached el
		do
		end

end
