-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

note
	description: "Iterator modifying QFIS_FORMULA."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	QFIS_ITERATOR_MODIFIER

inherit
	QFIS_ITERATOR
	redefine
		process_arguments, process_arithmetic_comparison, process_arithmetic_compound, process_boolean_formula, process_compound, process_function,
		process_integer_function, process_not_formula, process_length, process_position_integer_function, process_range, process_regexp_constraint,
		process_re_compound, process_re_unary, process_sequence_comparison, process_sequence_singleton, process_root
	end

create
	make


feature -- Node processing

	process_arguments (el: QFIS_ARGUMENTS)
		local c: INTEGER
		do
			from el.start
			until el.after
			loop
				c := replacements.count
				el.item.process (Current)
				if replacements.count > c then
					check replacements.count = c + 1 end
					if attached {QFIS_ARGUMENT} replacements.item as it then
						el.replace (it)
					else
						debug
							check False end
						end
					end
					replacements.remove
				end
				el.forth
			end
		end

	process_arithmetic_comparison (el: QFIS_ARITHMETIC_COMPARISON)
		local c: INTEGER
		do
			c := replacements.count
			el.left.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_INTEGER_TERM} replacements.item as it then
					el.set (it, el.right)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end

			c := replacements.count
			el.right.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_INTEGER_TERM} replacements.item as it then
					el.set (el.left, it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_arithmetic_compound (el: QFIS_ARITHMETIC_COMPOUND)
		local c: INTEGER
		do
			c := replacements.count
			el.left.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_INTEGER_TERM} replacements.item as it then
					el.set (it, el.right)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end

			c := replacements.count
			el.right.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_INTEGER_TERM} replacements.item as it then
					el.set (el.left, it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_boolean_formula (el: QFIS_BOOLEAN_FORMULA)
		local c: INTEGER
		do
			c := replacements.count
			el.left.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_FORMULA} replacements.item as it then
					el.set (it, el.right)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end

			c := replacements.count
			el.right.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_FORMULA} replacements.item as it then
					el.set (el.left, it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_compound (el: QFIS_COMPOUND)
		local c: INTEGER
		do
			c := replacements.count
			el.left.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_SEQUENCE} replacements.item as it then
					el.set (it, el.right)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end

			c := replacements.count
			el.right.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_SEQUENCE} replacements.item as it then
					el.set (el.left, it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_function (el: QFIS_FUNCTION)
		local
			c: INTEGER
		do
			from el.arguments.start
			until el.arguments.after
			loop
				c := replacements.count
				el.arguments.item.process (Current)
				if replacements.count > c then
					check replacements.count = c + 1 end
					if attached {QFIS_ARGUMENT} replacements.item as it then
						el.arguments.replace (it)
					else
						debug
							check False end
						end
					end
					replacements.remove
				end
				el.arguments.forth
			end
		end

	process_integer_function (el: QFIS_INTEGER_FUNCTION)
		local
			c: INTEGER
			var: QFIS_INTEGER_VARIABLE
			pos: QFIS_POSITION_INTEGER_FUNCTION
		do
			create var.make (el.function)
			c := replacements.count
			var.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_INTEGER_VARIABLE} replacements.item as v then
					el.set (v.identifier, el.arguments)
					replacements.remove
				elseif attached {QFIS_SEQUENCE} replacements.item as s then
					if el.is_position then
						create pos.make (s, el.position)
						replacements.remove
						replacements.extend (pos)
					else
						replacements.remove
					end
				else
					replacements.remove
				end
			end

			from el.arguments.start
			until el.arguments.after
			loop
				c := replacements.count
				el.arguments.item.process (Current)
				if replacements.count > c then
					check replacements.count = c + 1 end
					if attached {QFIS_ARGUMENT} replacements.item as it then
						el.arguments.replace (it)
					else
						debug
							check False end
						end
					end
					replacements.remove
				end
				el.arguments.forth
			end
		end

	process_length (el: QFIS_LENGTH)
		local c: INTEGER
		do
			c := replacements.count
			el.target.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_SEQUENCE} replacements.item as it then
					el.set (it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_not_formula (el: QFIS_NOT_FORMULA)
		local c: INTEGER
		do
			c := replacements.count
			el.target.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_FORMULA} replacements.item as it then
					el.set (it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_position_integer_function (el: QFIS_POSITION_INTEGER_FUNCTION)
		local c: INTEGER
		do
			c := replacements.count
			el.target.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_SEQUENCE} replacements.item as it then
					el.set (it, el.position)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_range (el: QFIS_RANGE)
		local c: INTEGER
		do
			c := replacements.count
			el.sequence.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_SEQUENCE} replacements.item as it then
					el.set (it, el.lower, el.upper)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_regexp_constraint (el: QFIS_REGEXP_CONSTRAINT)
		local c: INTEGER
		do
			c := replacements.count
			el.sequence.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_SEQUENCE} replacements.item as it then
					el.set (it, el.regexp, el.in)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end

			c := replacements.count
			el.regexp.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_REGEXP} replacements.item as it then
					el.set (el.sequence, it, el.in)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end


	process_re_compound (el: QFIS_RE_COMPOUND)
		local c: INTEGER
		do
			c := replacements.count
			el.left.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_REGEXP} replacements.item as it then
					el.set (it, el.right)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end

			c := replacements.count
			el.right.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_REGEXP} replacements.item as it then
					el.set (el.left, it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_re_unary (el: QFIS_RE_UNARY)
		local c: INTEGER
		do
			c := replacements.count
			el.target.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_REGEXP} replacements.item as it then
					el.set (it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_sequence_comparison (el: QFIS_SEQUENCE_COMPARISON)
		local c: INTEGER
		do
			c := replacements.count
			el.left.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_SEQUENCE} replacements.item as it then
					el.set (it, el.right)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end

			c := replacements.count
			el.right.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_SEQUENCE} replacements.item as it then
					el.set (el.left, it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_sequence_singleton (el: QFIS_SEQUENCE_SINGLETON)
		local c: INTEGER
		do
			c := replacements.count
			el.target.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_INTEGER_TERM} replacements.item as it then
					el.set (it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end

	process_root (el: QFIS_ROOT)
		local c: INTEGER
		do
			c := replacements.count
			el.root.process (Current)
			if replacements.count > c then
				check replacements.count = c + 1 end
				if attached {QFIS_FORMULA} replacements.item as it then
					el.set (it)
				else
					debug
						check False end
					end
				end
				replacements.remove
			end
		end


feature {QFIS_ITERATOR}

	replacements: LINKED_STACK [QFIS_PRINTABLE]


feature {NONE}

	make
		do
			create replacements.make
		end
end
