-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_SYMBOL_TABLE

create
	make

feature

	writable_locals: HASH_TABLE [HASH_TABLE [STRING, STRING], STRING]
		-- RoutineName --> (VariableId --> Type)

	all_locals: HASH_TABLE [HASH_TABLE [STRING, STRING], STRING]
		-- RoutineName --> (VariableId --> Type)

	globals: HASH_TABLE [STRING, STRING]
		-- VariableId --> Type

	old_variables: HASH_TABLE [LINKED_SET [QFIS_ARGUMENT], STRING]
		-- RoutineName --> QFIS_VARIABLE

	modify: HASH_TABLE [LINKED_LIST [STRING], STRING]
		-- RoutineName --> [VariableId, VariableId, ...] in modify clause

	global_functions: HASH_TABLE [LINKED_LIST [STRING], STRING]
		-- FunctionName --> [Codomain, Domain1, Domain2, ...]

	global_functions_with_declarations: HASH_TABLE [STRING, STRING]
		-- FunctionName --> Declaration file name

	routines: LINKED_SET [STRING]
		-- All names of routines

	axioms: LINKED_LIST [QFIS_FORMULA]
		-- All axioms

	routine_domains: HASH_TABLE [LINKED_LIST [STRING], STRING]
		-- RoutineName --> [Domain1, Domain2, ...]

	routine_signature_domain: HASH_TABLE [LINKED_LIST [STRING], STRING]
		-- RoutineName --> [formal1, formal2, ...]

	routine_signature_codomain: HASH_TABLE [LINKED_LIST [STRING], STRING]
		-- RoutineName --> [formal1, formal2, ...]

	routine_codomains: HASH_TABLE [LINKED_LIST [STRING], STRING]
		-- RoutineName --> [Codomain1, Codomain2, ...]

	routine_preconditions: HASH_TABLE [QFIS_PRECONDITION, STRING]

	routine_postconditions: HASH_TABLE [QFIS_POSTCONDITION, STRING]

	errors: LINKED_LIST [STRING]

	current_routine: STRING assign set_routine
		-- Defines the scope

	set_routine (r: STRING)
		require routines.has (r)
		do
			current_routine := r
		end


feature

	is_writable (v: STRING): BOOLEAN
		-- Is variable `v' writable in the current scope?
		do
			Result := writable_locals.at (current_routine).has (v) or (globals.has (v) and modify.at (current_routine).has (v))
		end

	is_readable (v: STRING): BOOLEAN
			-- Is variable `v' readable in the current scope?
		do
			Result := all_locals.at (current_routine).has (v) or globals.has (v)
		end

	is_public (v: STRING): BOOLEAN
			-- Is variable `v' public in the current scope?
		do
			Result := globals.has (v) or routine_signature_domain.at (current_routine).has (v) or routine_signature_codomain.at (current_routine).has (v)
		ensure
			Result implies is_readable (v)
		end

	type_of_var (v: STRING): STRING
			-- Type of variable `v' in the current scope
		require is_readable (v)
		do
			if all_locals.at (current_routine).has (v) then
				Result := all_locals.at (current_routine).at (v)
			else
				Result := globals.at (v)
			end
		end

	type_of_domain (routine: STRING; n: INTEGER): STRING
			-- Type of n-th argument of `routine''s domain
			-- Empty string if the routine or the argument don't exist
		do
			if routines.has (routine) and then routine_domains.at (routine).count >= n then
				Result := routine_domains.at (routine).i_th (n)
			else
				create Result.make_empty
			end
		end

	type_of_codomain (routine: STRING; n: INTEGER): STRING
			-- Type of n-th argument of `routine''s codomain
			-- Empty string if the routine or the argument don't exist
		do
			if routines.has (routine) and then routine_codomains.at (routine).count >= n then
				Result := routine_codomains.at (routine).i_th (n)
			else
				create Result.make_empty
			end
		end



feature {NONE} -- Initialization

	make
		do
			create writable_locals.make (1)
			writable_locals.compare_objects
			create all_locals.make (1)
			all_locals.compare_objects
			create globals.make (1)
			globals.compare_objects
			create modify.make (1)
			modify.compare_objects
			create global_functions_with_declarations.make (1)
			global_functions_with_declarations.compare_objects
			create global_functions.make (1)
			global_functions.compare_objects
			create axioms.make
			axioms.compare_objects
			create routines.make
			routines.compare_objects
			create routine_domains.make (1)
			routine_domains.compare_objects
			create routine_codomains.make (1)
			routine_codomains.compare_objects
			create routine_preconditions.make (1)
			routine_preconditions.compare_objects
			create routine_postconditions.make (1)
			routine_postconditions.compare_objects
			create routine_signature_domain.make (1)
			routine_signature_domain.compare_objects
			create routine_signature_codomain.make (1)
			routine_signature_codomain.compare_objects
			create errors.make
			create old_variables.make (1)
			old_variables.compare_objects
		end

end
