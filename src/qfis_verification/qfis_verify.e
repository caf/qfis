-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_VERIFY

create
	make_with_file, make_with_string

feature

	parse
		require
			attached parser
		do
			if attached parser.parsed_program as prg then
				program := prg
			end
			if not attached program or else parser.error_count > 0 then
				parsed := False
				parse_errors := parser.error_messages
			else
				parsed := True
			end
		end

	parsed: BOOLEAN attribute ensure parsed implies attached program end

	parse_errors: LINKED_LIST [STRING]


	typecheck
		require parsed
		local
			st_builder: QFIS_SYMBOL_TABLE_BUILDER
			tc_builder: QFIS_TYPECHECK
		do
			create st_builder.make
			program.process (st_builder)
			if not attached st_builder.symbols then
				typechecked := False
				create typecheck_errors.make
				typecheck_errors.extend ("Could not generate the symbol table.")
			else
				symbol_table := st_builder.symbols
				if attached symbol_table.errors and then symbol_table.errors.count > 0 then
					typechecked := False
					typecheck_errors := symbol_table.errors
				else
					create tc_builder.make_with_symbols (symbol_table)
					program.process (tc_builder)
					if attached tc_builder.errors and then tc_builder.errors.count > 0 then
						typechecked := False
						typecheck_errors := tc_builder.errors
					else
						typechecked := True
					end
				end
			end
		end

	typechecked: BOOLEAN attribute ensure typechecked implies attached symbol_table and parsed end

	typecheck_errors: LINKED_LIST [STRING]


	verify
		require typechecked
		do
			vc_num := 0
			create verification_errors.make (3)
			from program.start
			until program.after
			loop
				if attached {QFIS_ROUTINE} program.item as a_routine then
					verification_errors.put (create {LINKED_LIST [STRING]}.make, a_routine.signature.name)
					symbol_table.set_routine (a_routine.signature.name)
					verify_routine (a_routine)
				end
				program.forth
			end
		end

	verified: BOOLEAN
		do
			if attached verification_errors then
				Result := True
				from verification_errors.start
				until verification_errors.after or not Result
				loop
					if not verification_errors.item_for_iteration.is_empty then
						Result := False
					end
					verification_errors.forth
				end
			else
				Result := False
			end
		end


	smoke
		require typechecked
		local vc: QFIS_VC
		do
			create vc.make
			vc.put_prove (create {QFIS_BOOLEAN_CONSTANT}.make (False))
			vc.description.append ("Smoke test: try to prove False from the axioms.")
			create verification_errors.make (3)
			verification_errors.put (create {LINKED_LIST [STRING]}.make, "SMOKE_TEST")
			validate_vc (vc, "SMOKE_TEST")
			smoked := not verification_errors.at ("SMOKE_TEST").is_empty
		end

	smoked: BOOLEAN

	verification_errors: HASH_TABLE [LINKED_LIST [STRING], STRING]
		-- Routine names --> list of errors
		-- An empty list denotes a routine successfully verified



feature -- Services

	declarations_directory: STRING assign set_declarations_directory

	set_declarations_directory (d: STRING)
		require
			attached d
			not d.is_empty
		do
			declarations_directory := d
		end

	work_directory: STRING assign set_work_directory

	set_work_directory (d: STRING)
		require
			attached d
			not d.is_empty
		do
			work_directory := d
			if work_directory.ends_with ("/") then
				work_directory.remove_tail (1)
			end
		end

	timeout: INTEGER assign set_timeout

	set_timeout (t: INTEGER)
		do
			timeout := t
		end

	parser: QFIS_PL_PARSER

	program: QFIS_PROGRAM

	symbol_table: QFIS_SYMBOL_TABLE

	fresh_variables: QFIS_VARIABLE_DISPENSER

	verify_routine (a_routine: QFIS_ROUTINE)
		do
			if not attached fresh_variables then
				create fresh_variables.make (Void, 0)
			end
			a_routine.set_fresh (fresh_variables)
			a_routine.set_symbols (symbol_table)
			a_routine.generate_vc
			from a_routine.verification_conditions.start
			until a_routine.verification_conditions.after
			loop
				validate_vc (a_routine.verification_conditions.item, a_routine.signature.name)
				a_routine.verification_conditions.forth
			end
		end



feature {QFIS_VERIFY} -- Implementation

	solver: QFIS_SOLVER

	vc_num: INTEGER

	default_filename_prefix: STRING = "__qfis_temp"

	top_filename: STRING

	validate_vc (vc: QFIS_VC; routine: STRING)
		require
			attached vc
			attached routine
			not routine.is_empty
		local
			filename: STRING
			error: STRING
		do
			create solver.make
			vc_num := vc_num + 1

			if attached work_directory then
				filename := work_directory + "/"
			else
				filename := "./"
			end
			filename.append (top_filename + default_filename_prefix + "_VC_" + vc_num.out + "_" + routine + ".cvc")

			solver.comment ("File " + filename)
			solver.comment ("Verification condition # " + vc_num.out + " of routine " + routine)
			solver.comment ("Verification condition description: " + vc.description)
			solver.comment ("")

			from symbol_table.axioms.start
			until symbol_table.axioms.after
			loop
				solver.assert (symbol_table.axioms.item)
				symbol_table.axioms.forth
			end

			from vc.assume.start
			until vc.assume.after
			loop
				solver.assert (vc.assume.item)
				solver.comment ("  ASSUME  " + vc.assume.item.as_text)
				vc.assume.forth
			end
			from vc.prove.start
			until vc.prove.after
			loop
				solver.query (vc.prove.item)
				solver.comment ("  PROVE  " + vc.prove.item.as_text)
				vc.prove.forth
			end

			from symbol_table.global_functions_with_declarations.start
			until symbol_table.global_functions_with_declarations.after
			loop
				solver.add_declaration_file (symbol_table.global_functions_with_declarations.item_for_iteration,
												symbol_table.global_functions_with_declarations.key_for_iteration)
				symbol_table.global_functions_with_declarations.forth
			end

			if attached declarations_directory then
				solver.set_declarations_dir (declarations_directory)
			end

			solver.set_work_file (filename)
			solver.set_timeout (timeout)

			solver.solve
			if solver.success > 0 then
				if attached vc.description and then not vc.description.is_empty then
					 error := "Could not verify " + vc.description
				else
					error := "Could not verify VC # " + vc_num.out
				end
				if vc.prove.valid_index (solver.success) then
					if attached vc.prove.at (solver.success).annotation as an then
						error.append (" -- " + an)
					end
					error.append (" -- annotation on line " + vc.prove.at (solver.success).line_number.out)
				end
				verification_errors.at (routine).extend (error)
			end
		end




feature {NONE} -- Initialization

	make_with_file (filename: STRING)
		require
			attached filename
			not filename.is_empty
		do
			top_filename := filename
			create parser.make (filename)
		end

	make_with_string (str: STRING)
		require
			attached str
			not str.is_empty
		do
			top_filename := ""
			create parser.make_with_string (str)
		end

end
