-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_VARIABLE_DISPENSER

inherit
	QFIS_LIST_DECLARATIONS
	rename make as make_empty end

create
	make


feature

	prefix_name: STRING assign set_prefix

	set_prefix (pre: STRING)
		-- If `pre' does not start with an alphabetic character, use a default string instead
		do
			if attached pre and then not pre.is_empty and then pre.at (1).is_alpha then
				if pre.at (1).is_upper then
					create prefix_name.make (pre.count)
					prefix_name.extend (pre.at (1).as_lower)
					prefix_name.append (pre.substring (2, pre.count))
				else
					create prefix_name.make_from_string (pre)
				end
			else
				create prefix_name.make_from_string ("fresh_")
			end
		end

	index: INTEGER assign set_index
		-- Numeric id of next variable generated

	set_index (i: INTEGER)
		-- If `i <= 0', set `index' to 1 instead
		do
			if i <= 0 then
				index := 1
			else
				index := i
			end
		end

	index_forth
		do
			index := index + 1
		end

	index_string: STRING
		local fm: FORMAT_INTEGER
		do
			create fm.make (2)
			fm.zero_fill
			fm.right_justify
			fm.sign_ignore
			Result := fm.formatted (index)
		ensure attached Result and then not Result.is_empty
		end

	fresh_boolean
		-- Generate a fresh boolean variable. The new variable is in `item'
		local id: STRING
		do
			create id.make_from_string (prefix_name + index_string + "?")
			create {QFIS_BOOLEAN_VARIABLE} item.make (id)
			add_boolean (id)
			index_forth
		ensure
			attached {QFIS_BOOLEAN_VARIABLE} item
		end

	fresh_integer
		-- Generate a fresh integer variable. The new variable is in `item'
		local id: STRING
		do
			create id.make_from_string (prefix_name + index_string)
			create {QFIS_INTEGER_VARIABLE} item.make (id)
			add_integer (id)
			index_forth
		ensure
			attached {QFIS_INTEGER_VARIABLE} item
		end

	fresh_sequence
		-- Generate a fresh integer variable. The new variable is in `item'
		local id: STRING
		do
			create id.make_from_string (prefix_name + index_string)
			id.put (prefix_name.at (1).as_upper, 1)
			create {QFIS_VARIABLE} item.make (id)
			add_sequence (id)
			index_forth
		ensure
			attached {QFIS_VARIABLE} item
		end

	item: QFIS_ARGUMENT
		-- Last fresh variable generated

feature {NONE} -- Initialization

	make (pre: STRING; initial_index: INTEGER)
		-- Generate variables with name beginning with `pre' and starting with index `initial_index'
		-- If `pre' does not start with an alphabetic character, use a default string instead
		-- If `initial_index <= 0', start with index 1 instead
		do
			make_empty
			set_prefix (pre)
			set_index (initial_index)
		end


invariant
	attached prefix_name
	not prefix_name.is_empty
	prefix_name.at (1).is_lower
end
