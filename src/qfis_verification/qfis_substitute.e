-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_SUBSTITUTE [G -> {QFIS_PRINTABLE, QFIS_VISITABLE}]

inherit
	QFIS_ITERATOR_MODIFIER
	rename make as make_modifier
	redefine process_variable, process_integer_variable, process_boolean_variable end

create
	make

feature -- Node processing

	process_variable (el: QFIS_VARIABLE)
		do
			if el.identifier.is_equal (replace) then
				replacements.extend (into)
			end
		end

	process_integer_variable (el: QFIS_INTEGER_VARIABLE)
		do
			if el.identifier.is_equal (replace) then
				replacements.extend (into)
			end
		end

	process_boolean_variable (el: QFIS_BOOLEAN_VARIABLE)
		do
			if el.identifier.is_equal (replace) then
				replacements.extend (into)
			end
		end



feature {NONE} -- Initialization

	replace: STRING

	into: G

	make (a_replace: STRING; an_into: G)
		-- Replace every occurrence of variable with name `a_replace' with the term `an_into'
		require
			attached a_replace
			not a_replace.is_empty
			attached an_into
		do
			make_modifier
			replace := a_replace
			into := an_into
		end

end
