-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_VC

create
	make

feature

	assume: LINKED_SET [QFIS_FORMULA]
	prove: LINKED_SET [QFIS_FORMULA]

	put_assume (f: QFIS_FORMULA)
		require attached f
		do
			assume.extend (f)
		end

	put_prove (f: QFIS_FORMULA)
		require attached f
		do
			prove.extend (f)
		end

	description: STRING
		-- Textual description of the verification condition

feature {NONE} -- Initialization

	make
		do
			create assume.make
			create prove.make
			create description.make_empty
		end

end
