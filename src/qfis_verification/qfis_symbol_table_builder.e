-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_SYMBOL_TABLE_BUILDER

inherit
	QFIS_ITERATOR
	redefine process_axiom, process_boolean_variable, process_function_declaration, process_integer_variable, process_list_declarations,
			 process_local, process_routine, process_signature, process_variable end

create
	make

feature -- Node processing

	process_axiom (el: QFIS_AXIOM)
		do
			symbols.axioms.extend (el.formula)
		end

	process_boolean_variable (el: QFIS_BOOLEAN_VARIABLE)
		do
			if el.is_old then
				symbols.old_variables.at (symbols.current_routine).put (el.deep_twin)
			end
		end

	process_function_declaration (el: QFIS_FUNCTION_DECLARATION)
		local signature: LINKED_LIST [STRING]
		do
			create signature.make
			signature.compare_objects
			signature.extend (el.codomain)
			from el.domain.start
			until el.domain.after
			loop
				signature.extend (el.domain.item)
				el.domain.forth
			end
			if symbols.global_functions.has (el.name) then
				symbols.errors.extend (el.lineof + "Function " + el.name + " declared more than once.")
			else
				symbols.global_functions.put (signature, el.name)
				if attached el.file_name and then not el.file_name.is_empty then
					symbols.global_functions_with_declarations.put (el.file_name, el.name)
				end
			end
		end

	process_integer_variable (el: QFIS_INTEGER_VARIABLE)
		do
			if el.is_old then
				symbols.old_variables.at (symbols.current_routine).put (el.deep_twin)
			end
		end

	process_list_declarations (el: QFIS_LIST_DECLARATIONS)
		do
			put_all (el, symbols.globals)
		end

	process_local (el: QFIS_LOCAL)
		do
			put_all (el, symbols.writable_locals.at (symbols.current_routine))
			put_all (el, symbols.all_locals.at (symbols.current_routine))
		end

	process_routine (el: QFIS_ROUTINE)
		do
			process_signature (el.signature)
			symbols.modify.put (el.modify, el.signature.name)
			el.locals.process (Current)
			symbols.routine_preconditions.put (el.precondition, el.signature.name)
			symbols.routine_postconditions.put (el.postcondition, el.signature.name)
			-- This is needed for old variables
			symbols.old_variables.put (create {LINKED_SET [QFIS_ARGUMENT]}.make, el.signature.name)
			process_list_assertions (el.postcondition)
		end

	process_signature (el: QFIS_SIGNATURE)
		do
			if symbols.routines.has (el.name) then
				symbols.errors.extend (el.lineof + "Routine " + el.name + " declared more than once.")
			else
				symbols.routines.extend (el.name)
				symbols.set_routine (el.name)
				symbols.writable_locals.put (create {HASH_TABLE [STRING, STRING]}.make (1), el.name)
				symbols.writable_locals.at (el.name).compare_objects
				symbols.all_locals.put (create {HASH_TABLE [STRING, STRING]}.make (1), el.name)
				symbols.all_locals.at (el.name).compare_objects

				symbols.routine_domains.put (el.input.types_list, el.name)
				symbols.routine_domains.at (el.name).compare_objects
				symbols.routine_codomains.put (el.output.types_list, el.name)
				symbols.routine_codomains.at (el.name).compare_objects

				put_all (el.input, symbols.all_locals.at (el.name))
				put_all (el.output, symbols.all_locals.at (el.name))
				put_all (el.output, symbols.writable_locals.at (el.name))

				symbols.routine_signature_domain.put (el.input.ordered, el.name)
				symbols.routine_signature_codomain.put (el.output.ordered, el.name)
			end
		end

	process_variable (el: QFIS_VARIABLE)
		do
			if el.is_old then
				symbols.old_variables.at (symbols.current_routine).put (el.deep_twin)
			end
		end


feature -- Result

	symbols: QFIS_SYMBOL_TABLE


feature {NONE} -- Initialization

	put_all (d: QFIS_LIST_DECLARATIONS; h: HASH_TABLE [STRING, STRING])
		-- Transfer all declarations `d' in `h'
		do
			from d.booleans.start
			until d.booleans.after
			loop
				if h.has (d.booleans.item) then
					symbols.errors.extend (d.lineof + "Variable with name " + d.booleans.item + " declared more than once.")
				end
				h.put ("BOOLEAN", d.booleans.item)
				d.booleans.forth
			end
			from d.integers.start
			until d.integers.after
			loop
				if h.has (d.integers.item) then
					symbols.errors.extend (d.lineof + "Variable with name " + d.integers.item + " declared more than once.")
				end
				h.put ("INTEGER", d.integers.item)
				d.integers.forth
			end
			from d.sequences.start
			until d.sequences.after
			loop
				if h.has (d.sequences.item) then
					symbols.errors.extend (d.lineof + "Variable with name " + d.sequences.item + " declared more than once.")
				end
				h.put ("SEQUENCE", d.sequences.item)
				d.sequences.forth
			end
		end

	make
		do
			create symbols.make
		end

end
