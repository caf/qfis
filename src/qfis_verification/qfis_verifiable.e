-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

deferred class
	QFIS_VERIFIABLE

inherit
	ANY
	undefine is_equal, copy end

feature

	pre: LINKED_SET [QFIS_FORMULA]
		-- Local precondition

	compute_pre
		-- Compute `pre' from `post' by backward substitution
		require
			attached post
			not post.is_empty
			attached fresh
			attached symbols
		deferred
		ensure
			attached pre
			not pre.is_empty
		end

	post: LINKED_SET [QFIS_FORMULA] assign set_post
		-- Local postcondition

	set_post (f: LINKED_SET [QFIS_FORMULA])
		require attached f
		do
			post := f
			if post.is_empty then
				post.extend (create {QFIS_BOOLEAN_CONSTANT}.make (True))
			end
		ensure
			attached post
			not post.is_empty
		end

	generate_vc
		require attached post and then not post.is_empty
		do
			create verification_conditions.make
		ensure attached verification_conditions
		end

	verification_conditions: LINKED_SET [QFIS_VC]

	fresh: QFIS_VARIABLE_DISPENSER assign set_fresh

	set_fresh (d: QFIS_VARIABLE_DISPENSER)
		require attached d
		do
			fresh := d
		ensure attached fresh
		end

	symbols: QFIS_SYMBOL_TABLE assign set_symbols

	set_symbols (s: QFIS_SYMBOL_TABLE)
		require attached s
		do
			symbols := s
		end

feature {QFIS_VERIFIABLE} -- Utilities

	build_substitute (v: STRING; v_prime: STRING; a_type: STRING): QFIS_ITERATOR_MODIFIER
		-- A substitutor iterator that replaces `v' --> `v_prime' according to `v''s type in the current context
		-- a_type denotes the type of the substituted: must be a valid type if neither `v' nor `v_prime' are readable
		-- If `v_prime' is Void or empty, generates a fresh variable
		local
			type: STRING
			it: QFIS_ARGUMENT
		do
			if attached a_type then
				type := a_type
			elseif attached v and then symbols.is_readable (v) then
				type := symbols.type_of_var (v)
			elseif attached v_prime and then symbols.is_readable (v_prime) then
				type := symbols.type_of_var (v_prime)
			elseif attached v then
				if fresh.booleans.has (v) then
					type := "BOOLEAN"
				elseif fresh.integers.has (v) then
					type := "INTEGER"
				elseif fresh.sequences.has (v) then
					type := "SEQUENCE"
				else
					debug
						check False end
					end
				end
			elseif attached v_prime then
				if fresh.booleans.has (v_prime) then
					type := "BOOLEAN"
				elseif fresh.integers.has (v_prime) then
					type := "INTEGER"
				elseif fresh.sequences.has (v_prime) then
					type := "SEQUENCE"
				else
					debug
						check False end
					end
				end
			else
				debug
					check False end
				end
			end

			if type.is_equal ("BOOLEAN") then
				if attached v_prime and then not v_prime.is_empty then
					create {QFIS_BOOLEAN_VARIABLE} it.make (v_prime)
				else
					fresh.fresh_boolean
					it := fresh.item
				end
				check attached {QFIS_BOOLEAN_VARIABLE} it as l_it then
					create {QFIS_SUBSTITUTE [QFIS_BOOLEAN_VARIABLE]} Result.make (v, l_it)
				end
			elseif type.is_equal ("INTEGER") then
				if attached v_prime and then not v_prime.is_empty then
					create {QFIS_INTEGER_VARIABLE} it.make (v_prime)
				else
					fresh.fresh_integer
					it := fresh.item
				end
				check attached {QFIS_INTEGER_VARIABLE} it as l_it then
					create {QFIS_SUBSTITUTE [QFIS_INTEGER_VARIABLE]} Result.make (v, l_it)
				end
			elseif type.is_equal ("SEQUENCE") then
				if attached v_prime and then not v_prime.is_empty then
					create {QFIS_VARIABLE} it.make (v_prime)
				else
					fresh.fresh_sequence
					it := fresh.item
				end
				check attached {QFIS_VARIABLE} it as l_it then
					create {QFIS_SUBSTITUTE [QFIS_VARIABLE]} Result.make (v, l_it)
				end
			else
				debug
					check False end
				end
			end
		end


end
