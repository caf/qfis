-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	QFIS_TYPECHECK

inherit
	QFIS_ITERATOR
	redefine process_assign_boolean, process_assign_integer, process_assign_sequence,
				process_boolean_variable, process_call, process_call_function, process_function, process_havoc,
				process_integer_function, process_integer_variable, process_local, process_quantified_formula, process_predicate_formula,
				process_regexp_constraint, process_routine, process_signature, process_split, process_variable end

create
	make_with_symbols

feature -- Result

	errors: LINKED_LIST [STRING]


feature  -- Node processing

	process_assign_boolean (el: QFIS_ASSIGN_BOOLEAN)
		do
			Precursor (el)
			if not symbols.is_writable (el.target) then
				errors.extend (el.lineof + "Variable " + el.target + " is not writable.")
			end
		end

	process_assign_integer (el: QFIS_ASSIGN_INTEGER)
		do
			Precursor (el)
			if not symbols.is_writable (el.target) then
				errors.extend (el.lineof + "Variable " + el.target + " is not writable.")
			end
		end

	process_assign_sequence (el: QFIS_ASSIGN_SEQUENCE)
		do
			Precursor (el)
			if not symbols.is_writable (el.target) then
				errors.extend (el.lineof + "Variable " + el.target + " is not writable.")
			end
		end

	process_boolean_variable (el: QFIS_BOOLEAN_VARIABLE)
		do
			if not symbols.is_readable (el.identifier) and (in_global implies not scope.has (el.identifier)) then
				errors.extend (el.lineof + "Boolean variable " + el.identifier + " undeclared in the current scope.")
			end
			if public_only and not symbols.is_public (el.identifier) then
				errors.extend (el.lineof + "Variable " + el.identifier + " is not visible to clients.")
			end
			if not in_post and el.is_old then
				errors.extend (el.lineof + "Old syntax allowed only in postconditions.")
			end
			if in_post and el.is_old then
				el.identifier.prepend ("old_")
			end
		end

	process_call (el: QFIS_CALL)
		local
			ok: BOOLEAN
			mod_this, mod_called: LINKED_LIST [STRING]
		do
			if not symbols.routines.has (el.routine) then
				errors.extend (el.lineof + "Routine " + el.routine + " does not exist.")
			elseif symbols.routine_domains.at (el.routine).count /= el.arguments.count then
				errors.extend (el.lineof + "Call to routine " + el.routine + " uses an incorrect number of actual input arguments.")
			else
				ok := True
				from el.arguments.start
				until el.arguments.after or not ok
				loop
					ok := symbols.is_readable (el.arguments.item)
							and then symbols.type_of_var (el.arguments.item).is_equal (symbols.type_of_domain (el.routine, el.arguments.index))
					el.arguments.forth
				end
				if not ok then
					errors.extend (el.lineof + "Call to routine " + el.routine + " does not conform to input signature types.")
				end
				mod_called := symbols.modify.at (el.routine)
				mod_this := symbols.modify.at (symbols.current_routine)
				ok := True
				from mod_called.start
				until mod_called.after
				loop
					if not mod_this.has (mod_called.item) then
						errors.extend (el.lineof + "Called routine " + el.routine + " may modify " + mod_called.item + " but it is not listed in modify clause.")
					end
					mod_called.forth
				end
			end
		end

	process_call_function (el: QFIS_CALL_FUNCTION)
		local ok: BOOLEAN
		do
			process_call (el)
			ok := True
			from el.targets.start
			until el.targets.after or not ok
			loop
				ok := symbols.is_writable (el.targets.item)
						and then symbols.type_of_var (el.targets.item).is_equal (symbols.type_of_codomain (el.routine, el.targets.index))
				el.targets.forth
			end
			if not ok then
				errors.extend (el.lineof + "Call to routine " + el.routine + " does not conform to output signature types.")
			end
		end

	process_function (el: QFIS_FUNCTION)
		local
			ok: BOOLEAN
			it: QFIS_ARGUMENT
		do
			if not symbols.global_functions.has (el.function) then
				errors.extend (el.lineof + "Function " + el.function + " has not been declared.")
			elseif symbols.global_functions.at (el.function).count - 1 /= el.arguments.count then
				errors.extend (el.lineof + "Usage of function " + el.function + " does not conform to input signature.")
			else
				ok := True
				from el.arguments.start
				until el.arguments.after or not ok
				loop
					it := el.arguments.item
					it.process (Current)
					if attached {QFIS_SEQUENCE_SINGLETON} it then
						ok := symbols.global_functions.at (el.function).i_th (el.arguments.index + 1).is_equal ("INTEGER")
								or symbols.global_functions.at (el.function).i_th (el.arguments.index + 1).is_equal ("SEQUENCE")
					elseif attached {QFIS_SEQUENCE} it then
						ok := symbols.global_functions.at (el.function).i_th (el.arguments.index + 1).is_equal ("SEQUENCE")
					elseif attached {QFIS_FORMULA} it then
						ok := symbols.global_functions.at (el.function).i_th (el.arguments.index + 1).is_equal ("BOOLEAN")
					end
					el.arguments.forth
				end
				if not ok then
					errors.extend (el.lineof + "Usage of function " + el.function + " does not conform to input signature types.")
				end
			end
		end

	process_havoc (el: QFIS_HAVOC)
		local ok: BOOLEAN
		do
			ok := True
			from el.target.start
			until el.target.after or not ok
			loop
				ok := symbols.is_writable (el.target.item)
				el.target.forth
			end
			if not ok then
				el.target.go_i_th (el.target.index - 1)
				errors.extend (el.lineof + "Variable " + el.target.item + " is not writable.")
			end
		end

	process_integer_function (el: QFIS_INTEGER_FUNCTION)
		local f: QFIS_FUNCTION
		do
			if not el.is_position then
				create f.make (el.function, el.arguments)
				f.line_number := el.line_number
				f.process (Current)
			else
				if not symbols.is_readable (el.function) and (in_global implies not scope.has (el.function)) then
					errors.extend (el.lineof + "Invalid position: variable " + el.function + " undeclared in the current scope.")
				elseif (scope.has (el.function) and then not scope.at (el.function).is_equal ("SEQUENCE"))
						or else (not scope.has (el.function) and then not symbols.type_of_var (el.function).is_equal ("SEQUENCE")) then
					errors.extend (el.lineof + "Invalid position: " + el.function + " should be of type SEQUENCE.")
				end
			end
		end

	process_integer_variable (el: QFIS_INTEGER_VARIABLE)
		do
			if not symbols.is_readable (el.identifier) and (in_global implies not scope.has (el.identifier)) then
				errors.extend (el.lineof + "Integer variable " + el.identifier + " undeclared in the current scope.")
			end
			if public_only and not symbols.is_public (el.identifier) then
				errors.extend (el.lineof + "Variable " + el.identifier + " is not visible to clients.")
			end
			if not in_post and el.is_old then
				errors.extend (el.lineof + "Old syntax allowed only in postconditions.")
			end
			if in_post and el.is_old then
				el.identifier.prepend ("old_")
			end
		end

	process_local (el: QFIS_LOCAL)
		do
			from el.booleans.start
			until el.booleans.after
			loop
				if symbols.globals.has (el.booleans.item) then
					errors.extend (el.lineof + "Boolean variable " + el.booleans.item + " already deaclared as global.")
				end
				el.booleans.forth
			end
			from el.integers.start
			until el.integers.after
			loop
				if symbols.globals.has (el.integers.item) then
					errors.extend (el.lineof + "Integer variable " + el.integers.item + " already deaclared as global.")
				end
				el.integers.forth
			end
			from el.sequences.start
			until el.sequences.after
			loop
				if symbols.globals.has (el.sequences.item) then
					errors.extend (el.lineof + "Sequence variable " + el.sequences.item + " already deaclared as global.")
				end
				el.sequences.forth
			end
		end

	process_predicate_formula (el: QFIS_PREDICATE_FORMULA)
		local f: QFIS_FUNCTION
		do
			create f.make (el.predicate, el.arguments)
			f.line_number := el.line_number
			f.process (Current)
		end

	process_quantified_formula (el: QFIS_QUANTIFIED_FORMULA)
		local
			d: QFIS_LIST_VARIABLES
			t: LINKED_LIST [STRING]
		do
			check in_global end
			d := el.locals.ordered
			t := el.locals.types_list
			from
				d.start
				t.start
			until
				d.after
			loop
				if scope.has (d.item) or symbols.globals.has (d.item) then
					errors.extend (el.lineof + "Quantified variable " + d.item + " already declared in the current scope.")
				else
					scope.put (t.item, d.item)
				end
				d.forth
				t.forth
			end
			Precursor (el)
			from d.start
			until d.after
			loop
				scope.remove (d.item)
				d.forth
			end
		end

	process_regexp_constraint (el: QFIS_REGEXP_CONSTRAINT)
		do
			errors.extend (el.lineof + "Regular expression constraints currently unsupported.")
		end

	process_routine (el: QFIS_ROUTINE)
		do
			in_global := False
			el.signature.process (Current)
			public_only := True
			el.precondition.process (Current)
			public_only := False
			el.modify.process (Current)
			el.locals.process (Current)
			el.body.process (Current)
			public_only := True
			in_post := True
			el.postcondition.process (Current)
			in_post := False
			public_only := False
			from el.modify.start
			until el.modify.after
			loop
				if not symbols.globals.has (el.modify.item) then
					errors.extend (el.lineof + "Variable " + el.modify.item + " is not declared as global.")
				end
				el.modify.forth
			end
			in_global := True
		end

	process_signature (el: QFIS_SIGNATURE)
		do
			symbols.set_routine (el.name)
		end


	process_split (el: QFIS_SPLIT)
		do
			el.sequence.process (Current)
			from el.into.start
			until el.into.after
			loop
				if not symbols.is_writable (el.into.item) then
					errors.extend (el.lineof + "Variable " + el.into.item + " is not writable.")
				end
				el.into.forth
			end
		end

	process_variable (el: QFIS_VARIABLE)
		do
			if not symbols.is_readable (el.identifier) and (in_global implies not scope.has (el.identifier)) then
				errors.extend (el.lineof + "Sequence variable " + el.identifier + " undeclared in the current scope.")
			end
			if public_only and not symbols.is_public (el.identifier) then
				errors.extend (el.lineof + "Variable " + el.identifier + " is not visible to clients.")
			end
			if not in_post and el.is_old then
				errors.extend (el.lineof + "Old syntax allowed only in postconditions.")
			end
			if in_post and el.is_old then
				el.identifier.prepend ("OLD_")
			end
		end


feature {NONE} -- Initialization

	public_only: BOOLEAN
		-- Accept only public items

	in_post: BOOLEAN
		-- Processing postcondition

	symbols: QFIS_SYMBOL_TABLE

	scope: HASH_TABLE [STRING, STRING]
		-- VariableId --> Type

	in_global: BOOLEAN

	make_with_symbols (s: QFIS_SYMBOL_TABLE)
		do
			create errors.make
			symbols := s
			create scope.make (2)
			scope.compare_objects
			in_global := True
		end



end
