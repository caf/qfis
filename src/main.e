-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class
	MAIN

inherit
	ARGUMENTS

create
	make


feature {MAIN} -- Attributes

	verifier: QFIS_VERIFY

	file_name: STRING

	help: BOOLEAN
	help_flag: STRING = "h"

	smoke: BOOLEAN
	smoke_flag: STRING = "s"

	work_directory: STRING
	work_directory_flag: STRING = "w="
	default_work_directory: STRING = "."

	declarations_directory: STRING
	declarations_directory_flag: STRING = "d="
	decl_env_var_name: STRING = "QFIS_PRELUDE"

	decl_env_var: STRING
		-- Value of the environment variable `decl_env_var_name'
		-- Void if unset
		local
			ph: PROCESS_HELPER
			cl: STRING
			ee: EXECUTION_ENVIRONMENT
		do
			create ee
			Result := ee.get (decl_env_var_name)
			-- create ph
			-- if not {PLATFORM}.is_windows then
			-- 	cl := "/bin/sh -c %'echo $" + decl_env_var_name + "%'"
			-- else
			-- 	cl := "echo %%" + decl_env_var_name + "%%"
			-- end
			-- Result := ph.output_from_program (cl, Void)
			if attached Result  then
				Result.left_adjust
				Result.right_adjust
				if Result.is_empty then
					Result := Void
				end
			end
		end

	timeout: INTEGER
	timeout_flag: STRING = "t="
	default_timeout: INTEGER = 10

	validate_only: BOOLEAN
	validate_only_flag: STRING = "v"

	help_notice: STRING
		do
			Result := "%N" + "Usage: " + Command_name + " [options] <input_file>" +
						"%N" + "where [options] is one or more of the following." +
						"%N%T" + "-" + help_flag + "%T%T" + "display this notice." +
						"%N%T" + "-" + smoke_flag + "%T%T" + "perform a smoke test (consistency of the axioms)." +
						"%N%T" + "-" + work_directory_flag + "<wdir>" + "%T" + "set <wdir> as the work directory (default: current dir)." +
						"%N%T" + "-" + declarations_directory_flag + "<ddir>" + "%T" + "set <ddir> as the declarations directory (default: "
			if attached decl_env_var and then not decl_env_var.is_empty then
				Result.append (decl_env_var +  "-- $" + decl_env_var_name + " is set")
			else
				Result.append ("work dir -- $" + decl_env_var_name + " is unset")
			end
			Result.append (")." +
							"%N%T" + "-" + timeout_flag + "N" + "%T%T" + "give up after N seconds for each verification condition (default: " + default_timeout.out +")." +
							"%N%T" + "-" + validate_only_flag + "%T%T" + "parse and typecheck only (no verification).")
		end


feature {MAIN} -- Commands

	welcome
		do
			print ("This is QFIS --- a verifier for the theory of quantifier-free integer sequences.")
			set_option_sign ('-')
		end


	process_arguments
		local l_time: STRING
		do
			if argument_count = 0 then
				print ("%N" + "No input file. Try " + Command_name + " -" + help_flag + " for summary of usage.")

			else
				file_name := argument_array [argument_count]

				help := index_of_word_option (help_flag) /= 0

				smoke := index_of_word_option (smoke_flag) /= 0

				work_directory := coalesced_word_option_value (work_directory_flag)
				if not attached work_directory or else work_directory.is_empty then
					work_directory := default_work_directory
				end

				declarations_directory := coalesced_word_option_value (declarations_directory_flag)
				if not attached declarations_directory then
					if attached decl_env_var then
						declarations_directory := decl_env_var
					else
						declarations_directory := work_directory
					end
				end

				l_time := coalesced_word_option_value (timeout_flag)
				if attached l_time and then l_time.is_integer then
					timeout := l_time.to_integer
				else
					timeout := default_timeout
				end

				validate_only := index_of_word_option (validate_only_flag) /= 0
			end
		end


	perform
		do
			if help then
				print (help_notice)
			elseif not is_valid_input then
				print ("%N" + "Cannot open the input file.")
			else
				create verifier.make_with_file (file_name)
				verifier.parse
				if verifier.parsed then
					verifier.typecheck
					if verifier.typechecked then
						if validate_only then
							print ("%N" + file_name + " parsed and typechecked successfully.")
						else
							verifier.set_timeout (timeout)
							verifier.set_work_directory (work_directory)
							verifier.set_declarations_directory (declarations_directory)
							if smoke then
								verifier.smoke
								if not verifier.smoked then
									print ("%N%N" + "Smoke test failed: the axioms and the background theories are inconsistent.")
								else
									print ("%N%N" + "Smoke test successful: the axioms and the background theories seem consistent.")
								end
							else
								verifier.verify
								if not verifier.verified then
									print (errors_per_routine (verifier.verification_errors))
								end
								print ("%N%N" + statistics)
							end
						end
					else
						print (errors (verifier.typecheck_errors))
					end
				else
					print (errors (verifier.parse_errors))
				end
			end
		end



feature {MAIN} -- Queries

	is_valid_input: BOOLEAN
		-- Does `filename' refer to an accessible text file?
		local l_file: PLAIN_TEXT_FILE
		do
			Result := False
			if attached file_name and then not file_name.is_empty then
				create l_file.make (file_name)
				Result := l_file.exists and l_file.is_access_readable
			end
		end


	errors (l: LIST [STRING]): STRING
		do
			create Result.make (20)
			from l.start
			until l.after
			loop
				Result.append ("%N" + l.item)
				l.forth
			end
		end


	errors_per_routine (t: HASH_TABLE [LINKED_LIST [STRING], STRING]): STRING
		do
			create Result.make (30)
			from t.start
			until t.after
			loop
				Result.append ("%N%N" + "Routine " + t.key_for_iteration)
				if t.item_for_iteration.is_empty then
					Result.append (" verified successfully.")
				else
					Result.append (" not verified:")
					from t.item_for_iteration.start
					until t.item_for_iteration.after
					loop
						Result.append ("%N%T" + t.item_for_iteration.item)
						t.item_for_iteration.forth
					end
				end
				t.forth
			end
		end


	statistics: STRING
		require attached verifier.verification_errors
		local
			err: HASH_TABLE [LINKED_LIST [STRING], STRING]
			routines, verified, n_errors: INTEGER
		do
			err := verifier.verification_errors
			routines := 0
			verified := 0
			n_errors := 0
			from err.start
			until err.after
			loop
				routines := routines + 1
				n_errors := n_errors + err.item_for_iteration.count
				if err.item_for_iteration.is_empty then
					verified := verified + 1
				end
				err.forth
			end
			Result := routines.out + " routines, " + verified.out + " verified, " + n_errors.out + " errors."
		ensure
			attached Result
		end


feature {NONE} -- Initialization

	make
		do
			welcome
			print ("%N")
			process_arguments
			perform
			print ("%N")
		end


end
