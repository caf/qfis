-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class QFIS_PL_SCANNER
-- QFIS: Quantifier Free Integer Sequences

inherit

   YY_COMPRESSED_SCANNER_SKELETON

   QFIS_PL_TOKEN_CODES

create

   make_with_filename, make_with_string

feature -- Status report

	valid_start_condition (sc: INTEGER): BOOLEAN is
			-- Is `sc' a valid start condition?
		do
			Result := (sc = INITIAL)
		end

feature {NONE} -- Implementation

	yy_build_tables is
			-- Build scanner tables.
		do
			yy_nxt := yy_nxt_template
			yy_chk := yy_chk_template
			yy_base := yy_base_template
			yy_def := yy_def_template
			yy_ec := yy_ec_template
			yy_meta := yy_meta_template
			yy_accept := yy_accept_template
		end

	yy_execute_action (yy_act: INTEGER) is
			-- Execute semantic action.
		do
if yy_act <= 39 then
if yy_act <= 20 then
if yy_act <= 10 then
if yy_act <= 5 then
if yy_act <= 3 then
if yy_act <= 2 then
if yy_act = 1 then
--|#line 43 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 43")
end
line_nb := line_nb + 1
else
--|#line 46 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 46")
end
-- Discards blanks
end
else
--|#line 49 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 49")
end
line_nb := line_nb + 1
end
else
if yy_act = 4 then
--|#line 55 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 55")
end
 last_string_value := text; last_token := CATKW 
else
--|#line 57 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 57")
end
 last_string_value := text; last_token := EQSEQUENCEKW 
end
end
else
if yy_act <= 8 then
if yy_act <= 7 then
if yy_act = 6 then
--|#line 59 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 59")
end
 last_string_value := text; last_token := NEQSEQUENCEKW 
else
--|#line 61 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 61")
end
 last_string_value := text; last_token := LENGTHKW 
end
else
--|#line 64 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 64")
end
 last_string_value := text; last_token := PLUSKW 
end
else
if yy_act = 9 then
--|#line 66 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 66")
end
 last_string_value := text; last_token := MINUSKW 
else
--|#line 68 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 68")
end
 last_string_value := text; last_token := TIMESKW 
end
end
end
else
if yy_act <= 15 then
if yy_act <= 13 then
if yy_act <= 12 then
if yy_act = 11 then
--|#line 70 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 70")
end
 last_string_value := text; last_token := DIVIDEKW 
else
--|#line 73 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 73")
end
 last_string_value := text; last_token := NEQKW 
end
else
--|#line 75 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 75")
end
 last_string_value := text; last_token := EQKW 
end
else
if yy_act = 14 then
--|#line 77 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 77")
end
 last_string_value := text; last_token := LTKW 
else
--|#line 79 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 79")
end
 last_string_value := text; last_token := LTEKW 
end
end
else
if yy_act <= 18 then
if yy_act <= 17 then
if yy_act = 16 then
--|#line 81 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 81")
end
 last_string_value := text; last_token := GTKW 
else
--|#line 83 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 83")
end
 last_string_value := text; last_token := GTEKW 
end
else
--|#line 86 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 86")
end
 last_string_value := text; last_token := NOTKW 
end
else
if yy_act = 19 then
--|#line 88 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 88")
end
 last_string_value := text; last_token := ANDKW 
else
--|#line 90 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 90")
end
 last_string_value := text; last_token := ORKW 
end
end
end
end
else
if yy_act <= 30 then
if yy_act <= 25 then
if yy_act <= 23 then
if yy_act <= 22 then
if yy_act = 21 then
--|#line 92 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 92")
end
 last_string_value := text; last_token := IMPLIESKW 
else
--|#line 94 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 94")
end
 last_string_value := text; last_token := IFFKW 
end
else
--|#line 96 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 96")
end
 last_string_value := text; last_token := INKW 
end
else
if yy_act = 24 then
--|#line 98 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 98")
end
 last_string_value := text; last_token := COLONKW 
else
--|#line 100 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 100")
end
 last_string_value := text ; last_token := FORALLKW 
end
end
else
if yy_act <= 28 then
if yy_act <= 27 then
if yy_act = 26 then
--|#line 101 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 101")
end
 last_string_value := text ; last_token := EXISTKW 
else
--|#line 102 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 102")
end
 last_string_value := text ; last_token := OLDKW 
end
else
--|#line 105 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 105")
end
 last_string_value := text; last_token := OPENRANGEKW 
end
else
if yy_act = 29 then
--|#line 106 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 106")
end
 last_string_value := text; last_token := CLOSEDRANGEKW 
else
--|#line 107 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 107")
end
 last_string_value := text; last_token := OPENBRACKETKW 
end
end
end
else
if yy_act <= 35 then
if yy_act <= 33 then
if yy_act <= 32 then
if yy_act = 31 then
--|#line 108 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 108")
end
 last_string_value := text; last_token := CLOSEDBRACKETKW 
else
--|#line 109 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 109")
end
 last_string_value := text; last_token := CURLYOPENKW 
end
else
--|#line 110 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 110")
end
 last_string_value := text; last_token := CURLYCLOSEDKW 
end
else
if yy_act = 34 then
--|#line 111 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 111")
end
 last_string_value := text; last_token := OPENPARENKW 
else
--|#line 112 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 112")
end
 last_string_value := text; last_token := CLOSEDPARENKW 
end
end
else
if yy_act <= 37 then
if yy_act = 36 then
--|#line 116 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 116")
end
 last_string_value := text ; last_token := LOCALKW 
else
--|#line 117 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 117")
end
 last_string_value := text ; last_token := GLOBALKW 
end
else
if yy_act = 38 then
--|#line 118 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 118")
end
 last_string_value := text ; last_token := DOKW 
else
--|#line 119 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 119")
end
 last_string_value := text ; last_token := ENDKW 
end
end
end
end
end
else
if yy_act <= 59 then
if yy_act <= 49 then
if yy_act <= 44 then
if yy_act <= 42 then
if yy_act <= 41 then
if yy_act = 40 then
--|#line 120 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 120")
end
 last_string_value := text ; last_token := BOOLEAN 
else
--|#line 121 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 121")
end
 last_string_value := text ; last_token := SEQUENCE 
end
else
--|#line 122 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 122")
end
 last_string_value := text ; last_token := INTEGER 
end
else
if yy_act = 43 then
--|#line 123 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 123")
end
 last_string_value := text ; last_token := REQUIREKW 
else
--|#line 124 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 124")
end
 last_string_value := text ; last_token := ENSUREKW 
end
end
else
if yy_act <= 47 then
if yy_act <= 46 then
if yy_act = 45 then
--|#line 125 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 125")
end
 last_string_value := text ; last_token := MODIFYKW 
else
--|#line 126 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 126")
end
 last_string_value := text ; last_token := ASSERTKW 
end
else
--|#line 127 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 127")
end
 last_string_value := text ; last_token := ASSUMEKW 
end
else
if yy_act = 48 then
--|#line 128 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 128")
end
 last_string_value := text ; last_token := HAVOCKW 
else
--|#line 129 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 129")
end
 last_string_value := text ; last_token := SKIPKW 
end
end
end
else
if yy_act <= 54 then
if yy_act <= 52 then
if yy_act <= 51 then
if yy_act = 50 then
--|#line 130 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 130")
end
 last_string_value := text ; last_token := ASSIGNKW 
else
--|#line 131 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 131")
end
 last_string_value := text ; last_token := SPLITKW 
end
else
--|#line 132 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 132")
end
 last_string_value := text ; last_token := INTOKW 
end
else
if yy_act = 53 then
--|#line 133 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 133")
end
 last_string_value := text ; last_token := UNTILKW 
else
--|#line 134 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 134")
end
 last_string_value := text ; last_token := LOOPKW 
end
end
else
if yy_act <= 57 then
if yy_act <= 56 then
if yy_act = 55 then
--|#line 135 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 135")
end
 last_string_value := text ; last_token := INVARIANTKW 
else
--|#line 136 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 136")
end
 last_string_value := text ; last_token := IFKW 
end
else
--|#line 137 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 137")
end
 last_string_value := text ; last_token := THENKW 
end
else
if yy_act = 58 then
--|#line 138 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 138")
end
 last_string_value := text ; last_token := ELSEKW 
else
--|#line 139 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 139")
end
 last_string_value := text ; last_token := NDCHOICEKW 
end
end
end
end
else
if yy_act <= 69 then
if yy_act <= 64 then
if yy_act <= 62 then
if yy_act <= 61 then
if yy_act = 60 then
--|#line 140 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 140")
end
 last_string_value := text ; last_token := TERMINATORKW 
else
--|#line 141 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 141")
end
 last_string_value := text ; last_token := CALLKW 
end
else
--|#line 142 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 142")
end
 last_string_value := text ; last_token := ROUTINEKW 
end
else
if yy_act = 63 then
--|#line 143 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 143")
end
 last_string_value := text ; last_token := FUNCTIONKW 
else
--|#line 144 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 144")
end
 last_string_value := text ; last_token := AXIOMKW 
end
end
else
if yy_act <= 67 then
if yy_act <= 66 then
if yy_act = 65 then
--|#line 148 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 148")
end
 last_string_value := text; last_token := EPSILONKW 
else
--|#line 149 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 149")
end
 last_string_value := text; last_token := RECATKW 
end
else
--|#line 150 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 150")
end
 last_string_value := text; last_token := REUNIVERSEKW 
end
else
if yy_act = 68 then
--|#line 154 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 154")
end
 last_string_value := text; last_token := TRUEKW 
else
--|#line 155 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 155")
end
 last_string_value := text; last_token := FALSEKW 
end
end
end
else
if yy_act <= 74 then
if yy_act <= 72 then
if yy_act <= 71 then
if yy_act = 70 then
--|#line 159 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 159")
end
 last_string_value := text ; last_token := COMMAKW 
else
--|#line 162 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 162")
end
 last_string_value := text ; last_token := POSITIONKW 
end
else
--|#line 165 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 165")
end
 last_string_value := text ; last_token := BOOLEANID 
end
else
if yy_act = 73 then
--|#line 166 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 166")
end
 last_string_value := text ; last_token := SEQUENCEID 
else
--|#line 167 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 167")
end
 last_string_value := text ; last_token := INTEGERID 
end
end
else
if yy_act <= 76 then
if yy_act = 75 then
--|#line 168 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 168")
end
 last_string_value := text ; last_token := LABELID 
else
--|#line 171 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 171")
end
 last_string_value := text; last_token := NUMBER 
end
else
if yy_act = 77 then
--|#line 174 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 174")
end
 last_string_value := text.substring (2, text.count-1) ; last_token := DOUBLEQUOTEDSTRING 
else
--|#line 0 "qfisPL.l"
debug ("GELEX")
	std.error.put_line ("Executing scanner user-code from file 'qfisPL.l' at line 0")
end
default_action
end
end
end
end
end
end
		end

	yy_execute_eof_action (yy_sc: INTEGER) is
			-- Execute EOF semantic action.
		do
			terminate
		end

feature {NONE} -- Table templates

	yy_nxt_template: SPECIAL [INTEGER] is
		once
			Result := yy_fixed_array (<<
			    0,    4,    5,    6,    7,    4,    8,    9,   10,   11,
			   12,   13,   14,   15,   16,   17,   18,   19,   20,   21,
			    4,   22,   23,   24,   23,   23,   23,   25,   23,   26,
			   23,   23,   23,   23,   23,   27,   28,   29,   30,   31,
			   32,   33,   34,   35,   36,   37,   38,   39,   40,   41,
			   34,   34,   42,   43,   44,   45,   34,   34,   46,   47,
			   48,   49,   34,   34,   34,   50,   51,   52,   60,   61,
			   63,   62,   65,   66,   68,   64,   68,   68,   68,   68,
			   68,   68,   68,   68,   72,   68,   69,   68,   71,   68,
			   68,   68,   68,   68,   68,   68,   68,   70,   68,   68,

			   68,   68,   80,   88,  108,   68,   68,   68,   68,   68,
			   68,  111,  110,   68,   77,  100,   68,   73,   81,   78,
			   85,   87,   82,   79,   83,  101,   86,   89,   91,   92,
			   93,   94,   96,   84,  113,   90,   98,   95,  109,  114,
			   68,   99,   97,   68,   68,  115,   68,   68,  117,  112,
			   68,  121,   68,   68,   68,   68,   68,   68,   68,   68,
			   68,   68,   68,   68,   68,   68,   68,  118,   68,   68,
			   68,  141,  116,  120,  139,   68,  127,   68,  129,   68,
			  122,  131,  119,  142,   68,  123,   68,  136,  128,  124,
			  134,  135,  125,  132,  126,  130,   68,   68,  151,  133,

			  137,  147,  140,  146,  143,   68,   68,   68,   68,  144,
			   68,  148,   68,   68,   68,   68,  149,   68,   68,   68,
			  152,   68,   68,   68,   68,  145,   68,  153,   68,  156,
			  166,  157,   68,   68,  168,  150,   68,   68,  169,   68,
			   68,  154,  155,  159,  167,   68,   68,  163,  158,   68,
			  165,   68,   68,  162,  160,  161,  164,   68,   68,   68,
			   68,  170,   68,   68,  186,   68,  171,  177,  174,  172,
			   68,  175,  178,   68,  188,  173,  187,  181,   68,   68,
			  176,   68,   68,  179,  180,   68,  182,  183,   68,   68,
			   68,   68,  185,   68,   68,   68,   68,   68,  190,  184,

			  201,   68,  199,  191,   68,  189,   68,  200,   68,   68,
			  193,  192,  206,   68,  194,  203,   54,  195,   54,   54,
			   54,   68,  204,   68,  198,   68,  205,  197,   68,  196,
			  202,   67,   67,   68,   67,   74,   74,   74,  207,   68,
			   76,   76,  208,   76,   56,   56,   56,   56,   56,  209,
			   68,   68,   68,   68,   68,   68,   68,   68,   68,   68,
			   68,   68,   68,   68,   68,   68,   68,   68,   68,   68,
			   68,   68,   68,  138,   68,   68,   68,   68,   75,   68,
			  107,  106,  105,  104,   58,  103,  102,   53,   68,   75,
			   68,   68,   59,   58,   57,   56,   55,   53,  210,    3,

			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210, yy_Dummy>>)
		end

	yy_chk_template: SPECIAL [INTEGER] is
		once
			Result := yy_fixed_array (<<
			    0,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,   19,   19,
			   20,   19,   21,   21,   24,   20,   25,   26,   27,   28,
			   33,   35,   40,   36,   27,   38,   24,   48,   26,   39,
			   37,   49,   69,   42,   43,   44,   71,   25,   72,   45,

			   78,   41,   35,   40,   69,   47,   70,   46,   73,   82,
			   77,   72,   71,   85,   33,   48,   79,   28,   36,   33,
			   38,   39,   37,   33,   37,   49,   38,   41,   42,   43,
			   44,   45,   46,   37,   77,   41,   47,   45,   70,   78,
			   80,   47,   46,   83,   84,   79,   86,   88,   82,   73,
			   87,   85,   90,   91,   92,   93,   96,   94,   97,   99,
			  101,   98,  100,  109,  108,  110,  111,   83,  115,  116,
			  209,  110,   80,   84,  108,  119,   91,  121,   92,  112,
			   86,   94,   83,  111,  114,   87,  117,  100,   91,   88,
			   98,   99,   90,   96,   90,   93,  120,  122,  121,   97,

			  101,  116,  109,  115,  112,  123,  124,  125,  126,  114,
			  127,  117,  128,  132,  129,  133,  119,  134,  135,  140,
			  122,  137,  136,  144,  139,  114,  141,  123,  150,  126,
			  139,  127,  142,  145,  141,  120,  146,  149,  142,  151,
			  152,  124,  125,  129,  140,  156,  153,  135,  128,  154,
			  137,  159,  157,  134,  132,  133,  136,  160,  161,  163,
			  165,  144,  166,  169,  166,  170,  145,  153,  150,  146,
			  168,  151,  154,  171,  169,  149,  168,  159,  173,  175,
			  152,  176,  177,  156,  157,  181,  160,  161,  179,  182,
			  183,  186,  165,  187,  195,  193,  188,  197,  171,  163,

			  188,  198,  186,  173,  202,  170,  201,  187,  203,  208,
			  176,  175,  201,  207,  177,  195,  211,  179,  211,  211,
			  211,  206,  197,  205,  183,  204,  198,  182,  200,  181,
			  193,  212,  212,  199,  212,  213,  213,  213,  202,  196,
			  214,  214,  203,  214,  215,  215,  215,  215,  215,  208,
			  194,  192,  191,  190,  189,  185,  184,  180,  178,  174,
			  172,  167,  164,  162,  158,  155,  148,  147,  143,  131,
			  130,  118,  113,  104,   95,   89,   81,   76,   74,   67,
			   64,   63,   62,   61,   58,   56,   54,   53,   34,   32,
			   29,   23,   17,   16,   15,   13,   11,    5,    3,  210,

			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210, yy_Dummy>>)
		end

	yy_base_template: SPECIAL [INTEGER] is
		once
			Result := yy_fixed_array (<<
			    0,    0,    0,  398,  399,  395,  399,    0,  399,  399,
			  399,  387,  399,  384,  399,  376,  379,  374,  399,   51,
			   57,   54,  399,  371,   54,   56,   57,   58,   59,  370,
			  399,  399,  374,   60,  368,   61,   63,   70,   65,   69,
			   62,   81,   73,   74,   75,   79,   87,   85,   67,   71,
			  399,  399,  399,  385,  382,  399,  382,  399,  370,  399,
			  399,  365,  363,  363,  361,  399,  399,  359,  399,   72,
			   86,   76,   78,   88,  363,  399,  357,   90,   80,   96,
			  120,  356,   89,  123,  124,   93,  126,  130,  127,  355,
			  132,  133,  134,  135,  137,  354,  136,  138,  141,  139,

			  142,  140,  399,  399,  354,  399,  399,  399,  144,  143,
			  145,  146,  159,  352,  164,  148,  149,  166,  351,  155,
			  176,  157,  177,  185,  186,  187,  188,  190,  192,  194,
			  350,  349,  193,  195,  197,  198,  202,  201,  399,  204,
			  199,  206,  212,  348,  203,  213,  216,  347,  346,  217,
			  208,  219,  220,  226,  229,  345,  225,  232,  344,  231,
			  237,  238,  343,  239,  342,  240,  242,  341,  250,  243,
			  245,  253,  340,  258,  339,  259,  261,  262,  338,  268,
			  337,  265,  269,  270,  336,  335,  271,  273,  276,  334,
			  333,  332,  331,  275,  330,  274,  319,  277,  281,  313,

			  308,  286,  284,  288,  305,  303,  301,  293,  289,  150,
			  399,  315,  329,  333,  338,  343, yy_Dummy>>)
		end

	yy_def_template: SPECIAL [INTEGER] is
		once
			Result := yy_fixed_array (<<
			    0,  210,    1,  210,  210,  210,  210,  211,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  210,  210,  210,
			  210,  210,  210,  212,  212,  212,  212,  212,  212,  212,
			  210,  210,  213,  214,  214,  214,  214,  214,  214,  214,
			  214,  214,  214,  214,  214,  214,  214,  214,  214,  214,
			  210,  210,  210,  210,  211,  210,  215,  210,  210,  210,
			  210,  210,  210,  210,  210,  210,  210,  212,  210,  212,
			  212,  212,  212,  212,  213,  210,  214,  214,  214,  214,
			  214,  214,  214,  214,  214,  214,  214,  214,  214,  214,
			  214,  214,  214,  214,  214,  214,  214,  214,  214,  214,

			  214,  214,  210,  210,  210,  210,  210,  210,  212,  212,
			  212,  212,  212,  214,  214,  214,  214,  214,  214,  214,
			  214,  214,  214,  214,  214,  214,  214,  214,  214,  214,
			  214,  214,  214,  214,  214,  214,  214,  214,  210,  212,
			  212,  212,  212,  212,  214,  214,  214,  214,  214,  214,
			  214,  214,  214,  214,  214,  214,  214,  214,  214,  214,
			  214,  214,  214,  214,  214,  214,  212,  212,  212,  212,
			  214,  214,  214,  214,  214,  214,  214,  214,  214,  214,
			  214,  214,  214,  214,  214,  214,  212,  212,  212,  214,
			  214,  214,  214,  214,  214,  214,  214,  214,  214,  212,

			  212,  212,  214,  214,  214,  214,  212,  214,  214,  214,
			    0,  210,  210,  210,  210,  210, yy_Dummy>>)
		end

	yy_ec_template: SPECIAL [INTEGER] is
		once
			Result := yy_fixed_array (<<
			    0,    1,    1,    1,    1,    1,    1,    1,    1,    2,
			    3,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    2,    1,    4,    5,    1,    1,    1,    1,
			    6,    7,    8,    9,   10,   11,   12,   13,   14,   14,
			   14,   14,   14,   14,   14,   14,   14,   14,   15,   16,
			   17,   18,   19,   20,   21,   22,   23,   24,   25,   26,
			   27,   28,   25,   29,   25,   25,   30,   25,   31,   32,
			   25,   33,   34,   35,   36,   37,   25,   25,   25,   25,
			   25,   38,    1,   39,    1,   40,    1,   41,   42,   43,

			   44,   45,   46,   47,   48,   49,   50,   51,   52,   53,
			   54,   55,   56,   57,   58,   59,   60,   61,   62,   50,
			   63,   64,   50,   65,   66,   67,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,

			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
			    1,    1,    1,    1,    1,    1,    1, yy_Dummy>>)
		end

	yy_meta_template: SPECIAL [INTEGER] is
		once
			Result := yy_fixed_array (<<
			    0,    1,    1,    1,    2,    3,    1,    1,    1,    1,
			    1,    1,    1,    1,    3,    4,    1,    1,    1,    1,
			    5,    1,    3,    3,    3,    3,    3,    3,    3,    3,
			    3,    3,    3,    3,    3,    3,    3,    3,    1,    1,
			    3,    3,    3,    3,    3,    3,    3,    3,    3,    3,
			    3,    3,    3,    3,    3,    3,    3,    3,    3,    3,
			    3,    3,    3,    3,    3,    1,    1,    1, yy_Dummy>>)
		end

	yy_accept_template: SPECIAL [INTEGER] is
		once
			Result := yy_fixed_array (<<
			    0,    0,    0,   79,   78,    2,    3,   78,   34,   35,
			   10,    8,   70,    9,   66,   11,   76,   24,   60,   14,
			   13,   16,   71,   73,   73,   73,   73,   73,   73,   67,
			   30,   31,   78,   74,   74,   74,   74,   65,   74,   74,
			   74,   74,   74,   74,   74,   74,   74,   74,   74,   74,
			   32,    7,   33,    2,    0,    4,    0,   12,   76,   50,
			   28,   15,    0,    0,    5,   17,   29,   73,   72,   73,
			   73,   73,   73,   73,    0,   75,   74,   74,   74,   74,
			   74,   38,   74,   74,   74,   74,   74,   74,   74,   56,
			   23,   74,   74,   74,   74,   20,   74,   74,   74,   74,

			   74,   74,   77,    1,    0,   59,    6,   21,   73,   73,
			   73,   73,   73,   19,   74,   74,   74,   74,   39,   74,
			   74,   74,   74,   74,   74,   74,   74,   74,   74,   74,
			   18,   27,   74,   74,   74,   74,   74,   74,   22,   73,
			   73,   73,   73,   68,   74,   74,   74,   61,   58,   74,
			   74,   74,   74,   74,   74,   52,   74,   74,   54,   74,
			   74,   74,   49,   74,   57,   74,   73,   69,   73,   73,
			   74,   74,   64,   74,   26,   74,   74,   74,   48,   74,
			   36,   74,   74,   74,   51,   53,   73,   73,   73,   46,
			   47,   44,   25,   74,   37,   74,   45,   74,   74,   40,

			   42,   73,   74,   74,   43,   62,   41,   63,   74,   55,
			    0, yy_Dummy>>)
		end

feature {NONE} -- Constants

	yyJam_base: INTEGER is 399
			-- Position in `yy_nxt'/`yy_chk' tables
			-- where default jam table starts

	yyJam_state: INTEGER is 210
			-- State id corresponding to jam state

	yyTemplate_mark: INTEGER is 211
			-- Mark between normal states and templates

	yyNull_equiv_class: INTEGER is 1
			-- Equivalence code for NULL character

	yyReject_used: BOOLEAN is false
			-- Is `reject' called?

	yyVariable_trail_context: BOOLEAN is false
			-- Is there a regular expression with
			-- both leading and trailing parts having
			-- variable length?

	yyReject_or_variable_trail_context: BOOLEAN is false
			-- Is `reject' called or is there a
			-- regular expression with both leading
			-- and trailing parts having variable length?

	yyNb_rules: INTEGER is 78
			-- Number of rules

	yyEnd_of_buffer: INTEGER is 79
			-- End of buffer rule code

	yyLine_used: BOOLEAN is false
			-- Are line and column numbers used?

	yyPosition_used: BOOLEAN is false
			-- Is `position' used?

	INITIAL: INTEGER is 0
			-- Start condition codes

feature -- User-defined features



  line_nb: INTEGER
           -- Current line number

  scanned_file: PLAIN_TEXT_FILE
  scanned_file_to_string: STRING
  scanned_file_to_buffer: YY_BUFFER

  feature {NONE} -- Initialization

       make_with_filename (filename: STRING) is
             -- Create new scanner with `filename' as input file
          do
             line_nb := 1
             create scanned_file.make_open_read (filename)

             scanned_file.read_stream (scanned_file.count)

				 scanned_file_to_string := scanned_file.last_string.twin
             create scanned_file_to_buffer.make (scanned_file_to_string)
				 make_with_buffer (scanned_file_to_buffer)
--           scan
         end


       make_with_string (str: STRING) is
             -- Create new scanner for string `str'
          do
             line_nb := 1
             scanned_file_to_string := str
             create scanned_file_to_buffer.make (scanned_file_to_string)
			    make_with_buffer (scanned_file_to_buffer)
--             scan
         end

end
