indexing

	description: "Parser token codes"
	generator: "geyacc version 3.9"

class QFIS_PL_TOKEN_CODES

inherit

	YY_PARSER_TOKENS

feature -- Last values

	last_any_value: ANY
	last_string_value: STRING

feature -- Access

	token_name (a_token: INTEGER): STRING is
			-- Name of token `a_token'
		do
			inspect a_token
			when 0 then
				Result := "EOF token"
			when -1 then
				Result := "Error token"
			when CATKW then
				Result := "CATKW"
			when EQSEQUENCEKW then
				Result := "EQSEQUENCEKW"
			when NEQSEQUENCEKW then
				Result := "NEQSEQUENCEKW"
			when LENGTHKW then
				Result := "LENGTHKW"
			when PLUSKW then
				Result := "PLUSKW"
			when MINUSKW then
				Result := "MINUSKW"
			when TIMESKW then
				Result := "TIMESKW"
			when DIVIDEKW then
				Result := "DIVIDEKW"
			when NEQKW then
				Result := "NEQKW"
			when EQKW then
				Result := "EQKW"
			when LTKW then
				Result := "LTKW"
			when LTEKW then
				Result := "LTEKW"
			when GTKW then
				Result := "GTKW"
			when GTEKW then
				Result := "GTEKW"
			when NOTKW then
				Result := "NOTKW"
			when ANDKW then
				Result := "ANDKW"
			when ORKW then
				Result := "ORKW"
			when IMPLIESKW then
				Result := "IMPLIESKW"
			when IFFKW then
				Result := "IFFKW"
			when INKW then
				Result := "INKW"
			when COLONKW then
				Result := "COLONKW"
			when OPENRANGEKW then
				Result := "OPENRANGEKW"
			when CLOSEDRANGEKW then
				Result := "CLOSEDRANGEKW"
			when OPENBRACKETKW then
				Result := "OPENBRACKETKW"
			when CLOSEDBRACKETKW then
				Result := "CLOSEDBRACKETKW"
			when CURLYOPENKW then
				Result := "CURLYOPENKW"
			when CURLYCLOSEDKW then
				Result := "CURLYCLOSEDKW"
			when OPENPARENKW then
				Result := "OPENPARENKW"
			when CLOSEDPARENKW then
				Result := "CLOSEDPARENKW"
			when EPSILONKW then
				Result := "EPSILONKW"
			when RECATKW then
				Result := "RECATKW"
			when REUNIVERSEKW then
				Result := "REUNIVERSEKW"
			when COMMAKW then
				Result := "COMMAKW"
			when POSITIONKW then
				Result := "POSITIONKW"
			when FORALLKW then
				Result := "FORALLKW"
			when EXISTKW then
				Result := "EXISTKW"
			when OLDKW then
				Result := "OLDKW"
			when LOCALKW then
				Result := "LOCALKW"
			when GLOBALKW then
				Result := "GLOBALKW"
			when DOKW then
				Result := "DOKW"
			when ENDKW then
				Result := "ENDKW"
			when BOOLEAN then
				Result := "BOOLEAN"
			when SEQUENCE then
				Result := "SEQUENCE"
			when INTEGER then
				Result := "INTEGER"
			when REQUIREKW then
				Result := "REQUIREKW"
			when ENSUREKW then
				Result := "ENSUREKW"
			when MODIFYKW then
				Result := "MODIFYKW"
			when ASSERTKW then
				Result := "ASSERTKW"
			when ASSUMEKW then
				Result := "ASSUMEKW"
			when HAVOCKW then
				Result := "HAVOCKW"
			when SKIPKW then
				Result := "SKIPKW"
			when ASSIGNKW then
				Result := "ASSIGNKW"
			when SPLITKW then
				Result := "SPLITKW"
			when INTOKW then
				Result := "INTOKW"
			when UNTILKW then
				Result := "UNTILKW"
			when LOOPKW then
				Result := "LOOPKW"
			when INVARIANTKW then
				Result := "INVARIANTKW"
			when IFKW then
				Result := "IFKW"
			when THENKW then
				Result := "THENKW"
			when ELSEKW then
				Result := "ELSEKW"
			when NDCHOICEKW then
				Result := "NDCHOICEKW"
			when TERMINATORKW then
				Result := "TERMINATORKW"
			when CALLKW then
				Result := "CALLKW"
			when ROUTINEKW then
				Result := "ROUTINEKW"
			when FUNCTIONKW then
				Result := "FUNCTIONKW"
			when AXIOMKW then
				Result := "AXIOMKW"
			when TRUEKW then
				Result := "TRUEKW"
			when FALSEKW then
				Result := "FALSEKW"
			when SEQUENCEID then
				Result := "SEQUENCEID"
			when INTEGERID then
				Result := "INTEGERID"
			when BOOLEANID then
				Result := "BOOLEANID"
			when LABELID then
				Result := "LABELID"
			when NUMBER then
				Result := "NUMBER"
			when DOUBLEQUOTEDSTRING then
				Result := "DOUBLEQUOTEDSTRING"
			when EXISTSKW then
				Result := "EXISTSKW"
			else
				Result := yy_character_token_name (a_token)
			end
		end

feature -- Token codes

	CATKW: INTEGER is 258
	EQSEQUENCEKW: INTEGER is 259
	NEQSEQUENCEKW: INTEGER is 260
	LENGTHKW: INTEGER is 261
	PLUSKW: INTEGER is 262
	MINUSKW: INTEGER is 263
	TIMESKW: INTEGER is 264
	DIVIDEKW: INTEGER is 265
	NEQKW: INTEGER is 266
	EQKW: INTEGER is 267
	LTKW: INTEGER is 268
	LTEKW: INTEGER is 269
	GTKW: INTEGER is 270
	GTEKW: INTEGER is 271
	NOTKW: INTEGER is 272
	ANDKW: INTEGER is 273
	ORKW: INTEGER is 274
	IMPLIESKW: INTEGER is 275
	IFFKW: INTEGER is 276
	INKW: INTEGER is 277
	COLONKW: INTEGER is 278
	OPENRANGEKW: INTEGER is 279
	CLOSEDRANGEKW: INTEGER is 280
	OPENBRACKETKW: INTEGER is 281
	CLOSEDBRACKETKW: INTEGER is 282
	CURLYOPENKW: INTEGER is 283
	CURLYCLOSEDKW: INTEGER is 284
	OPENPARENKW: INTEGER is 285
	CLOSEDPARENKW: INTEGER is 286
	EPSILONKW: INTEGER is 287
	RECATKW: INTEGER is 288
	REUNIVERSEKW: INTEGER is 289
	COMMAKW: INTEGER is 290
	POSITIONKW: INTEGER is 291
	FORALLKW: INTEGER is 292
	EXISTKW: INTEGER is 293
	OLDKW: INTEGER is 294
	LOCALKW: INTEGER is 295
	GLOBALKW: INTEGER is 296
	DOKW: INTEGER is 297
	ENDKW: INTEGER is 298
	BOOLEAN: INTEGER is 299
	SEQUENCE: INTEGER is 300
	INTEGER: INTEGER is 301
	REQUIREKW: INTEGER is 302
	ENSUREKW: INTEGER is 303
	MODIFYKW: INTEGER is 304
	ASSERTKW: INTEGER is 305
	ASSUMEKW: INTEGER is 306
	HAVOCKW: INTEGER is 307
	SKIPKW: INTEGER is 308
	ASSIGNKW: INTEGER is 309
	SPLITKW: INTEGER is 310
	INTOKW: INTEGER is 311
	UNTILKW: INTEGER is 312
	LOOPKW: INTEGER is 313
	INVARIANTKW: INTEGER is 314
	IFKW: INTEGER is 315
	THENKW: INTEGER is 316
	ELSEKW: INTEGER is 317
	NDCHOICEKW: INTEGER is 318
	TERMINATORKW: INTEGER is 319
	CALLKW: INTEGER is 320
	ROUTINEKW: INTEGER is 321
	FUNCTIONKW: INTEGER is 322
	AXIOMKW: INTEGER is 323
	TRUEKW: INTEGER is 324
	FALSEKW: INTEGER is 325
	SEQUENCEID: INTEGER is 326
	INTEGERID: INTEGER is 327
	BOOLEANID: INTEGER is 328
	LABELID: INTEGER is 329
	NUMBER: INTEGER is 330
	DOUBLEQUOTEDSTRING: INTEGER is 331
	EXISTSKW: INTEGER is 332

end
