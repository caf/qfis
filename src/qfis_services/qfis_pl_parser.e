-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

note

  description: "Parser for the quantifier-free fragment of the theory of integer sequences and uninterpreted functions and its programming language"

class QFIS_PL_PARSER

inherit

	  YY_PARSER_SKELETON
	    rename
	      make as make_parser_skeleton
       redefine
	      report_error
       end

	  QFIS_PL_SCANNER
	     rename
	       make as make_std_scanner,
	  make_with_filename as make_scanner,
	       make_with_string as make_scanner_with_string
	     export
	       {NONE} all
		  end

create

						 make, make_with_string


feature {NONE} -- Implementation

	yy_build_parser_tables is
			-- Build parser tables.
		do
			yytranslate := yytranslate_template
			yyr1 := yyr1_template
			yytypes1 := yytypes1_template
			yytypes2 := yytypes2_template
			yydefact := yydefact_template
			yydefgoto := yydefgoto_template
			yypact := yypact_template
			yypgoto := yypgoto_template
			yytable := yytable_template
			yycheck := yycheck_template
		end

	yy_create_value_stacks is
			-- Create value stacks.
		do
		end

	yy_init_value_stacks is
			-- Initialize value stacks.
		do
			yyvsp1 := -1
			yyvsp2 := -1
			yyvsp3 := -1
			yyvsp4 := -1
			yyvsp5 := -1
			yyvsp6 := -1
			yyvsp7 := -1
			yyvsp8 := -1
			yyvsp9 := -1
			yyvsp10 := -1
			yyvsp11 := -1
			yyvsp12 := -1
			yyvsp13 := -1
			yyvsp14 := -1
			yyvsp15 := -1
			yyvsp16 := -1
			yyvsp17 := -1
			yyvsp18 := -1
			yyvsp19 := -1
			yyvsp20 := -1
			yyvsp21 := -1
			yyvsp22 := -1
			yyvsp23 := -1
			yyvsp24 := -1
			yyvsp25 := -1
			yyvsp26 := -1
			yyvsp27 := -1
			yyvsp28 := -1
			yyvsp29 := -1
			yyvsp30 := -1
			yyvsp31 := -1
		end

	yy_clear_value_stacks is
			-- Clear objects in semantic value stacks so that
			-- they can be collected by the garbage collector.
		do
			if yyvs1 /= Void then
				yyvs1.clear_all
			end
			if yyvs2 /= Void then
				yyvs2.clear_all
			end
			if yyvs3 /= Void then
				yyvs3.clear_all
			end
			if yyvs4 /= Void then
				yyvs4.clear_all
			end
			if yyvs5 /= Void then
				yyvs5.clear_all
			end
			if yyvs6 /= Void then
				yyvs6.clear_all
			end
			if yyvs7 /= Void then
				yyvs7.clear_all
			end
			if yyvs8 /= Void then
				yyvs8.clear_all
			end
			if yyvs9 /= Void then
				yyvs9.clear_all
			end
			if yyvs10 /= Void then
				yyvs10.clear_all
			end
			if yyvs11 /= Void then
				yyvs11.clear_all
			end
			if yyvs12 /= Void then
				yyvs12.clear_all
			end
			if yyvs13 /= Void then
				yyvs13.clear_all
			end
			if yyvs14 /= Void then
				yyvs14.clear_all
			end
			if yyvs15 /= Void then
				yyvs15.clear_all
			end
			if yyvs16 /= Void then
				yyvs16.clear_all
			end
			if yyvs17 /= Void then
				yyvs17.clear_all
			end
			if yyvs18 /= Void then
				yyvs18.clear_all
			end
			if yyvs19 /= Void then
				yyvs19.clear_all
			end
			if yyvs20 /= Void then
				yyvs20.clear_all
			end
			if yyvs21 /= Void then
				yyvs21.clear_all
			end
			if yyvs22 /= Void then
				yyvs22.clear_all
			end
			if yyvs23 /= Void then
				yyvs23.clear_all
			end
			if yyvs24 /= Void then
				yyvs24.clear_all
			end
			if yyvs25 /= Void then
				yyvs25.clear_all
			end
			if yyvs26 /= Void then
				yyvs26.clear_all
			end
			if yyvs27 /= Void then
				yyvs27.clear_all
			end
			if yyvs28 /= Void then
				yyvs28.clear_all
			end
			if yyvs29 /= Void then
				yyvs29.clear_all
			end
			if yyvs30 /= Void then
				yyvs30.clear_all
			end
			if yyvs31 /= Void then
				yyvs31.clear_all
			end
		end

	yy_push_last_value (yychar1: INTEGER) is
			-- Push semantic value associated with token `last_token'
			-- (with internal id `yychar1') on top of corresponding
			-- value stack.
		do
			inspect yytypes2.item (yychar1)
			when 1 then
				yyvsp1 := yyvsp1 + 1
				if yyvsp1 >= yyvsc1 then
					if yyvs1 = Void then
						debug ("GEYACC")
							std.error.put_line ("Create yyvs1")
						end
						create yyspecial_routines1
						yyvsc1 := yyInitial_yyvs_size
						yyvs1 := yyspecial_routines1.make (yyvsc1)
					else
						debug ("GEYACC")
							std.error.put_line ("Resize yyvs1")
						end
						yyvsc1 := yyvsc1 + yyInitial_yyvs_size
						yyvs1 := yyspecial_routines1.resize (yyvs1, yyvsc1)
					end
				end
				yyvs1.put (last_any_value, yyvsp1)
			when 2 then
				yyvsp2 := yyvsp2 + 1
				if yyvsp2 >= yyvsc2 then
					if yyvs2 = Void then
						debug ("GEYACC")
							std.error.put_line ("Create yyvs2")
						end
						create yyspecial_routines2
						yyvsc2 := yyInitial_yyvs_size
						yyvs2 := yyspecial_routines2.make (yyvsc2)
					else
						debug ("GEYACC")
							std.error.put_line ("Resize yyvs2")
						end
						yyvsc2 := yyvsc2 + yyInitial_yyvs_size
						yyvs2 := yyspecial_routines2.resize (yyvs2, yyvsc2)
					end
				end
				yyvs2.put (last_string_value, yyvsp2)
			else
				debug ("GEYACC")
					std.error.put_string ("Error in parser: not a token type: ")
					std.error.put_integer (yytypes2.item (yychar1))
					std.error.put_new_line
				end
				abort
			end
		end

	yy_push_error_value is
			-- Push semantic value associated with token 'error'
			-- on top of corresponding value stack.
		local
			yyval1: ANY
		do
			yyvsp1 := yyvsp1 + 1
			if yyvsp1 >= yyvsc1 then
				if yyvs1 = Void then
					debug ("GEYACC")
						std.error.put_line ("Create yyvs1")
					end
					create yyspecial_routines1
					yyvsc1 := yyInitial_yyvs_size
					yyvs1 := yyspecial_routines1.make (yyvsc1)
				else
					debug ("GEYACC")
						std.error.put_line ("Resize yyvs1")
					end
					yyvsc1 := yyvsc1 + yyInitial_yyvs_size
					yyvs1 := yyspecial_routines1.resize (yyvs1, yyvsc1)
				end
			end
			yyvs1.put (yyval1, yyvsp1)
		end

	yy_pop_last_value (yystate: INTEGER) is
			-- Pop semantic value from stack when in state `yystate'.
		local
			yy_type_id: INTEGER
		do
			yy_type_id := yytypes1.item (yystate)
			inspect yy_type_id
			when 1 then
				yyvsp1 := yyvsp1 - 1
			when 2 then
				yyvsp2 := yyvsp2 - 1
			when 3 then
				yyvsp3 := yyvsp3 - 1
			when 4 then
				yyvsp4 := yyvsp4 - 1
			when 5 then
				yyvsp5 := yyvsp5 - 1
			when 6 then
				yyvsp6 := yyvsp6 - 1
			when 7 then
				yyvsp7 := yyvsp7 - 1
			when 8 then
				yyvsp8 := yyvsp8 - 1
			when 9 then
				yyvsp9 := yyvsp9 - 1
			when 10 then
				yyvsp10 := yyvsp10 - 1
			when 11 then
				yyvsp11 := yyvsp11 - 1
			when 12 then
				yyvsp12 := yyvsp12 - 1
			when 13 then
				yyvsp13 := yyvsp13 - 1
			when 14 then
				yyvsp14 := yyvsp14 - 1
			when 15 then
				yyvsp15 := yyvsp15 - 1
			when 16 then
				yyvsp16 := yyvsp16 - 1
			when 17 then
				yyvsp17 := yyvsp17 - 1
			when 18 then
				yyvsp18 := yyvsp18 - 1
			when 19 then
				yyvsp19 := yyvsp19 - 1
			when 20 then
				yyvsp20 := yyvsp20 - 1
			when 21 then
				yyvsp21 := yyvsp21 - 1
			when 22 then
				yyvsp22 := yyvsp22 - 1
			when 23 then
				yyvsp23 := yyvsp23 - 1
			when 24 then
				yyvsp24 := yyvsp24 - 1
			when 25 then
				yyvsp25 := yyvsp25 - 1
			when 26 then
				yyvsp26 := yyvsp26 - 1
			when 27 then
				yyvsp27 := yyvsp27 - 1
			when 28 then
				yyvsp28 := yyvsp28 - 1
			when 29 then
				yyvsp29 := yyvsp29 - 1
			when 30 then
				yyvsp30 := yyvsp30 - 1
			when 31 then
				yyvsp31 := yyvsp31 - 1
			else
				debug ("GEYACC")
					std.error.put_string ("Error in parser: unknown type id: ")
					std.error.put_integer (yy_type_id)
					std.error.put_new_line
				end
				abort
			end
		end

feature {NONE} -- Semantic actions

	yy_do_action (yy_act: INTEGER) is
			-- Execute semantic action.
		local
			yyval1: ANY
			yyval14: QFIS_PROGRAM
			yyval3: QFIS_FORMULA
			yyval31: QFIS_AXIOM
			yyval30: QFIS_FUNCTION_DECLARATION
			yyval15: QFIS_ROUTINE
			yyval16: QFIS_PRECONDITION
			yyval17: QFIS_POSTCONDITION
			yyval19: QFIS_LOCAL
			yyval18: QFIS_LIST_VARIABLES
			yyval20: QFIS_SIGNATURE
			yyval2: STRING
			yyval21: QFIS_LIST_DECLARATIONS
			yyval22: QFIS_BODY
			yyval24: QFIS_LIST [QFIS_INSTRUCTION]
			yyval23: QFIS_INSTRUCTION
			yyval25: QFIS_CONDITIONAL
			yyval26: QFIS_LOOP
			yyval27: QFIS_INVARIANT
			yyval28: QFIS_LIST [QFIS_FORMULA]
			yyval29: QFIS_CALL
			yyval5: QFIS_QUANTIFIED_FORMULA
			yyval4: QFIS_ATOMIC_FORMULA
			yyval8: QFIS_SEQUENCE_COMPARISON
			yyval9: QFIS_ARITHMETIC_COMPARISON
			yyval6: QFIS_SEQUENCE
			yyval12: INTEGER
			yyval13: QFIS_REGEXP
			yyval7: QFIS_ARGUMENTS
			yyval11: QFIS_INTEGER_TERM
			yyval10: QFIS_ARITHMETIC_COMPOUND
		do
			inspect yy_act
when 1 then
--|#line 191 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 191")
end

parsed_program := yyvs14.item (yyvsp14) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp1 := yyvsp1 + 1
	yyvsp14 := yyvsp14 -1
	if yyvsp1 >= yyvsc1 then
		if yyvs1 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs1")
			end
			create yyspecial_routines1
			yyvsc1 := yyInitial_yyvs_size
			yyvs1 := yyspecial_routines1.make (yyvsc1)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs1")
			end
			yyvsc1 := yyvsc1 + yyInitial_yyvs_size
			yyvs1 := yyspecial_routines1.resize (yyvs1, yyvsc1)
		end
	end
	yyvs1.put (yyval1, yyvsp1)
end
when 2 then
--|#line 192 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 192")
end

parsed_formula := yyvs3.item (yyvsp3) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp1 := yyvsp1 + 1
	yyvsp3 := yyvsp3 -1
	if yyvsp1 >= yyvsc1 then
		if yyvs1 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs1")
			end
			create yyspecial_routines1
			yyvsc1 := yyInitial_yyvs_size
			yyvs1 := yyspecial_routines1.make (yyvsc1)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs1")
			end
			yyvsc1 := yyvsc1 + yyInitial_yyvs_size
			yyvs1 := yyspecial_routines1.resize (yyvs1, yyvsc1)
		end
	end
	yyvs1.put (yyval1, yyvsp1)
end
when 3 then
--|#line 195 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 195")
end

create {QFIS_PROGRAM} yyval14.make ; yyval14.extend (yyvs15.item (yyvsp15)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp14 := yyvsp14 + 1
	yyvsp15 := yyvsp15 -1
	if yyvsp14 >= yyvsc14 then
		if yyvs14 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs14")
			end
			create yyspecial_routines14
			yyvsc14 := yyInitial_yyvs_size
			yyvs14 := yyspecial_routines14.make (yyvsc14)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs14")
			end
			yyvsc14 := yyvsc14 + yyInitial_yyvs_size
			yyvs14 := yyspecial_routines14.resize (yyvs14, yyvsc14)
		end
	end
	yyvs14.put (yyval14, yyvsp14)
end
when 4 then
--|#line 196 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 196")
end

create {QFIS_PROGRAM} yyval14.make ; yyval14.extend (yyvs21.item (yyvsp21)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp14 := yyvsp14 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp21 := yyvsp21 -1
	if yyvsp14 >= yyvsc14 then
		if yyvs14 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs14")
			end
			create yyspecial_routines14
			yyvsc14 := yyInitial_yyvs_size
			yyvs14 := yyspecial_routines14.make (yyvsc14)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs14")
			end
			yyvsc14 := yyvsc14 + yyInitial_yyvs_size
			yyvs14 := yyspecial_routines14.resize (yyvs14, yyvsc14)
		end
	end
	yyvs14.put (yyval14, yyvsp14)
end
when 5 then
--|#line 197 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 197")
end

create {QFIS_PROGRAM} yyval14.make ; yyval14.extend (yyvs30.item (yyvsp30)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp14 := yyvsp14 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp30 := yyvsp30 -1
	if yyvsp14 >= yyvsc14 then
		if yyvs14 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs14")
			end
			create yyspecial_routines14
			yyvsc14 := yyInitial_yyvs_size
			yyvs14 := yyspecial_routines14.make (yyvsc14)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs14")
			end
			yyvsc14 := yyvsc14 + yyInitial_yyvs_size
			yyvs14 := yyspecial_routines14.resize (yyvs14, yyvsc14)
		end
	end
	yyvs14.put (yyval14, yyvsp14)
end
when 6 then
--|#line 198 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 198")
end

create {QFIS_PROGRAM} yyval14.make ; yyval14.extend (yyvs31.item (yyvsp31)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp14 := yyvsp14 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp31 := yyvsp31 -1
	if yyvsp14 >= yyvsc14 then
		if yyvs14 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs14")
			end
			create yyspecial_routines14
			yyvsc14 := yyInitial_yyvs_size
			yyvs14 := yyspecial_routines14.make (yyvsc14)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs14")
			end
			yyvsc14 := yyvsc14 + yyInitial_yyvs_size
			yyvs14 := yyspecial_routines14.resize (yyvs14, yyvsc14)
		end
	end
	yyvs14.put (yyval14, yyvsp14)
end
when 7 then
--|#line 199 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 199")
end

yyvs14.item (yyvsp14).put_front (yyvs15.item (yyvsp15)) ; yyval14 := yyvs14.item (yyvsp14) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp15 := yyvsp15 -1
	yyvs14.put (yyval14, yyvsp14)
end
when 8 then
--|#line 200 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 200")
end

yyvs14.item (yyvsp14).put_front (yyvs21.item (yyvsp21)) ; yyval14 := yyvs14.item (yyvsp14) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -1
	yyvsp21 := yyvsp21 -1
	yyvs14.put (yyval14, yyvsp14)
end
when 9 then
--|#line 201 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 201")
end

yyvs14.item (yyvsp14).put_front (yyvs30.item (yyvsp30)) ; yyval14 := yyvs14.item (yyvsp14) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -1
	yyvsp30 := yyvsp30 -1
	yyvs14.put (yyval14, yyvsp14)
end
when 10 then
--|#line 202 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 202")
end

yyvs14.item (yyvsp14).put_front (yyvs31.item (yyvsp31)) ; yyval14 := yyvs14.item (yyvsp14) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -1
	yyvsp31 := yyvsp31 -1
	yyvs14.put (yyval14, yyvsp14)
end
when 11 then
--|#line 205 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 205")
end

yyval3 := yyvs3.item (yyvsp3) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs3.put (yyval3, yyvsp3)
end
when 12 then
--|#line 206 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 206")
end

yyval3 := yyvs5.item (yyvsp5) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp3 := yyvsp3 + 1
	yyvsp5 := yyvsp5 -1
	if yyvsp3 >= yyvsc3 then
		if yyvs3 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs3")
			end
			create yyspecial_routines3
			yyvsc3 := yyInitial_yyvs_size
			yyvs3 := yyspecial_routines3.make (yyvsc3)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs3")
			end
			yyvsc3 := yyvsc3 + yyInitial_yyvs_size
			yyvs3 := yyspecial_routines3.resize (yyvs3, yyvsc3)
		end
	end
	yyvs3.put (yyval3, yyvsp3)
end
when 13 then
--|#line 209 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 209")
end

create yyval31.make (yyvs3.item (yyvsp3)) ; yyval31.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp31 := yyvsp31 + 1
	yyvsp3 := yyvsp3 -1
	if yyvsp31 >= yyvsc31 then
		if yyvs31 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs31")
			end
			create yyspecial_routines31
			yyvsc31 := yyInitial_yyvs_size
			yyvs31 := yyspecial_routines31.make (yyvsc31)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs31")
			end
			yyvsc31 := yyvsc31 + yyInitial_yyvs_size
			yyvs31 := yyspecial_routines31.resize (yyvs31, yyvsc31)
		end
	end
	yyvs31.put (yyval31, yyvsp31)
end
when 14 then
--|#line 212 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 212")
end

create yyval30.make (yyvs2.item (yyvsp2 - 2), yyvs2.item (yyvsp2)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp30 := yyvsp30 + 1
	yyvsp2 := yyvsp2 -3
	if yyvsp30 >= yyvsc30 then
		if yyvs30 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs30")
			end
			create yyspecial_routines30
			yyvsc30 := yyInitial_yyvs_size
			yyvs30 := yyspecial_routines30.make (yyvsc30)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs30")
			end
			yyvsc30 := yyvsc30 + yyInitial_yyvs_size
			yyvs30 := yyspecial_routines30.resize (yyvs30, yyvsc30)
		end
	end
	yyvs30.put (yyval30, yyvsp30)
end
when 15 then
--|#line 214 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 214")
end

create yyval30.make (yyvs2.item (yyvsp2 - 4), yyvs2.item (yyvsp2 - 2)) ; yyval30.set_file_name (yyvs2.item (yyvsp2)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 5
	yyvsp30 := yyvsp30 + 1
	yyvsp2 := yyvsp2 -5
	if yyvsp30 >= yyvsc30 then
		if yyvs30 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs30")
			end
			create yyspecial_routines30
			yyvsc30 := yyInitial_yyvs_size
			yyvs30 := yyspecial_routines30.make (yyvsc30)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs30")
			end
			yyvsc30 := yyvsc30 + yyInitial_yyvs_size
			yyvs30 := yyspecial_routines30.resize (yyvs30, yyvsc30)
		end
	end
	yyvs30.put (yyval30, yyvsp30)
end
when 16 then
--|#line 216 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 216")
end

create yyval30.make (yyvs2.item (yyvsp2 - 4), yyvs2.item (yyvsp2)) ; yyval30.set_domain (yyvs18.item (yyvsp18)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 6
	yyvsp30 := yyvsp30 + 1
	yyvsp2 := yyvsp2 -5
	yyvsp18 := yyvsp18 -1
	if yyvsp30 >= yyvsc30 then
		if yyvs30 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs30")
			end
			create yyspecial_routines30
			yyvsc30 := yyInitial_yyvs_size
			yyvs30 := yyspecial_routines30.make (yyvsc30)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs30")
			end
			yyvsc30 := yyvsc30 + yyInitial_yyvs_size
			yyvs30 := yyspecial_routines30.resize (yyvs30, yyvsc30)
		end
	end
	yyvs30.put (yyval30, yyvsp30)
end
when 17 then
--|#line 218 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 218")
end

create yyval30.make (yyvs2.item (yyvsp2 - 6), yyvs2.item (yyvsp2 - 2)) ; yyval30.set_domain (yyvs18.item (yyvsp18)) ; yyval30.set_file_name (yyvs2.item (yyvsp2)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 8
	yyvsp30 := yyvsp30 + 1
	yyvsp2 := yyvsp2 -7
	yyvsp18 := yyvsp18 -1
	if yyvsp30 >= yyvsc30 then
		if yyvs30 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs30")
			end
			create yyspecial_routines30
			yyvsc30 := yyInitial_yyvs_size
			yyvs30 := yyspecial_routines30.make (yyvsc30)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs30")
			end
			yyvsc30 := yyvsc30 + yyInitial_yyvs_size
			yyvs30 := yyspecial_routines30.resize (yyvs30, yyvsc30)
		end
	end
	yyvs30.put (yyval30, yyvsp30)
end
when 18 then
--|#line 222 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 222")
end

create yyval15.make (yyvs20.item (yyvsp20), yyvs16.item (yyvsp16), yyvs18.item (yyvsp18), yyvs19.item (yyvsp19), yyvs22.item (yyvsp22), yyvs17.item (yyvsp17)) ; yyval15.line_number := yyvs20.item (yyvsp20).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 7
	yyvsp15 := yyvsp15 + 1
	yyvsp20 := yyvsp20 -1
	yyvsp16 := yyvsp16 -1
	yyvsp18 := yyvsp18 -1
	yyvsp19 := yyvsp19 -1
	yyvsp22 := yyvsp22 -1
	yyvsp17 := yyvsp17 -1
	yyvsp2 := yyvsp2 -1
	if yyvsp15 >= yyvsc15 then
		if yyvs15 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs15")
			end
			create yyspecial_routines15
			yyvsc15 := yyInitial_yyvs_size
			yyvs15 := yyspecial_routines15.make (yyvsc15)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs15")
			end
			yyvsc15 := yyvsc15 + yyInitial_yyvs_size
			yyvs15 := yyspecial_routines15.resize (yyvs15, yyvsc15)
		end
	end
	yyvs15.put (yyval15, yyvsp15)
end
when 19 then
--|#line 226 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 226")
end

create yyval16.make ; yyval16.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 0
	yyvsp16 := yyvsp16 + 1
	if yyvsp16 >= yyvsc16 then
		if yyvs16 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs16")
			end
			create yyspecial_routines16
			yyvsc16 := yyInitial_yyvs_size
			yyvs16 := yyspecial_routines16.make (yyvsc16)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs16")
			end
			yyvsc16 := yyvsc16 + yyInitial_yyvs_size
			yyvs16 := yyspecial_routines16.resize (yyvs16, yyvsc16)
		end
	end
	yyvs16.put (yyval16, yyvsp16)
end
when 20 then
--|#line 227 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 227")
end

create yyval16.make ; yyval16.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp16 := yyvsp16 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp16 >= yyvsc16 then
		if yyvs16 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs16")
			end
			create yyspecial_routines16
			yyvsc16 := yyInitial_yyvs_size
			yyvs16 := yyspecial_routines16.make (yyvsc16)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs16")
			end
			yyvsc16 := yyvsc16 + yyInitial_yyvs_size
			yyvs16 := yyspecial_routines16.resize (yyvs16, yyvsc16)
		end
	end
	yyvs16.put (yyval16, yyvsp16)
end
when 21 then
--|#line 228 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 228")
end

create yyval16.make_from_list (yyvs28.item (yyvsp28)) ; yyval16.line_number := yyvs28.item (yyvsp28).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp16 := yyvsp16 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp28 := yyvsp28 -1
	if yyvsp16 >= yyvsc16 then
		if yyvs16 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs16")
			end
			create yyspecial_routines16
			yyvsc16 := yyInitial_yyvs_size
			yyvs16 := yyspecial_routines16.make (yyvsc16)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs16")
			end
			yyvsc16 := yyvsc16 + yyInitial_yyvs_size
			yyvs16 := yyspecial_routines16.resize (yyvs16, yyvsc16)
		end
	end
	yyvs16.put (yyval16, yyvsp16)
end
when 22 then
--|#line 231 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 231")
end

create yyval17.make ; yyval17.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 0
	yyvsp17 := yyvsp17 + 1
	if yyvsp17 >= yyvsc17 then
		if yyvs17 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs17")
			end
			create yyspecial_routines17
			yyvsc17 := yyInitial_yyvs_size
			yyvs17 := yyspecial_routines17.make (yyvsc17)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs17")
			end
			yyvsc17 := yyvsc17 + yyInitial_yyvs_size
			yyvs17 := yyspecial_routines17.resize (yyvs17, yyvsc17)
		end
	end
	yyvs17.put (yyval17, yyvsp17)
end
when 23 then
--|#line 232 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 232")
end

create yyval17.make ; yyval17.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp17 := yyvsp17 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp17 >= yyvsc17 then
		if yyvs17 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs17")
			end
			create yyspecial_routines17
			yyvsc17 := yyInitial_yyvs_size
			yyvs17 := yyspecial_routines17.make (yyvsc17)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs17")
			end
			yyvsc17 := yyvsc17 + yyInitial_yyvs_size
			yyvs17 := yyspecial_routines17.resize (yyvs17, yyvsc17)
		end
	end
	yyvs17.put (yyval17, yyvsp17)
end
when 24 then
--|#line 233 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 233")
end

create yyval17.make_from_list (yyvs28.item (yyvsp28)) ; yyval17.line_number := yyvs28.item (yyvsp28).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp17 := yyvsp17 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp28 := yyvsp28 -1
	if yyvsp17 >= yyvsc17 then
		if yyvs17 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs17")
			end
			create yyspecial_routines17
			yyvsc17 := yyInitial_yyvs_size
			yyvs17 := yyspecial_routines17.make (yyvsc17)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs17")
			end
			yyvsc17 := yyvsc17 + yyInitial_yyvs_size
			yyvs17 := yyspecial_routines17.resize (yyvs17, yyvsc17)
		end
	end
	yyvs17.put (yyval17, yyvsp17)
end
when 25 then
--|#line 236 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 236")
end

create yyval19.make ; yyval19.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 0
	yyvsp19 := yyvsp19 + 1
	if yyvsp19 >= yyvsc19 then
		if yyvs19 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs19")
			end
			create yyspecial_routines19
			yyvsc19 := yyInitial_yyvs_size
			yyvs19 := yyspecial_routines19.make (yyvsc19)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs19")
			end
			yyvsc19 := yyvsc19 + yyInitial_yyvs_size
			yyvs19 := yyspecial_routines19.resize (yyvs19, yyvsc19)
		end
	end
	yyvs19.put (yyval19, yyvsp19)
end
when 26 then
--|#line 237 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 237")
end

create yyval19.make ; yyval19.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp19 := yyvsp19 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp19 >= yyvsc19 then
		if yyvs19 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs19")
			end
			create yyspecial_routines19
			yyvsc19 := yyInitial_yyvs_size
			yyvs19 := yyspecial_routines19.make (yyvsc19)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs19")
			end
			yyvsc19 := yyvsc19 + yyInitial_yyvs_size
			yyvs19 := yyspecial_routines19.resize (yyvs19, yyvsc19)
		end
	end
	yyvs19.put (yyval19, yyvsp19)
end
when 27 then
--|#line 238 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 238")
end

create yyval19.make_from_declarations (yyvs21.item (yyvsp21)) ; yyval19.line_number := yyvs21.item (yyvsp21).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp19 := yyvsp19 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp21 := yyvsp21 -1
	if yyvsp19 >= yyvsc19 then
		if yyvs19 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs19")
			end
			create yyspecial_routines19
			yyvsc19 := yyInitial_yyvs_size
			yyvs19 := yyspecial_routines19.make (yyvsc19)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs19")
			end
			yyvsc19 := yyvsc19 + yyInitial_yyvs_size
			yyvs19 := yyspecial_routines19.resize (yyvs19, yyvsc19)
		end
	end
	yyvs19.put (yyval19, yyvsp19)
end
when 28 then
--|#line 241 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 241")
end

create yyval18.make ; yyval18.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 0
	yyvsp18 := yyvsp18 + 1
	if yyvsp18 >= yyvsc18 then
		if yyvs18 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs18")
			end
			create yyspecial_routines18
			yyvsc18 := yyInitial_yyvs_size
			yyvs18 := yyspecial_routines18.make (yyvsc18)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs18")
			end
			yyvsc18 := yyvsc18 + yyInitial_yyvs_size
			yyvs18 := yyspecial_routines18.resize (yyvs18, yyvsc18)
		end
	end
	yyvs18.put (yyval18, yyvsp18)
end
when 29 then
--|#line 242 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 242")
end

create yyval18.make ; yyval18.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp18 := yyvsp18 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp18 >= yyvsc18 then
		if yyvs18 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs18")
			end
			create yyspecial_routines18
			yyvsc18 := yyInitial_yyvs_size
			yyvs18 := yyspecial_routines18.make (yyvsc18)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs18")
			end
			yyvsc18 := yyvsc18 + yyInitial_yyvs_size
			yyvs18 := yyspecial_routines18.resize (yyvs18, yyvsc18)
		end
	end
	yyvs18.put (yyval18, yyvsp18)
end
when 30 then
--|#line 243 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 243")
end

yyval18 := yyvs18.item (yyvsp18) ; yyval18.line_number := yyvs18.item (yyvsp18).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp2 := yyvsp2 -1
	yyvs18.put (yyval18, yyvsp18)
end
when 31 then
--|#line 247 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 247")
end

create yyval20.make (yyvs2.item (yyvsp2), Void, Void) ; yyval20.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp20 := yyvsp20 + 1
	yyvsp2 := yyvsp2 -2
	if yyvsp20 >= yyvsc20 then
		if yyvs20 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs20")
			end
			create yyspecial_routines20
			yyvsc20 := yyInitial_yyvs_size
			yyvs20 := yyspecial_routines20.make (yyvsc20)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs20")
			end
			yyvsc20 := yyvsc20 + yyInitial_yyvs_size
			yyvs20 := yyspecial_routines20.resize (yyvs20, yyvsc20)
		end
	end
	yyvs20.put (yyval20, yyvsp20)
end
when 32 then
--|#line 248 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 248")
end

create yyval20.make (yyvs2.item (yyvsp2 - 2), yyvs21.item (yyvsp21), Void) ; yyval20.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 5
	yyvsp20 := yyvsp20 + 1
	yyvsp2 := yyvsp2 -4
	yyvsp21 := yyvsp21 -1
	if yyvsp20 >= yyvsc20 then
		if yyvs20 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs20")
			end
			create yyspecial_routines20
			yyvsc20 := yyInitial_yyvs_size
			yyvs20 := yyspecial_routines20.make (yyvsc20)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs20")
			end
			yyvsc20 := yyvsc20 + yyInitial_yyvs_size
			yyvs20 := yyspecial_routines20.resize (yyvs20, yyvsc20)
		end
	end
	yyvs20.put (yyval20, yyvsp20)
end
when 33 then
--|#line 250 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 250")
end

create yyval20.make (yyvs2.item (yyvsp2 - 3), Void, yyvs21.item (yyvsp21)) ; yyval20.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 6
	yyvsp20 := yyvsp20 + 1
	yyvsp2 := yyvsp2 -5
	yyvsp21 := yyvsp21 -1
	if yyvsp20 >= yyvsc20 then
		if yyvs20 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs20")
			end
			create yyspecial_routines20
			yyvsc20 := yyInitial_yyvs_size
			yyvs20 := yyspecial_routines20.make (yyvsc20)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs20")
			end
			yyvsc20 := yyvsc20 + yyInitial_yyvs_size
			yyvs20 := yyspecial_routines20.resize (yyvs20, yyvsc20)
		end
	end
	yyvs20.put (yyval20, yyvsp20)
end
when 34 then
--|#line 252 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 252")
end

create yyval20.make (yyvs2.item (yyvsp2 - 5), yyvs21.item (yyvsp21 - 1), yyvs21.item (yyvsp21)) ; yyval20.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 9
	yyvsp20 := yyvsp20 + 1
	yyvsp2 := yyvsp2 -7
	yyvsp21 := yyvsp21 -2
	if yyvsp20 >= yyvsc20 then
		if yyvs20 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs20")
			end
			create yyspecial_routines20
			yyvsc20 := yyInitial_yyvs_size
			yyvs20 := yyspecial_routines20.make (yyvsc20)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs20")
			end
			yyvsc20 := yyvsc20 + yyInitial_yyvs_size
			yyvs20 := yyspecial_routines20.resize (yyvs20, yyvsc20)
		end
	end
	yyvs20.put (yyval20, yyvsp20)
end
when 35 then
--|#line 256 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 256")
end

yyval2 := yyvs2.item (yyvsp2) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs2.put (yyval2, yyvsp2)
end
when 36 then
--|#line 257 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 257")
end

yyval2 := yyvs2.item (yyvsp2) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs2.put (yyval2, yyvsp2)
end
when 37 then
--|#line 258 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 258")
end

yyval2 := yyvs2.item (yyvsp2) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs2.put (yyval2, yyvsp2)
end
when 38 then
--|#line 261 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 261")
end

create yyval21.make ; if yyvs2.item (yyvsp2) ~ token_name (BOOLEAN) then yyval21.add_booleans (yyvs18.item (yyvsp18)) elseif yyvs2.item (yyvsp2) ~ token_name (SEQUENCE) then yyval21.add_sequences (yyvs18.item (yyvsp18)) elseif yyvs2.item (yyvsp2) ~ token_name (INTEGER) then yyval21.add_integers (yyvs18.item (yyvsp18)) end ; yyval21.line_number := yyvs18.item (yyvsp18).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp21 := yyvsp21 + 1
	yyvsp18 := yyvsp18 -1
	yyvsp2 := yyvsp2 -2
	if yyvsp21 >= yyvsc21 then
		if yyvs21 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs21")
			end
			create yyspecial_routines21
			yyvsc21 := yyInitial_yyvs_size
			yyvs21 := yyspecial_routines21.make (yyvsc21)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs21")
			end
			yyvsc21 := yyvsc21 + yyInitial_yyvs_size
			yyvs21 := yyspecial_routines21.resize (yyvs21, yyvsc21)
		end
	end
	yyvs21.put (yyval21, yyvsp21)
end
when 39 then
--|#line 263 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 263")
end

yyval21 := yyvs21.item (yyvsp21) ; if yyvs2.item (yyvsp2) ~ token_name (BOOLEAN) then yyval21.add_booleans (yyvs18.item (yyvsp18)) elseif yyvs2.item (yyvsp2) ~ token_name (SEQUENCE) then yyval21.add_sequences (yyvs18.item (yyvsp18)) elseif yyvs2.item (yyvsp2) ~ token_name (INTEGER) then yyval21.add_integers (yyvs18.item (yyvsp18)) end ; yyval21.line_number := yyvs21.item (yyvsp21).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 5
	yyvsp2 := yyvsp2 -3
	yyvsp18 := yyvsp18 -1
	yyvs21.put (yyval21, yyvsp21)
end
when 40 then
--|#line 267 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 267")
end

create yyval18.make ; yyval18.extend (yyvs2.item (yyvsp2)) ; yyval18.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp18 := yyvsp18 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp18 >= yyvsc18 then
		if yyvs18 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs18")
			end
			create yyspecial_routines18
			yyvsc18 := yyInitial_yyvs_size
			yyvs18 := yyspecial_routines18.make (yyvsc18)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs18")
			end
			yyvsc18 := yyvsc18 + yyInitial_yyvs_size
			yyvs18 := yyspecial_routines18.resize (yyvs18, yyvsc18)
		end
	end
	yyvs18.put (yyval18, yyvsp18)
end
when 41 then
--|#line 268 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 268")
end

yyval18 := yyvs18.item (yyvsp18) ; yyval18.put_front (yyvs2.item (yyvsp2 - 1)) ; yyval18.line_number := yyvs18.item (yyvsp18).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -2
	yyvs18.put (yyval18, yyvsp18)
end
when 42 then
--|#line 272 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 272")
end

yyval2 := yyvs2.item (yyvsp2) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs2.put (yyval2, yyvsp2)
end
when 43 then
--|#line 273 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 273")
end

yyval2 := yyvs2.item (yyvsp2) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs2.put (yyval2, yyvsp2)
end
when 44 then
--|#line 274 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 274")
end

yyval2 := yyvs2.item (yyvsp2) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs2.put (yyval2, yyvsp2)
end
when 45 then
--|#line 277 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 277")
end

create yyval18.make ; yyval18.extend (yyvs2.item (yyvsp2)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp18 := yyvsp18 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp18 >= yyvsc18 then
		if yyvs18 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs18")
			end
			create yyspecial_routines18
			yyvsc18 := yyInitial_yyvs_size
			yyvs18 := yyspecial_routines18.make (yyvsc18)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs18")
			end
			yyvsc18 := yyvsc18 + yyInitial_yyvs_size
			yyvs18 := yyspecial_routines18.resize (yyvs18, yyvsc18)
		end
	end
	yyvs18.put (yyval18, yyvsp18)
end
when 46 then
--|#line 278 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 278")
end

yyvs18.item (yyvsp18).put_front (yyvs2.item (yyvsp2 - 1)) ; yyval18 := yyvs18.item (yyvsp18) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -2
	yyvs18.put (yyval18, yyvsp18)
end
when 47 then
--|#line 282 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 282")
end

create yyval22.make ; yyval22.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp22 := yyvsp22 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp22 >= yyvsc22 then
		if yyvs22 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs22")
			end
			create yyspecial_routines22
			yyvsc22 := yyInitial_yyvs_size
			yyvs22 := yyspecial_routines22.make (yyvsc22)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs22")
			end
			yyvsc22 := yyvsc22 + yyInitial_yyvs_size
			yyvs22 := yyspecial_routines22.resize (yyvs22, yyvsc22)
		end
	end
	yyvs22.put (yyval22, yyvsp22)
end
when 48 then
--|#line 283 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 283")
end

create yyval22.make_with_instructions (yyvs24.item (yyvsp24)) ; yyval22.line_number := yyvs24.item (yyvsp24).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp22 := yyvsp22 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp24 := yyvsp24 -1
	if yyvsp22 >= yyvsc22 then
		if yyvs22 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs22")
			end
			create yyspecial_routines22
			yyvsc22 := yyInitial_yyvs_size
			yyvs22 := yyspecial_routines22.make (yyvsc22)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs22")
			end
			yyvsc22 := yyvsc22 + yyInitial_yyvs_size
			yyvs22 := yyspecial_routines22.resize (yyvs22, yyvsc22)
		end
	end
	yyvs22.put (yyval22, yyvsp22)
end
when 49 then
--|#line 286 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 286")
end

create yyval24.make ; yyval24.extend (yyvs23.item (yyvsp23)) ; yyval24.line_number := yyvs23.item (yyvsp23).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp24 := yyvsp24 + 1
	yyvsp23 := yyvsp23 -1
	if yyvsp24 >= yyvsc24 then
		if yyvs24 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs24")
			end
			create yyspecial_routines24
			yyvsc24 := yyInitial_yyvs_size
			yyvs24 := yyspecial_routines24.make (yyvsc24)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs24")
			end
			yyvsc24 := yyvsc24 + yyInitial_yyvs_size
			yyvs24 := yyspecial_routines24.resize (yyvs24, yyvsc24)
		end
	end
	yyvs24.put (yyval24, yyvsp24)
end
when 50 then
--|#line 287 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 287")
end

yyval24 := yyvs24.item (yyvsp24) ; yyval24.put_front (yyvs23.item (yyvsp23)) ; yyval24.line_number := yyvs23.item (yyvsp23).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp23 := yyvsp23 -1
	yyvs24.put (yyval24, yyvsp24)
end
when 51 then
--|#line 289 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 289")
end

yyval24 := yyvs24.item (yyvsp24) ; yyval24.put_front (yyvs23.item (yyvsp23)) ; yyval24.line_number := yyvs23.item (yyvsp23).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp23 := yyvsp23 -1
	yyvsp2 := yyvsp2 -1
	yyvs24.put (yyval24, yyvsp24)
end
when 52 then
--|#line 293 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 293")
end

create {QFIS_SKIP} yyval23 ; yyval23.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp23 := yyvsp23 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 53 then
--|#line 295 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 295")
end

create {QFIS_ASSERT} yyval23.make (yyvs3.item (yyvsp3)) ; yyval23.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp23 := yyvsp23 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp3 := yyvsp3 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 54 then
--|#line 297 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 297")
end

create {QFIS_ASSUME} yyval23.make (yyvs3.item (yyvsp3)) ; yyval23.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp23 := yyvsp23 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp3 := yyvsp3 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 55 then
--|#line 299 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 299")
end

create {QFIS_HAVOC} yyval23.make (yyvs18.item (yyvsp18)) ; yyval23.line_number := yyvs18.item (yyvsp18).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp23 := yyvsp23 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp18 := yyvsp18 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 56 then
--|#line 301 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 301")
end

create {QFIS_SPLIT} yyval23.make (yyvs6.item (yyvsp6), yyvs18.item (yyvsp18)) ; yyval23.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 4
	yyvsp23 := yyvsp23 + 1
	yyvsp2 := yyvsp2 -2
	yyvsp6 := yyvsp6 -1
	yyvsp18 := yyvsp18 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 57 then
--|#line 303 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 303")
end

create {QFIS_ASSIGN_SEQUENCE} yyval23.make (yyvs2.item (yyvsp2 - 1), yyvs6.item (yyvsp6)) ; yyval23.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp23 := yyvsp23 + 1
	yyvsp2 := yyvsp2 -2
	yyvsp6 := yyvsp6 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 58 then
--|#line 305 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 305")
end

create {QFIS_ASSIGN_BOOLEAN} yyval23.make (yyvs2.item (yyvsp2 - 1), yyvs3.item (yyvsp3)) ; yyval23.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp23 := yyvsp23 + 1
	yyvsp2 := yyvsp2 -2
	yyvsp3 := yyvsp3 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 59 then
--|#line 307 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 307")
end

create {QFIS_ASSIGN_INTEGER} yyval23.make (yyvs2.item (yyvsp2 - 1), yyvs11.item (yyvsp11)) ; yyval23.line_number := yyvs11.item (yyvsp11).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp23 := yyvsp23 + 1
	yyvsp2 := yyvsp2 -2
	yyvsp11 := yyvsp11 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 60 then
--|#line 309 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 309")
end

yyval23 := yyvs25.item (yyvsp25) ; yyval23.line_number := yyvs25.item (yyvsp25).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp23 := yyvsp23 + 1
	yyvsp25 := yyvsp25 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 61 then
--|#line 311 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 311")
end

yyval23 := yyvs26.item (yyvsp26) ; yyval23.line_number := yyvs26.item (yyvsp26).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp23 := yyvsp23 + 1
	yyvsp26 := yyvsp26 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 62 then
--|#line 313 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 313")
end

yyval23 := yyvs29.item (yyvsp29) ; yyval23.line_number := yyvs29.item (yyvsp29).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp23 := yyvsp23 + 1
	yyvsp29 := yyvsp29 -1
	if yyvsp23 >= yyvsc23 then
		if yyvs23 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs23")
			end
			create yyspecial_routines23
			yyvsc23 := yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.make (yyvsc23)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs23")
			end
			yyvsc23 := yyvsc23 + yyInitial_yyvs_size
			yyvs23 := yyspecial_routines23.resize (yyvs23, yyvsc23)
		end
	end
	yyvs23.put (yyval23, yyvsp23)
end
when 63 then
--|#line 317 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 317")
end

create yyval25.make (yyvs3.item (yyvsp3), yyvs24.item (yyvsp24), Void) ; yyval25.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 4
	yyvsp25 := yyvsp25 + 1
	yyvsp2 := yyvsp2 -2
	yyvsp3 := yyvsp3 -1
	yyvsp24 := yyvsp24 -1
	if yyvsp25 >= yyvsc25 then
		if yyvs25 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs25")
			end
			create yyspecial_routines25
			yyvsc25 := yyInitial_yyvs_size
			yyvs25 := yyspecial_routines25.make (yyvsc25)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs25")
			end
			yyvsc25 := yyvsc25 + yyInitial_yyvs_size
			yyvs25 := yyspecial_routines25.resize (yyvs25, yyvsc25)
		end
	end
	yyvs25.put (yyval25, yyvsp25)
end
when 64 then
--|#line 318 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 318")
end

create yyval25.make (yyvs3.item (yyvsp3), yyvs24.item (yyvsp24 - 1), yyvs24.item (yyvsp24)) ; yyval25.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 5
	yyvsp25 := yyvsp25 + 1
	yyvsp2 := yyvsp2 -2
	yyvsp3 := yyvsp3 -1
	yyvsp24 := yyvsp24 -2
	if yyvsp25 >= yyvsc25 then
		if yyvs25 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs25")
			end
			create yyspecial_routines25
			yyvsc25 := yyInitial_yyvs_size
			yyvs25 := yyspecial_routines25.make (yyvsc25)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs25")
			end
			yyvsc25 := yyvsc25 + yyInitial_yyvs_size
			yyvs25 := yyspecial_routines25.resize (yyvs25, yyvsc25)
		end
	end
	yyvs25.put (yyval25, yyvsp25)
end
when 65 then
--|#line 321 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 321")
end

yyval24 := Void 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp24 := yyvsp24 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp24 >= yyvsc24 then
		if yyvs24 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs24")
			end
			create yyspecial_routines24
			yyvsc24 := yyInitial_yyvs_size
			yyvs24 := yyspecial_routines24.make (yyvsc24)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs24")
			end
			yyvsc24 := yyvsc24 + yyInitial_yyvs_size
			yyvs24 := yyspecial_routines24.resize (yyvs24, yyvsc24)
		end
	end
	yyvs24.put (yyval24, yyvsp24)
end
when 66 then
--|#line 322 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 322")
end

yyval24 := yyvs24.item (yyvsp24) ; yyval24.line_number := yyvs24.item (yyvsp24).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp2 := yyvsp2 -1
	yyvs24.put (yyval24, yyvsp24)
end
when 67 then
--|#line 325 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 325")
end

yyval24 := Void 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp24 := yyvsp24 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp24 >= yyvsc24 then
		if yyvs24 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs24")
			end
			create yyspecial_routines24
			yyvsc24 := yyInitial_yyvs_size
			yyvs24 := yyspecial_routines24.make (yyvsc24)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs24")
			end
			yyvsc24 := yyvsc24 + yyInitial_yyvs_size
			yyvs24 := yyspecial_routines24.resize (yyvs24, yyvsc24)
		end
	end
	yyvs24.put (yyval24, yyvsp24)
end
when 68 then
--|#line 326 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 326")
end

yyval24 := yyvs24.item (yyvsp24) ; yyval24.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp2 := yyvsp2 -1
	yyvs24.put (yyval24, yyvsp24)
end
when 69 then
--|#line 329 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 329")
end

yyval3 := Void ; yyval3.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp3 := yyvsp3 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp3 >= yyvsc3 then
		if yyvs3 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs3")
			end
			create yyspecial_routines3
			yyvsc3 := yyInitial_yyvs_size
			yyvs3 := yyspecial_routines3.make (yyvsc3)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs3")
			end
			yyvsc3 := yyvsc3 + yyInitial_yyvs_size
			yyvs3 := yyspecial_routines3.resize (yyvs3, yyvsc3)
		end
	end
	yyvs3.put (yyval3, yyvsp3)
end
when 70 then
--|#line 330 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 330")
end

yyval3 := yyvs3.item (yyvsp3) ; yyval3.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs3.put (yyval3, yyvsp3)
end
when 71 then
--|#line 333 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 333")
end

create yyval26.make (yyvs3.item (yyvsp3), yyvs27.item (yyvsp27), yyvs24.item (yyvsp24)) ; yyval26.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 5
	yyvsp26 := yyvsp26 + 1
	yyvsp2 := yyvsp2 -2
	yyvsp3 := yyvsp3 -1
	yyvsp27 := yyvsp27 -1
	yyvsp24 := yyvsp24 -1
	if yyvsp26 >= yyvsc26 then
		if yyvs26 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs26")
			end
			create yyspecial_routines26
			yyvsc26 := yyInitial_yyvs_size
			yyvs26 := yyspecial_routines26.make (yyvsc26)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs26")
			end
			yyvsc26 := yyvsc26 + yyInitial_yyvs_size
			yyvs26 := yyspecial_routines26.resize (yyvs26, yyvsc26)
		end
	end
	yyvs26.put (yyval26, yyvsp26)
end
when 72 then
--|#line 336 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 336")
end

create yyval27.make ; yyval27.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 0
	yyvsp27 := yyvsp27 + 1
	if yyvsp27 >= yyvsc27 then
		if yyvs27 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs27")
			end
			create yyspecial_routines27
			yyvsc27 := yyInitial_yyvs_size
			yyvs27 := yyspecial_routines27.make (yyvsc27)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs27")
			end
			yyvsc27 := yyvsc27 + yyInitial_yyvs_size
			yyvs27 := yyspecial_routines27.resize (yyvs27, yyvsc27)
		end
	end
	yyvs27.put (yyval27, yyvsp27)
end
when 73 then
--|#line 337 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 337")
end

create yyval27.make ; yyval27.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp27 := yyvsp27 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp27 >= yyvsc27 then
		if yyvs27 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs27")
			end
			create yyspecial_routines27
			yyvsc27 := yyInitial_yyvs_size
			yyvs27 := yyspecial_routines27.make (yyvsc27)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs27")
			end
			yyvsc27 := yyvsc27 + yyInitial_yyvs_size
			yyvs27 := yyspecial_routines27.resize (yyvs27, yyvsc27)
		end
	end
	yyvs27.put (yyval27, yyvsp27)
end
when 74 then
--|#line 338 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 338")
end

create yyval27.make_from_list (yyvs28.item (yyvsp28)) ; yyval27.line_number := yyvs28.item (yyvsp28).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp27 := yyvsp27 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp28 := yyvsp28 -1
	if yyvsp27 >= yyvsc27 then
		if yyvs27 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs27")
			end
			create yyspecial_routines27
			yyvsc27 := yyInitial_yyvs_size
			yyvs27 := yyspecial_routines27.make (yyvsc27)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs27")
			end
			yyvsc27 := yyvsc27 + yyInitial_yyvs_size
			yyvs27 := yyspecial_routines27.resize (yyvs27, yyvsc27)
		end
	end
	yyvs27.put (yyval27, yyvsp27)
end
when 75 then
--|#line 341 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 341")
end

create yyval24.make ; yyval24.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp24 := yyvsp24 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp24 >= yyvsc24 then
		if yyvs24 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs24")
			end
			create yyspecial_routines24
			yyvsc24 := yyInitial_yyvs_size
			yyvs24 := yyspecial_routines24.make (yyvsc24)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs24")
			end
			yyvsc24 := yyvsc24 + yyInitial_yyvs_size
			yyvs24 := yyspecial_routines24.resize (yyvs24, yyvsc24)
		end
	end
	yyvs24.put (yyval24, yyvsp24)
end
when 76 then
--|#line 342 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 342")
end

yyval24 := yyvs24.item (yyvsp24) ; yyval24.line_number := yyvs24.item (yyvsp24).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp2 := yyvsp2 -1
	yyvs24.put (yyval24, yyvsp24)
end
when 77 then
--|#line 345 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 345")
end

create yyval28.make ; yyval28.extend (yyvs3.item (yyvsp3)) ; yyval28.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp28 := yyvsp28 + 1
	yyvsp3 := yyvsp3 -1
	if yyvsp28 >= yyvsc28 then
		if yyvs28 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs28")
			end
			create yyspecial_routines28
			yyvsc28 := yyInitial_yyvs_size
			yyvs28 := yyspecial_routines28.make (yyvsc28)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs28")
			end
			yyvsc28 := yyvsc28 + yyInitial_yyvs_size
			yyvs28 := yyspecial_routines28.resize (yyvs28, yyvsc28)
		end
	end
	yyvs28.put (yyval28, yyvsp28)
end
when 78 then
--|#line 346 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 346")
end

yyvs3.item (yyvsp3).set_label (yyvs2.item (yyvsp2)) ; create yyval28.make ; yyval28.extend (yyvs3.item (yyvsp3)) ; yyval28.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp28 := yyvsp28 + 1
	yyvsp2 := yyvsp2 -1
	yyvsp3 := yyvsp3 -1
	if yyvsp28 >= yyvsc28 then
		if yyvs28 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs28")
			end
			create yyspecial_routines28
			yyvsc28 := yyInitial_yyvs_size
			yyvs28 := yyspecial_routines28.make (yyvsc28)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs28")
			end
			yyvsc28 := yyvsc28 + yyInitial_yyvs_size
			yyvs28 := yyspecial_routines28.resize (yyvs28, yyvsc28)
		end
	end
	yyvs28.put (yyval28, yyvsp28)
end
when 79 then
--|#line 347 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 347")
end

yyvs28.item (yyvsp28).extend (yyvs3.item (yyvsp3)) ; yyval28 := yyvs28.item (yyvsp28) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -1
	yyvsp3 := yyvsp3 -1
	yyvs28.put (yyval28, yyvsp28)
end
when 80 then
--|#line 349 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 349")
end

yyvs3.item (yyvsp3).set_label (yyvs2.item (yyvsp2)) ; yyvs28.item (yyvsp28).extend (yyvs3.item (yyvsp3)) ; yyval28 := yyvs28.item (yyvsp28) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 4
	yyvsp2 := yyvsp2 -2
	yyvsp3 := yyvsp3 -1
	yyvs28.put (yyval28, yyvsp28)
end
when 81 then
--|#line 351 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 351")
end

yyvs3.item (yyvsp3).set_label (yyvs2.item (yyvsp2)) ; yyvs28.item (yyvsp28).extend (yyvs3.item (yyvsp3)) ; yyval28 := yyvs28.item (yyvsp28) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -1
	yyvsp3 := yyvsp3 -1
	yyvs28.put (yyval28, yyvsp28)
end
when 82 then
--|#line 355 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 355")
end

create {QFIS_CALL_COMMAND} yyval29.make (yyvs2.item (yyvsp2), Void) ; yyval29.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp29 := yyvsp29 + 1
	yyvsp2 := yyvsp2 -2
	if yyvsp29 >= yyvsc29 then
		if yyvs29 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs29")
			end
			create yyspecial_routines29
			yyvsc29 := yyInitial_yyvs_size
			yyvs29 := yyspecial_routines29.make (yyvsc29)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs29")
			end
			yyvsc29 := yyvsc29 + yyInitial_yyvs_size
			yyvs29 := yyspecial_routines29.resize (yyvs29, yyvsc29)
		end
	end
	yyvs29.put (yyval29, yyvsp29)
end
when 83 then
--|#line 357 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 357")
end

create {QFIS_CALL_COMMAND} yyval29.make (yyvs2.item (yyvsp2 - 2), yyvs18.item (yyvsp18)) ; yyval29.line_number := yyvs18.item (yyvsp18).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 5
	yyvsp29 := yyvsp29 + 1
	yyvsp2 := yyvsp2 -4
	yyvsp18 := yyvsp18 -1
	if yyvsp29 >= yyvsc29 then
		if yyvs29 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs29")
			end
			create yyspecial_routines29
			yyvsc29 := yyInitial_yyvs_size
			yyvs29 := yyspecial_routines29.make (yyvsc29)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs29")
			end
			yyvsc29 := yyvsc29 + yyInitial_yyvs_size
			yyvs29 := yyspecial_routines29.resize (yyvs29, yyvsc29)
		end
	end
	yyvs29.put (yyval29, yyvsp29)
end
when 84 then
--|#line 359 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 359")
end

create {QFIS_CALL_FUNCTION} yyval29.make (yyvs2.item (yyvsp2), Void, yyvs18.item (yyvsp18)) ; yyval29.line_number := yyvs18.item (yyvsp18).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 4
	yyvsp29 := yyvsp29 + 1
	yyvsp2 := yyvsp2 -3
	yyvsp18 := yyvsp18 -1
	if yyvsp29 >= yyvsc29 then
		if yyvs29 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs29")
			end
			create yyspecial_routines29
			yyvsc29 := yyInitial_yyvs_size
			yyvs29 := yyspecial_routines29.make (yyvsc29)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs29")
			end
			yyvsc29 := yyvsc29 + yyInitial_yyvs_size
			yyvs29 := yyspecial_routines29.resize (yyvs29, yyvsc29)
		end
	end
	yyvs29.put (yyval29, yyvsp29)
end
when 85 then
--|#line 361 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 361")
end

create {QFIS_CALL_FUNCTION} yyval29.make (yyvs2.item (yyvsp2 - 2), yyvs18.item (yyvsp18), yyvs18.item (yyvsp18 - 1)) ; yyval29.line_number := yyvs18.item (yyvsp18 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 7
	yyvsp29 := yyvsp29 + 1
	yyvsp2 := yyvsp2 -5
	yyvsp18 := yyvsp18 -2
	if yyvsp29 >= yyvsc29 then
		if yyvs29 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs29")
			end
			create yyspecial_routines29
			yyvsc29 := yyInitial_yyvs_size
			yyvs29 := yyspecial_routines29.make (yyvsc29)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs29")
			end
			yyvsc29 := yyvsc29 + yyInitial_yyvs_size
			yyvs29 := yyspecial_routines29.resize (yyvs29, yyvsc29)
		end
	end
	yyvs29.put (yyval29, yyvsp29)
end
when 86 then
--|#line 368 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 368")
end

yyval3 := yyvs4.item (yyvsp4) ; yyval3.line_number := yyvs4.item (yyvsp4).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp3 := yyvsp3 + 1
	yyvsp4 := yyvsp4 -1
	if yyvsp3 >= yyvsc3 then
		if yyvs3 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs3")
			end
			create yyspecial_routines3
			yyvsc3 := yyInitial_yyvs_size
			yyvs3 := yyspecial_routines3.make (yyvsc3)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs3")
			end
			yyvsc3 := yyvsc3 + yyInitial_yyvs_size
			yyvs3 := yyspecial_routines3.resize (yyvs3, yyvsc3)
		end
	end
	yyvs3.put (yyval3, yyvsp3)
end
when 87 then
--|#line 370 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 370")
end

create {QFIS_NOT_FORMULA} yyval3.make (yyvs3.item (yyvsp3)) ; yyval3.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp2 := yyvsp2 -1
	yyvs3.put (yyval3, yyvsp3)
end
when 88 then
--|#line 372 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 372")
end

create {QFIS_PREDICATE_FORMULA} yyval3.make (yyvs2.item (yyvsp2 - 2), yyvs7.item (yyvsp7)) ; yyval3.line_number := yyvs7.item (yyvsp7).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 4
	yyvsp3 := yyvsp3 + 1
	yyvsp2 := yyvsp2 -3
	yyvsp7 := yyvsp7 -1
	if yyvsp3 >= yyvsc3 then
		if yyvs3 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs3")
			end
			create yyspecial_routines3
			yyvsc3 := yyInitial_yyvs_size
			yyvs3 := yyspecial_routines3.make (yyvsc3)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs3")
			end
			yyvsc3 := yyvsc3 + yyInitial_yyvs_size
			yyvs3 := yyspecial_routines3.resize (yyvs3, yyvsc3)
		end
	end
	yyvs3.put (yyval3, yyvsp3)
end
when 89 then
--|#line 374 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 374")
end

yyval3 := yyvs3.item (yyvsp3) ; yyval3.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -2
	yyvs3.put (yyval3, yyvsp3)
end
when 90 then
--|#line 376 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 376")
end

create {QFIS_AND_FORMULA} yyval3.make (yyvs3.item (yyvsp3 - 1), yyvs3.item (yyvsp3)) ; yyval3.line_number := yyvs3.item (yyvsp3 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp3 := yyvsp3 -1
	yyvsp2 := yyvsp2 -1
	yyvs3.put (yyval3, yyvsp3)
end
when 91 then
--|#line 378 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 378")
end

create {QFIS_OR_FORMULA} yyval3.make (yyvs3.item (yyvsp3 - 1), yyvs3.item (yyvsp3)) ; yyval3.line_number := yyvs3.item (yyvsp3 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp3 := yyvsp3 -1
	yyvsp2 := yyvsp2 -1
	yyvs3.put (yyval3, yyvsp3)
end
when 92 then
--|#line 380 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 380")
end

create {QFIS_IMPLIES_FORMULA} yyval3.make (yyvs3.item (yyvsp3 - 1), yyvs3.item (yyvsp3)) ; yyval3.line_number := yyvs3.item (yyvsp3 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp3 := yyvsp3 -1
	yyvsp2 := yyvsp2 -1
	yyvs3.put (yyval3, yyvsp3)
end
when 93 then
--|#line 382 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 382")
end

create {QFIS_IFF_FORMULA} yyval3.make (yyvs3.item (yyvsp3 - 1), yyvs3.item (yyvsp3)) ; yyval3.line_number := yyvs3.item (yyvsp3 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp3 := yyvsp3 -1
	yyvsp2 := yyvsp2 -1
	yyvs3.put (yyval3, yyvsp3)
end
when 94 then
--|#line 386 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 386")
end

create {QFIS_FORALL_FORMULA} yyval5.make (yyvs21.item (yyvsp21), yyvs3.item (yyvsp3)) ; yyval5.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 7
	yyvsp5 := yyvsp5 + 1
	yyvsp2 := yyvsp2 -5
	yyvsp21 := yyvsp21 -1
	yyvsp3 := yyvsp3 -1
	if yyvsp5 >= yyvsc5 then
		if yyvs5 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs5")
			end
			create yyspecial_routines5
			yyvsc5 := yyInitial_yyvs_size
			yyvs5 := yyspecial_routines5.make (yyvsc5)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs5")
			end
			yyvsc5 := yyvsc5 + yyInitial_yyvs_size
			yyvs5 := yyspecial_routines5.resize (yyvs5, yyvsc5)
		end
	end
	yyvs5.put (yyval5, yyvsp5)
end
when 95 then
--|#line 387 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 387")
end

create {QFIS_EXIST_FORMULA} yyval5.make (yyvs21.item (yyvsp21), yyvs3.item (yyvsp3)) ; yyval5.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 7
	yyvsp5 := yyvsp5 + 1
	yyvsp2 := yyvsp2 -5
	yyvsp21 := yyvsp21 -1
	yyvsp3 := yyvsp3 -1
	if yyvsp5 >= yyvsc5 then
		if yyvs5 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs5")
			end
			create yyspecial_routines5
			yyvsc5 := yyInitial_yyvs_size
			yyvs5 := yyspecial_routines5.make (yyvsc5)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs5")
			end
			yyvsc5 := yyvsc5 + yyInitial_yyvs_size
			yyvs5 := yyspecial_routines5.resize (yyvs5, yyvsc5)
		end
	end
	yyvs5.put (yyval5, yyvsp5)
end
when 96 then
--|#line 391 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 391")
end

create {QFIS_BOOLEAN_CONSTANT} yyval4.make (True) ; yyval4.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp4 := yyvsp4 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp4 >= yyvsc4 then
		if yyvs4 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs4")
			end
			create yyspecial_routines4
			yyvsc4 := yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.make (yyvsc4)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs4")
			end
			yyvsc4 := yyvsc4 + yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.resize (yyvs4, yyvsc4)
		end
	end
	yyvs4.put (yyval4, yyvsp4)
end
when 97 then
--|#line 392 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 392")
end

create {QFIS_BOOLEAN_CONSTANT} yyval4.make (False) ; yyval4.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp4 := yyvsp4 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp4 >= yyvsc4 then
		if yyvs4 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs4")
			end
			create yyspecial_routines4
			yyvsc4 := yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.make (yyvsc4)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs4")
			end
			yyvsc4 := yyvsc4 + yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.resize (yyvs4, yyvsc4)
		end
	end
	yyvs4.put (yyval4, yyvsp4)
end
when 98 then
--|#line 393 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 393")
end

create {QFIS_BOOLEAN_VARIABLE} yyval4.make (yyvs2.item (yyvsp2)) ; yyval4.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp4 := yyvsp4 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp4 >= yyvsc4 then
		if yyvs4 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs4")
			end
			create yyspecial_routines4
			yyvsc4 := yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.make (yyvsc4)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs4")
			end
			yyvsc4 := yyvsc4 + yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.resize (yyvs4, yyvsc4)
		end
	end
	yyvs4.put (yyval4, yyvsp4)
end
when 99 then
--|#line 394 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 394")
end

create {QFIS_BOOLEAN_VARIABLE} yyval4.make (yyvs2.item (yyvsp2)) ; yyval4.set_old ; yyval4.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp4 := yyvsp4 + 1
	yyvsp2 := yyvsp2 -2
	if yyvsp4 >= yyvsc4 then
		if yyvs4 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs4")
			end
			create yyspecial_routines4
			yyvsc4 := yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.make (yyvsc4)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs4")
			end
			yyvsc4 := yyvsc4 + yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.resize (yyvs4, yyvsc4)
		end
	end
	yyvs4.put (yyval4, yyvsp4)
end
when 100 then
--|#line 396 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 396")
end

yyvs8.item (yyvsp8).set (yyvs6.item (yyvsp6 - 1), yyvs6.item (yyvsp6)) ; yyval4 := yyvs8.item (yyvsp8) ; yyval4.line_number := yyvs6.item (yyvsp6 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp4 := yyvsp4 + 1
	yyvsp6 := yyvsp6 -2
	yyvsp8 := yyvsp8 -1
	if yyvsp4 >= yyvsc4 then
		if yyvs4 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs4")
			end
			create yyspecial_routines4
			yyvsc4 := yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.make (yyvsc4)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs4")
			end
			yyvsc4 := yyvsc4 + yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.resize (yyvs4, yyvsc4)
		end
	end
	yyvs4.put (yyval4, yyvsp4)
end
when 101 then
--|#line 398 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 398")
end

yyvs9.item (yyvsp9).set (yyvs11.item (yyvsp11 - 1), yyvs11.item (yyvsp11)) ; yyval4 := yyvs9.item (yyvsp9) ; yyval4.line_number := yyvs11.item (yyvsp11 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp4 := yyvsp4 + 1
	yyvsp11 := yyvsp11 -2
	yyvsp9 := yyvsp9 -1
	if yyvsp4 >= yyvsc4 then
		if yyvs4 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs4")
			end
			create yyspecial_routines4
			yyvsc4 := yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.make (yyvsc4)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs4")
			end
			yyvsc4 := yyvsc4 + yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.resize (yyvs4, yyvsc4)
		end
	end
	yyvs4.put (yyval4, yyvsp4)
end
when 102 then
--|#line 400 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 400")
end

create {QFIS_REGEXP_CONSTRAINT} yyval4.make(yyvs6.item (yyvsp6), yyvs13.item (yyvsp13), True) ; yyval4.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 5
	yyvsp4 := yyvsp4 + 1
	yyvsp6 := yyvsp6 -1
	yyvsp2 := yyvsp2 -3
	yyvsp13 := yyvsp13 -1
	if yyvsp4 >= yyvsc4 then
		if yyvs4 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs4")
			end
			create yyspecial_routines4
			yyvsc4 := yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.make (yyvsc4)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs4")
			end
			yyvsc4 := yyvsc4 + yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.resize (yyvs4, yyvsc4)
		end
	end
	yyvs4.put (yyval4, yyvsp4)
end
when 103 then
--|#line 402 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 402")
end

create {QFIS_REGEXP_CONSTRAINT} yyval4.make(yyvs6.item (yyvsp6), yyvs13.item (yyvsp13), False) ; yyval4.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 6
	yyvsp4 := yyvsp4 + 1
	yyvsp6 := yyvsp6 -1
	yyvsp2 := yyvsp2 -4
	yyvsp13 := yyvsp13 -1
	if yyvsp4 >= yyvsc4 then
		if yyvs4 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs4")
			end
			create yyspecial_routines4
			yyvsc4 := yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.make (yyvsc4)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs4")
			end
			yyvsc4 := yyvsc4 + yyInitial_yyvs_size
			yyvs4 := yyspecial_routines4.resize (yyvs4, yyvsc4)
		end
	end
	yyvs4.put (yyval4, yyvsp4)
end
when 104 then
--|#line 406 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 406")
end

create {QFIS_SEQUENCE_EQ} yyval8.make (Void, Void) ; yyval8.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp8 := yyvsp8 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp8 >= yyvsc8 then
		if yyvs8 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs8")
			end
			create yyspecial_routines8
			yyvsc8 := yyInitial_yyvs_size
			yyvs8 := yyspecial_routines8.make (yyvsc8)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs8")
			end
			yyvsc8 := yyvsc8 + yyInitial_yyvs_size
			yyvs8 := yyspecial_routines8.resize (yyvs8, yyvsc8)
		end
	end
	yyvs8.put (yyval8, yyvsp8)
end
when 105 then
--|#line 408 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 408")
end

create {QFIS_SEQUENCE_NEQ} yyval8.make (Void, Void) ; yyval8.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp8 := yyvsp8 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp8 >= yyvsc8 then
		if yyvs8 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs8")
			end
			create yyspecial_routines8
			yyvsc8 := yyInitial_yyvs_size
			yyvs8 := yyspecial_routines8.make (yyvsc8)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs8")
			end
			yyvsc8 := yyvsc8 + yyInitial_yyvs_size
			yyvs8 := yyspecial_routines8.resize (yyvs8, yyvsc8)
		end
	end
	yyvs8.put (yyval8, yyvsp8)
end
when 106 then
--|#line 412 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 412")
end

create {QFIS_ARITHMETIC_EQ} yyval9.make (Void, Void) ; yyval9.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp9 := yyvsp9 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp9 >= yyvsc9 then
		if yyvs9 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs9")
			end
			create yyspecial_routines9
			yyvsc9 := yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.make (yyvsc9)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs9")
			end
			yyvsc9 := yyvsc9 + yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.resize (yyvs9, yyvsc9)
		end
	end
	yyvs9.put (yyval9, yyvsp9)
end
when 107 then
--|#line 414 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 414")
end

create {QFIS_ARITHMETIC_NEQ} yyval9.make (Void, Void) ; yyval9.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp9 := yyvsp9 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp9 >= yyvsc9 then
		if yyvs9 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs9")
			end
			create yyspecial_routines9
			yyvsc9 := yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.make (yyvsc9)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs9")
			end
			yyvsc9 := yyvsc9 + yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.resize (yyvs9, yyvsc9)
		end
	end
	yyvs9.put (yyval9, yyvsp9)
end
when 108 then
--|#line 416 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 416")
end

create {QFIS_ARITHMETIC_LT} yyval9.make (Void, Void) ; yyval9.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp9 := yyvsp9 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp9 >= yyvsc9 then
		if yyvs9 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs9")
			end
			create yyspecial_routines9
			yyvsc9 := yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.make (yyvsc9)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs9")
			end
			yyvsc9 := yyvsc9 + yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.resize (yyvs9, yyvsc9)
		end
	end
	yyvs9.put (yyval9, yyvsp9)
end
when 109 then
--|#line 418 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 418")
end

create {QFIS_ARITHMETIC_LTE} yyval9.make (Void, Void) ; yyval9.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp9 := yyvsp9 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp9 >= yyvsc9 then
		if yyvs9 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs9")
			end
			create yyspecial_routines9
			yyvsc9 := yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.make (yyvsc9)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs9")
			end
			yyvsc9 := yyvsc9 + yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.resize (yyvs9, yyvsc9)
		end
	end
	yyvs9.put (yyval9, yyvsp9)
end
when 110 then
--|#line 420 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 420")
end

create {QFIS_ARITHMETIC_GT} yyval9.make (Void, Void) ; yyval9.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp9 := yyvsp9 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp9 >= yyvsc9 then
		if yyvs9 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs9")
			end
			create yyspecial_routines9
			yyvsc9 := yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.make (yyvsc9)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs9")
			end
			yyvsc9 := yyvsc9 + yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.resize (yyvs9, yyvsc9)
		end
	end
	yyvs9.put (yyval9, yyvsp9)
end
when 111 then
--|#line 422 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 422")
end

create {QFIS_ARITHMETIC_GTE} yyval9.make (Void, Void) ; yyval9.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp9 := yyvsp9 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp9 >= yyvsc9 then
		if yyvs9 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs9")
			end
			create yyspecial_routines9
			yyvsc9 := yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.make (yyvsc9)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs9")
			end
			yyvsc9 := yyvsc9 + yyInitial_yyvs_size
			yyvs9 := yyspecial_routines9.resize (yyvs9, yyvsc9)
		end
	end
	yyvs9.put (yyval9, yyvsp9)
end
when 112 then
--|#line 426 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 426")
end

create {QFIS_VARIABLE} yyval6.make (yyvs2.item (yyvsp2)) ; yyval6.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp6 := yyvsp6 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp6 >= yyvsc6 then
		if yyvs6 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs6")
			end
			create yyspecial_routines6
			yyvsc6 := yyInitial_yyvs_size
			yyvs6 := yyspecial_routines6.make (yyvsc6)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs6")
			end
			yyvsc6 := yyvsc6 + yyInitial_yyvs_size
			yyvs6 := yyspecial_routines6.resize (yyvs6, yyvsc6)
		end
	end
	yyvs6.put (yyval6, yyvsp6)
end
when 113 then
--|#line 427 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 427")
end

create {QFIS_VARIABLE} yyval6.make (yyvs2.item (yyvsp2)) ; yyval6.set_old ; yyval6.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp6 := yyvsp6 + 1
	yyvsp2 := yyvsp2 -2
	if yyvsp6 >= yyvsc6 then
		if yyvs6 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs6")
			end
			create yyspecial_routines6
			yyvsc6 := yyInitial_yyvs_size
			yyvs6 := yyspecial_routines6.make (yyvsc6)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs6")
			end
			yyvsc6 := yyvsc6 + yyInitial_yyvs_size
			yyvs6 := yyspecial_routines6.resize (yyvs6, yyvsc6)
		end
	end
	yyvs6.put (yyval6, yyvsp6)
end
when 114 then
--|#line 429 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 429")
end

create {QFIS_SEQUENCE_EMPTY} yyval6.make ; yyval6.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp6 := yyvsp6 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp6 >= yyvsc6 then
		if yyvs6 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs6")
			end
			create yyspecial_routines6
			yyvsc6 := yyInitial_yyvs_size
			yyvs6 := yyspecial_routines6.make (yyvsc6)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs6")
			end
			yyvsc6 := yyvsc6 + yyInitial_yyvs_size
			yyvs6 := yyspecial_routines6.resize (yyvs6, yyvsc6)
		end
	end
	yyvs6.put (yyval6, yyvsp6)
end
when 115 then
--|#line 430 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 430")
end

create {QFIS_SEQUENCE_SINGLETON} yyval6.make (yyvs11.item (yyvsp11)) ; yyval6.line_number := yyvs11.item (yyvsp11).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp6 := yyvsp6 + 1
	yyvsp11 := yyvsp11 -1
	if yyvsp6 >= yyvsc6 then
		if yyvs6 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs6")
			end
			create yyspecial_routines6
			yyvsc6 := yyInitial_yyvs_size
			yyvs6 := yyspecial_routines6.make (yyvsc6)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs6")
			end
			yyvsc6 := yyvsc6 + yyInitial_yyvs_size
			yyvs6 := yyspecial_routines6.resize (yyvs6, yyvsc6)
		end
	end
	yyvs6.put (yyval6, yyvsp6)
end
when 116 then
--|#line 432 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 432")
end

create {QFIS_FUNCTION} yyval6.make (yyvs2.item (yyvsp2 - 2), yyvs7.item (yyvsp7)) ; yyval6.line_number := yyvs7.item (yyvsp7).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 4
	yyvsp6 := yyvsp6 + 1
	yyvsp2 := yyvsp2 -3
	yyvsp7 := yyvsp7 -1
	if yyvsp6 >= yyvsc6 then
		if yyvs6 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs6")
			end
			create yyspecial_routines6
			yyvsc6 := yyInitial_yyvs_size
			yyvs6 := yyspecial_routines6.make (yyvsc6)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs6")
			end
			yyvsc6 := yyvsc6 + yyInitial_yyvs_size
			yyvs6 := yyspecial_routines6.resize (yyvs6, yyvsc6)
		end
	end
	yyvs6.put (yyval6, yyvsp6)
end
when 117 then
--|#line 434 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 434")
end

yyval6 := yyvs6.item (yyvsp6) ; yyval6.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -2
	yyvs6.put (yyval6, yyvsp6)
end
when 118 then
--|#line 436 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 436")
end

create {QFIS_RANGE} yyval6.make (yyvs6.item (yyvsp6), yyvs12.item (yyvsp12 - 1), yyvs12.item (yyvsp12)) ; yyval6.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 6
	yyvsp2 := yyvsp2 -3
	yyvsp12 := yyvsp12 -2
	yyvs6.put (yyval6, yyvsp6)
end
when 119 then
--|#line 438 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 438")
end

create {QFIS_RANGE} yyval6.make (yyvs6.item (yyvsp6), yyvs12.item (yyvsp12), yyvs12.item (yyvsp12)) ; yyval6.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -1
	yyvsp12 := yyvsp12 -1
	yyvs6.put (yyval6, yyvsp6)
end
when 120 then
--|#line 440 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 440")
end

create {QFIS_CAT_COMPOUND} yyval6.make (yyvs6.item (yyvsp6 - 1), yyvs6.item (yyvsp6)) ; yyval6.line_number := yyvs6.item (yyvsp6 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp6 := yyvsp6 -1
	yyvsp2 := yyvsp2 -1
	yyvs6.put (yyval6, yyvsp6)
end
when 121 then
--|#line 444 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 444")
end

yyval2 := yyvs2.item (yyvsp2) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs2.put (yyval2, yyvsp2)
end
when 122 then
--|#line 445 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 445")
end

yyval2 := yyvs2.item (yyvsp2) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs2.put (yyval2, yyvsp2)
end
when 123 then
--|#line 446 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 446")
end

yyval2 := yyvs2.item (yyvsp2) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvs2.put (yyval2, yyvsp2)
end
when 124 then
--|#line 449 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 449")
end

yyval12 := yyvs2.item (yyvsp2).to_integer 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp12 := yyvsp12 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp12 >= yyvsc12 then
		if yyvs12 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs12")
			end
			create yyspecial_routines12
			yyvsc12 := yyInitial_yyvs_size
			yyvs12 := yyspecial_routines12.make (yyvsc12)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs12")
			end
			yyvsc12 := yyvsc12 + yyInitial_yyvs_size
			yyvs12 := yyspecial_routines12.resize (yyvs12, yyvsc12)
		end
	end
	yyvs12.put (yyval12, yyvsp12)
end
when 125 then
--|#line 450 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 450")
end

yyval12 := yyvs2.item (yyvsp2).to_integer ; yyval12 := - yyval12 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp12 := yyvsp12 + 1
	yyvsp2 := yyvsp2 -2
	if yyvsp12 >= yyvsc12 then
		if yyvs12 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs12")
			end
			create yyspecial_routines12
			yyvsc12 := yyInitial_yyvs_size
			yyvs12 := yyspecial_routines12.make (yyvsc12)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs12")
			end
			yyvsc12 := yyvsc12 + yyInitial_yyvs_size
			yyvs12 := yyspecial_routines12.resize (yyvs12, yyvsc12)
		end
	end
	yyvs12.put (yyval12, yyvsp12)
end
when 126 then
--|#line 453 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 453")
end

create {QFIS_RE_EPSILON} yyval13 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp13 := yyvsp13 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp13 >= yyvsc13 then
		if yyvs13 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs13")
			end
			create yyspecial_routines13
			yyvsc13 := yyInitial_yyvs_size
			yyvs13 := yyspecial_routines13.make (yyvsc13)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs13")
			end
			yyvsc13 := yyvsc13 + yyInitial_yyvs_size
			yyvs13 := yyspecial_routines13.resize (yyvs13, yyvsc13)
		end
	end
	yyvs13.put (yyval13, yyvsp13)
end
when 127 then
--|#line 455 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 455")
end

create {QFIS_RE_INTEGER} yyval13.make (yyvs12.item (yyvsp12)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp13 := yyvsp13 + 1
	yyvsp12 := yyvsp12 -1
	if yyvsp13 >= yyvsc13 then
		if yyvs13 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs13")
			end
			create yyspecial_routines13
			yyvsc13 := yyInitial_yyvs_size
			yyvs13 := yyspecial_routines13.make (yyvsc13)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs13")
			end
			yyvsc13 := yyvsc13 + yyInitial_yyvs_size
			yyvs13 := yyspecial_routines13.resize (yyvs13, yyvsc13)
		end
	end
	yyvs13.put (yyval13, yyvsp13)
end
when 128 then
--|#line 457 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 457")
end

create {QFIS_RE_UNIVERSE} yyval13 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp13 := yyvsp13 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp13 >= yyvsc13 then
		if yyvs13 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs13")
			end
			create yyspecial_routines13
			yyvsc13 := yyInitial_yyvs_size
			yyvs13 := yyspecial_routines13.make (yyvsc13)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs13")
			end
			yyvsc13 := yyvsc13 + yyInitial_yyvs_size
			yyvs13 := yyspecial_routines13.resize (yyvs13, yyvsc13)
		end
	end
	yyvs13.put (yyval13, yyvsp13)
end
when 129 then
--|#line 459 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 459")
end

create {QFIS_RE_NOT} yyval13.make (yyvs13.item (yyvsp13)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp2 := yyvsp2 -1
	yyvs13.put (yyval13, yyvsp13)
end
when 130 then
--|#line 461 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 461")
end

yyval13 := yyvs13.item (yyvsp13) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp2 := yyvsp2 -2
	yyvs13.put (yyval13, yyvsp13)
end
when 131 then
--|#line 463 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 463")
end

create {QFIS_RE_CAT} yyval13.make (yyvs13.item (yyvsp13 - 1), yyvs13.item (yyvsp13)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp13 := yyvsp13 -1
	yyvsp2 := yyvsp2 -1
	yyvs13.put (yyval13, yyvsp13)
end
when 132 then
--|#line 465 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 465")
end

create {QFIS_RE_STAR} yyval13.make (yyvs13.item (yyvsp13)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp2 := yyvsp2 -1
	yyvs13.put (yyval13, yyvsp13)
end
when 133 then
--|#line 467 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 467")
end

create {QFIS_RE_PLUS} yyval13.make (yyvs13.item (yyvsp13)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp2 := yyvsp2 -1
	yyvs13.put (yyval13, yyvsp13)
end
when 134 then
--|#line 469 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 469")
end

create {QFIS_RE_AND} yyval13.make (yyvs13.item (yyvsp13 - 1), yyvs13.item (yyvsp13)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp13 := yyvsp13 -1
	yyvsp2 := yyvsp2 -1
	yyvs13.put (yyval13, yyvsp13)
end
when 135 then
--|#line 471 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 471")
end

create {QFIS_RE_OR} yyval13.make (yyvs13.item (yyvsp13 - 1), yyvs13.item (yyvsp13)) 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp13 := yyvsp13 -1
	yyvsp2 := yyvsp2 -1
	yyvs13.put (yyval13, yyvsp13)
end
when 136 then
--|#line 475 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 475")
end

create yyval7.make_empty ; yyval7.extend (yyvs6.item (yyvsp6)) ; yyval7.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp7 := yyvsp7 + 1
	yyvsp6 := yyvsp6 -1
	if yyvsp7 >= yyvsc7 then
		if yyvs7 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs7")
			end
			create yyspecial_routines7
			yyvsc7 := yyInitial_yyvs_size
			yyvs7 := yyspecial_routines7.make (yyvsc7)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs7")
			end
			yyvsc7 := yyvsc7 + yyInitial_yyvs_size
			yyvs7 := yyspecial_routines7.resize (yyvs7, yyvsc7)
		end
	end
	yyvs7.put (yyval7, yyvsp7)
end
when 137 then
--|#line 477 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 477")
end

create yyval7.make_empty ; yyval7.extend (yyvs3.item (yyvsp3)) ; yyval7.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp7 := yyvsp7 + 1
	yyvsp3 := yyvsp3 -1
	if yyvsp7 >= yyvsc7 then
		if yyvs7 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs7")
			end
			create yyspecial_routines7
			yyvsc7 := yyInitial_yyvs_size
			yyvs7 := yyspecial_routines7.make (yyvsc7)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs7")
			end
			yyvsc7 := yyvsc7 + yyInitial_yyvs_size
			yyvs7 := yyspecial_routines7.resize (yyvs7, yyvsc7)
		end
	end
	yyvs7.put (yyval7, yyvsp7)
end
when 138 then
--|#line 479 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 479")
end

yyvs7.item (yyvsp7).put_front (yyvs6.item (yyvsp6)) ; yyval7 := yyvs7.item (yyvsp7) ; yyval7.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp6 := yyvsp6 -1
	yyvsp2 := yyvsp2 -1
	yyvs7.put (yyval7, yyvsp7)
end
when 139 then
--|#line 481 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 481")
end

yyvs7.item (yyvsp7).put_front (yyvs3.item (yyvsp3)) ; yyval7 := yyvs7.item (yyvsp7) ; yyval7.line_number := yyvs3.item (yyvsp3).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp3 := yyvsp3 -1
	yyvsp2 := yyvsp2 -1
	yyvs7.put (yyval7, yyvsp7)
end
when 140 then
--|#line 485 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 485")
end

create {QFIS_CONSTANT} yyval11.make (yyvs12.item (yyvsp12)) ; yyval11.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp11 := yyvsp11 + 1
	yyvsp12 := yyvsp12 -1
	if yyvsp11 >= yyvsc11 then
		if yyvs11 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs11")
			end
			create yyspecial_routines11
			yyvsc11 := yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.make (yyvsc11)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs11")
			end
			yyvsc11 := yyvsc11 + yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.resize (yyvs11, yyvsc11)
		end
	end
	yyvs11.put (yyval11, yyvsp11)
end
when 141 then
--|#line 487 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 487")
end

create {QFIS_INTEGER_VARIABLE} yyval11.make (yyvs2.item (yyvsp2)) ; yyval11.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp11 := yyvsp11 + 1
	yyvsp2 := yyvsp2 -1
	if yyvsp11 >= yyvsc11 then
		if yyvs11 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs11")
			end
			create yyspecial_routines11
			yyvsc11 := yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.make (yyvsc11)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs11")
			end
			yyvsc11 := yyvsc11 + yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.resize (yyvs11, yyvsc11)
		end
	end
	yyvs11.put (yyval11, yyvsp11)
end
when 142 then
--|#line 489 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 489")
end

create {QFIS_INTEGER_VARIABLE} yyval11.make (yyvs2.item (yyvsp2)) ; yyval11.set_old ; yyval11.line_number := line_nb 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 2
	yyvsp11 := yyvsp11 + 1
	yyvsp2 := yyvsp2 -2
	if yyvsp11 >= yyvsc11 then
		if yyvs11 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs11")
			end
			create yyspecial_routines11
			yyvsc11 := yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.make (yyvsc11)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs11")
			end
			yyvsc11 := yyvsc11 + yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.resize (yyvs11, yyvsc11)
		end
	end
	yyvs11.put (yyval11, yyvsp11)
end
when 143 then
--|#line 491 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 491")
end

create {QFIS_INTEGER_FUNCTION} yyval11.make (yyvs2.item (yyvsp2 - 2), yyvs7.item (yyvsp7)) ; yyval11.line_number := yyvs7.item (yyvsp7).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 4
	yyvsp11 := yyvsp11 + 1
	yyvsp2 := yyvsp2 -3
	yyvsp7 := yyvsp7 -1
	if yyvsp11 >= yyvsc11 then
		if yyvs11 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs11")
			end
			create yyspecial_routines11
			yyvsc11 := yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.make (yyvsc11)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs11")
			end
			yyvsc11 := yyvsc11 + yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.resize (yyvs11, yyvsc11)
		end
	end
	yyvs11.put (yyval11, yyvsp11)
end
when 144 then
--|#line 493 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 493")
end

create {QFIS_POSITION_INTEGER_FUNCTION} yyval11.make (yyvs6.item (yyvsp6), yyvs12.item (yyvsp12)) ; yyval11.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 6
	yyvsp11 := yyvsp11 + 1
	yyvsp2 := yyvsp2 -4
	yyvsp6 := yyvsp6 -1
	yyvsp12 := yyvsp12 -1
	if yyvsp11 >= yyvsc11 then
		if yyvs11 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs11")
			end
			create yyspecial_routines11
			yyvsc11 := yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.make (yyvsc11)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs11")
			end
			yyvsc11 := yyvsc11 + yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.resize (yyvs11, yyvsc11)
		end
	end
	yyvs11.put (yyval11, yyvsp11)
end
when 145 then
--|#line 495 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 495")
end

create {QFIS_LENGTH} yyval11.make (yyvs6.item (yyvsp6)) ; yyval11.line_number := yyvs6.item (yyvsp6).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp11 := yyvsp11 + 1
	yyvsp2 := yyvsp2 -2
	yyvsp6 := yyvsp6 -1
	if yyvsp11 >= yyvsc11 then
		if yyvs11 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs11")
			end
			create yyspecial_routines11
			yyvsc11 := yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.make (yyvsc11)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs11")
			end
			yyvsc11 := yyvsc11 + yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.resize (yyvs11, yyvsc11)
		end
	end
	yyvs11.put (yyval11, yyvsp11)
end
when 146 then
--|#line 497 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 497")
end

yyval11 := yyvs10.item (yyvsp10) ; yyval11.line_number := yyvs10.item (yyvsp10).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 1
	yyvsp11 := yyvsp11 + 1
	yyvsp10 := yyvsp10 -1
	if yyvsp11 >= yyvsc11 then
		if yyvs11 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs11")
			end
			create yyspecial_routines11
			yyvsc11 := yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.make (yyvsc11)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs11")
			end
			yyvsc11 := yyvsc11 + yyInitial_yyvs_size
			yyvs11 := yyspecial_routines11.resize (yyvs11, yyvsc11)
		end
	end
	yyvs11.put (yyval11, yyvsp11)
end
when 147 then
--|#line 502 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 502")
end

create {QFIS_TIMES_COMPOUND} yyval10.make (yyvs11.item (yyvsp11 - 1), yyvs11.item (yyvsp11)) ; yyval10.line_number := yyvs11.item (yyvsp11 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp10 := yyvsp10 + 1
	yyvsp11 := yyvsp11 -2
	yyvsp2 := yyvsp2 -1
	if yyvsp10 >= yyvsc10 then
		if yyvs10 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs10")
			end
			create yyspecial_routines10
			yyvsc10 := yyInitial_yyvs_size
			yyvs10 := yyspecial_routines10.make (yyvsc10)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs10")
			end
			yyvsc10 := yyvsc10 + yyInitial_yyvs_size
			yyvs10 := yyspecial_routines10.resize (yyvs10, yyvsc10)
		end
	end
	yyvs10.put (yyval10, yyvsp10)
end
when 148 then
--|#line 504 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 504")
end

create {QFIS_DIVIDE_COMPOUND} yyval10.make (yyvs11.item (yyvsp11 - 1), yyvs11.item (yyvsp11)) ; yyval10.line_number := yyvs11.item (yyvsp11 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp10 := yyvsp10 + 1
	yyvsp11 := yyvsp11 -2
	yyvsp2 := yyvsp2 -1
	if yyvsp10 >= yyvsc10 then
		if yyvs10 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs10")
			end
			create yyspecial_routines10
			yyvsc10 := yyInitial_yyvs_size
			yyvs10 := yyspecial_routines10.make (yyvsc10)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs10")
			end
			yyvsc10 := yyvsc10 + yyInitial_yyvs_size
			yyvs10 := yyspecial_routines10.resize (yyvs10, yyvsc10)
		end
	end
	yyvs10.put (yyval10, yyvsp10)
end
when 149 then
--|#line 506 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 506")
end

create {QFIS_PLUS_COMPOUND} yyval10.make (yyvs11.item (yyvsp11 - 1), yyvs11.item (yyvsp11)) ; yyval10.line_number := yyvs11.item (yyvsp11 - 1).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp10 := yyvsp10 + 1
	yyvsp11 := yyvsp11 -2
	yyvsp2 := yyvsp2 -1
	if yyvsp10 >= yyvsc10 then
		if yyvs10 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs10")
			end
			create yyspecial_routines10
			yyvsc10 := yyInitial_yyvs_size
			yyvs10 := yyspecial_routines10.make (yyvsc10)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs10")
			end
			yyvsc10 := yyvsc10 + yyInitial_yyvs_size
			yyvs10 := yyspecial_routines10.resize (yyvs10, yyvsc10)
		end
	end
	yyvs10.put (yyval10, yyvsp10)
end
when 150 then
--|#line 508 "qfisPL.y"
debug ("GEYACC")
	std.error.put_line ("Executing parser user-code from file 'qfisPL.y' at line 508")
end

create {QFIS_MINUS_COMPOUND} yyval10.make (yyvs11.item (yyvsp11 - 1), yyvs11.item (yyvsp11)) ; yyval10.line_number := yyvs11.item (yyvsp11).line_number 
if yy_parsing_status >= yyContinue then
	yyssp := yyssp - 3
	yyvsp10 := yyvsp10 + 1
	yyvsp11 := yyvsp11 -2
	yyvsp2 := yyvsp2 -1
	if yyvsp10 >= yyvsc10 then
		if yyvs10 = Void then
			debug ("GEYACC")
				std.error.put_line ("Create yyvs10")
			end
			create yyspecial_routines10
			yyvsc10 := yyInitial_yyvs_size
			yyvs10 := yyspecial_routines10.make (yyvsc10)
		else
			debug ("GEYACC")
				std.error.put_line ("Resize yyvs10")
			end
			yyvsc10 := yyvsc10 + yyInitial_yyvs_size
			yyvs10 := yyspecial_routines10.resize (yyvs10, yyvsc10)
		end
	end
	yyvs10.put (yyval10, yyvsp10)
end
			else
				debug ("GEYACC")
					std.error.put_string ("Error in parser: unknown rule id: ")
					std.error.put_integer (yy_act)
					std.error.put_new_line
				end
				abort
			end
		end

	yy_do_error_action (yy_act: INTEGER) is
			-- Execute error action.
		do
			inspect yy_act
			when 283 then
					-- End-of-file expected action.
				report_eof_expected_error
			else
					-- Default action.
				report_error ("parse error")
			end
		end

feature {NONE} -- Table templates

	yytranslate_template: SPECIAL [INTEGER] is
			-- Template for `yytranslate'
		once
			Result := yyfixed_array (<<
			    0,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,

			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,

			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    1,    2,    3,    4,
			    5,    6,    7,    8,    9,   10,   11,   12,   13,   14,
			   15,   16,   17,   18,   19,   20,   21,   22,   23,   24,
			   25,   26,   27,   28,   29,   30,   31,   32,   33,   34,
			   35,   36,   37,   38,   39,   40,   41,   42,   43,   44,

			   45,   46,   47,   48,   49,   50,   51,   52,   53,   54,
			   55,   56,   57,   58,   59,   60,   61,   62,   63,   64,
			   65,   66,   67,   68,   69,   70,   71,   72,   73,   74,
			   75,   76,   77, yyDummy>>)
		end

	yyr1_template: SPECIAL [INTEGER] is
			-- Template for `yyr1'
		once
			Result := yyfixed_array (<<
			    0,  117,  117,   89,   89,   89,   89,   89,   89,   89,
			   89,  116,  116,  115,  114,  114,  114,  114,   90,   91,
			   91,   91,   92,   92,   92,   94,   94,   94,   93,   93,
			   93,   95,   95,   95,   95,  113,  113,  113,   96,   96,
			   97,   97,   99,   99,   99,  100,  100,  101,  101,  102,
			  102,  102,  103,  103,  103,  103,  103,  103,  103,  103,
			  103,  103,  103,  104,  104,  105,  105,  106,  106,  107,
			  107,  108,  109,  109,  109,  110,  110,  111,  111,  111,
			  111,  111,  112,  112,  112,  112,   78,   78,   78,   78,
			   78,   78,   78,   78,   80,   80,   79,   79,   79,   79,

			   79,   79,   79,   79,   83,   83,   84,   84,   84,   84,
			   84,   84,   81,   81,   81,   81,   81,   81,   81,   81,
			   81,   98,   98,   98,   87,   87,   88,   88,   88,   88,
			   88,   88,   88,   88,   88,   88,   82,   82,   82,   82,
			   86,   86,   86,   86,   86,   86,   86,   85,   85,   85,
			   85, yyDummy>>)
		end

	yytypes1_template: SPECIAL [INTEGER] is
			-- Template for `yytypes1'
		once
			Result := yyfixed_array (<<
			    1,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    3,    4,    6,
			   10,   11,   12,   14,   15,   20,    2,    2,    2,    2,
			    2,    3,    5,   31,    3,    2,    2,    2,    2,   30,
			    2,    2,    2,    2,   21,   18,    2,    2,    2,    2,
			    3,    6,    3,    2,    2,    2,    6,   11,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    8,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    9,   14,    2,   16,    2,    3,    6,    7,    7,    2,
			    2,   14,    2,    2,   14,    2,    2,    2,   14,    2,

			    2,    2,    2,    6,    2,    3,    3,    3,    3,   12,
			   12,    2,    2,    6,    6,    2,    2,   11,   11,   11,
			   11,   11,    2,    3,   28,    2,   18,    7,    2,    2,
			    2,    2,   21,   21,    2,    2,    2,    2,   18,    2,
			   21,    2,   18,    2,   18,    2,    2,    2,    2,    2,
			    2,   12,   13,    2,    6,    3,    2,    2,   18,    2,
			   19,    2,    7,    7,    2,    2,    2,    2,    2,    2,
			   21,    2,   12,   12,   13,   13,    2,    2,    2,    2,
			    2,    2,   13,    2,    3,    2,    3,   21,    2,   22,
			    2,    2,   18,    2,    2,    2,    2,    2,    2,    2,

			    2,   13,   13,   13,    2,    3,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,   24,   23,   25,
			   26,   29,    2,   17,    3,    3,    2,    2,    2,    2,
			    2,    2,    2,    2,   18,    2,    2,    3,    3,    3,
			    6,   18,    3,    3,    2,   24,   28,    2,    2,    2,
			    2,   21,    3,   11,    6,    2,    2,    2,   24,    2,
			   27,    2,   24,    2,    2,    2,   18,   24,    2,    2,
			   24,   28,    2,   24,   18,    2,    2,   24,    2,   24,
			    2,   18,    2,    1,    1,    1, yyDummy>>)
		end

	yytypes2_template: SPECIAL [INTEGER] is
			-- Template for `yytypes2'
		once
			Result := yyfixed_array (<<
			    1,    1,    1,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
			    2,    2,    2,    2,    2,    2,    2,    1, yyDummy>>)
		end

	yydefact_template: SPECIAL [INTEGER] is
			-- Template for `yydefact'
		once
			Result := yyfixed_array (<<
			    0,  124,  122,  141,  123,   97,   96,    0,    0,    0,
			    0,    0,  114,    0,    0,    0,    0,    2,   86,    0,
			  146,  115,  140,    1,    3,   19,    0,    0,    0,    0,
			    0,   11,   12,    6,   13,   35,   37,   36,    0,    5,
			   31,  122,  121,  123,    4,    0,   40,   99,  142,  113,
			    0,    0,   87,  125,    0,    0,    0,  115,    0,    0,
			    0,    0,    0,    0,    0,    0,  105,  104,    0,    0,
			  111,  110,  109,  108,  106,  107,    0,    0,    0,    0,
			    0,    7,   20,   28,    0,  137,  136,    0,    0,    0,
			    0,   10,    0,    0,    9,    0,    0,    0,    8,    0,

			    0,   89,  117,    0,  145,   93,   92,   91,   90,  119,
			    0,    0,    0,  120,  100,    0,    0,  148,  147,  150,
			  149,  101,    0,   77,   21,   29,   25,    0,    0,    0,
			   88,  116,    0,    0,   44,   43,   42,   45,    0,   14,
			    0,    0,    0,   38,   41,    0,    0,  128,  126,    0,
			    0,  127,    0,    0,    0,   78,    0,    0,   30,   26,
			    0,  143,  139,  138,    0,    0,    0,    0,    0,   32,
			    0,    0,    0,    0,    0,  129,    0,  102,    0,    0,
			  132,  133,    0,    0,   81,    0,   79,   27,   47,   22,
			    0,    0,   46,    0,   15,    0,   33,   39,  144,  118,

			  130,  131,  135,  134,  103,   80,    0,    0,    0,    0,
			    0,    0,    0,   52,    0,    0,    0,   48,   49,   60,
			   61,   62,   23,    0,    0,    0,   16,    0,    0,    0,
			    0,  122,  121,  123,    0,   82,   69,   70,    0,   72,
			    0,   55,   54,   53,    0,   50,   24,   18,   95,   94,
			    0,    0,   58,   59,   57,    0,    0,   65,    0,   73,
			    0,    0,   51,   17,   34,   84,    0,   66,   67,   63,
			    0,   74,   75,    0,   56,    0,   83,   68,   64,   76,
			   71,    0,   85,    0,    0,    0, yyDummy>>)
		end

	yydefgoto_template: SPECIAL [INTEGER] is
			-- Template for `yydefgoto'
		once
			Result := yyfixed_array (<<
			   85,   18,   32,   19,   87,   69,   80,   20,   21,   22,
			  152,   23,   24,   83,  223,  126,  160,   25,   44,   45,
			   26,  137,  138,  189,  217,  218,  219,  258,  270,  238,
			  220,  260,  273,  124,  221,   38,   39,   33,   34,  283, yyDummy>>)
		end

	yypact_template: SPECIAL [INTEGER] is
			-- Template for `yypact'
		once
			Result := yyfixed_array (<<
			  350, -32768,  281,  325,  218, -32768, -32768,  136,  125,  125,
			   68,  331, -32768,  446,  446,  282,  113,  477, -32768,  585,
			 -32768,  666, -32768, -32768,   -7,  303,  309,  446,  446,  314,
			  307,  477, -32768,   -7, -32768, -32768, -32768, -32768,   65,   -7,
			   24, -32768, -32768, -32768,  114,  305,  291, -32768, -32768, -32768,
			  437,  577, -32768, -32768,   27,  113,    6,  427,  446,  446,
			  446,  446,   18,   18,  297,  298, -32768, -32768,  113,  113,
			 -32768, -32768, -32768, -32768, -32768, -32768,  288,  288,  288,  288,
			  288, -32768,  358,  258,  446,  391,  569,  279,  278,   68,
			   68, -32768,  269,  269, -32768,   68,  276,   68, -32768,  269,

			   68, -32768,  177,   70, -32768,  359,  359,  285, -32768, -32768,
			  274,    7,  270, -32768,   89,  216,  113, -32768, -32768,   -3,
			   -3,  427,  446,  477,  -29,   68,  247,  245,  446,  446,
			 -32768, -32768,   92,   69, -32768, -32768, -32768,  229,  231,  230,
			   60,   68,  227, -32768, -32768,   18,   18, -32768, -32768,    7,
			    7, -32768,  605,    7,   20,  477,  446,  434, -32768,   68,
			  199, -32768, -32768, -32768,  214,  213,  269,  211,  157,  208,
			   17,  269,  202,  188,  286,  139,    7, -32768,    7,    7,
			 -32768, -32768,  158,  177,  477,  446,  477,  138,  599,  131,
			  136,  136, -32768,  269, -32768,  163, -32768, -32768, -32768, -32768,

			 -32768,  127,  367,  139, -32768,  477,  171,  162,  146,  184,
			  375,  375,  113, -32768,   68,  446,  446, -32768,  575, -32768,
			 -32768, -32768,  358,  149,  159,  140,  148,   68,  446,  288,
			  113,  506,  480,  429,  109,  124, -32768,  477,  101,   88,
			   94, -32768,  477,  477,  599, -32768,  -29, -32768, -32768, -32768,
			   51,  -21,  477,  427,   89,  125,   68,  599,  -12,  358,
			   93,   68, -32768, -32768, -32768,   19,   41, -32768,  599, -32768,
			   77,  -29,  599,    9, -32768,   68, -32768, -32768, -32768, -32768,
			 -32768,   15, -32768,   36,   29, -32768, yyDummy>>)
		end

	yypgoto_template: SPECIAL [INTEGER] is
			-- Template for `yypgoto'
		once
			Result := yyfixed_array (<<
			    4, -32768, -32768,    0,  -26, -32768, -32768, -32768,   -2,  -41,
			  220,  362, -32768, -32768, -32768, -32768, -32768, -32768,  -70,  -92,
			  -10,  -61,  205, -32768, -161, -32768, -32768, -32768, -32768,  151,
			 -32768, -32768, -32768, -219, -32768,   -8, -32768, -32768,  -32, -32768, yyDummy>>)
		end

	yytable_template: SPECIAL [INTEGER] is
			-- Template for `yytable'
		once
			Result := yyfixed_array (<<
			   46,   40,   88,  246,   17,  142,   77,   76,  144,   68,
			  264,   31,  104,   51,   57,   15,   56,   50,   52,  132,
			  133,  109,  110,   68,  150,  140,   15,   86,   86,  285,
			   63,  269,  139,  158,   10,  157,  284,  149,  143,  148,
			  271,  147,   62,   97,   63,  156,  282,   96,  196,  275,
			  268,  183,  280,   57,   95,  103,   62,  245,  127,    9,
			    8,    7,  105,  106,  107,  108,   57,   57,  113,  114,
			  151,  170,  276,   68,  117,  118,  119,  120,  121,   46,
			   46,   97,    1,  262,   86,   46,  123,   46,   93,  187,
			   46,  169,   68,    1,   63,   92,  267,   68,   49,   48,

			  165,  102,  162,  163,  172,  173,   62,  277,  151,  151,
			  197,  279,  151,   63,   57,   46,  154,  234,   63,   16,
			  278,   15,  241,  164,   97,   62,  155,  263,   86,   86,
			   62,   46,  226,   97,  181,  151,  180,  151,  151,   43,
			   42,   41,   16,   55,   15,   12,  181,  259,  180,   46,
			  261,  272,   54,   14,  256,   10,   97,  251,  224,  225,
			  184,  186,  257,  255,  266,  181,   13,  180,   12,  274,
			  250,  249,  176,   30,   29,   11,  179,  178,   97,  222,
			    9,    8,    7,  281,    4,    3,   41,  204,    1,  205,
			  248,  176,  247,  227,   31,   31,   37,   36,   35,   46,

			  230,  235,   97,  145,   46,    6,    5,    4,    3,    2,
			   57,    1,  240,  199,  237,  237,  229,   46, -112,  242,
			  243, -112, -112, -112, -112,  228,  123,  253,   57,  198,
			  254,  195,  252,  194,  193, -112, -112, -112, -112, -112,
			 -112,  188, -112,  191,  190, -112,   46,  265,   28, -112,
			  171,   46,  168, -112, -112,  233,  232,  231, -112, -112,
			 -112, -112,  167,  123,  166,   46, -112, -112, -112, -112,
			 -112, -112,  161, -112, -112, -112, -112, -112, -112, -112,
			 -112,  -98, -112, -112, -112, -112, -112,  159,   48, -112,
			 -112, -112, -112,  181,   16,  180,   15,  146,  153,  -98,

			  -98,  -98,  -98,   61,  179,  178,  141,  125,  -98,  131,
			  130,   27,  -98,  136,  135,  134,  -98,  200,  116,  176,
			  112,  -98,  -98,  -98,  -98,  111,  100,  115,   99,  -98,
			  -98,  -98,  -98,  -98,  -98,   84,  -98,   90,  -98,  -98,
			  -98,  -98,  -98,  -98,   89,  -98,  -98,  -98,  -98,  -98,
			   82, -121,  -98,  -98,  -98,  -98,   16,   53,   15,   43,
			    3,   41,  239,    1,   16,    0,   15,   14,    0,  174,
			  175,  192,    0,  182,  181,   14,  180,   61,   60,   59,
			   13,   16,   12,   15,    0,  179,   81,    0,   13,   11,
			   12,   10,   14,    0,    0,   91,  201,   11,  202,  203,

			  176,   94,   49,   48,   47,   13,   98,   12,    0,   61,
			   60,   59,   58,    0,   11,    0,    9,    8,    7,    6,
			    5,    4,    3,    2,    0,    1,  128,    6,    5,    4,
			    3,    2,  122,    1,   79,   78,   77,   76,  236,    0,
			   16,    0,   15,    0,    6,    5,    4,    3,    2,    0,
			    1,   14,   16,    0,   15,   61,   60,   59,   58,  -36,
			    0,    0,    0,   14,   13,    0,   12,    0,  101,    0,
			    0,    0,  -36,   11,    0,    0,   13,  -36,   12,  -36,
			  -36,  -36,  -36,    0,  -36,   11,  -36,    0,    0,  -36,
			    0,  -36,    0,  -36,  -36,   61,   60,   59,   58,    0,

			  -36,  -36,  -36,    6,    5,    4,    3,    2,  185,    1,
			  -37,    0,    0,    0,    0,    6,    5,    4,    3,    2,
			    0,    1,    0,  -37,    0,    0,    0,    0,  -37,    0,
			  -37,  -37,  -37,  -37,    0,  -37,  -35,  -37,    0,    0,
			  -37,    0,  -37,    0,  -37,  -37,    0,    0,    0,  -35,
			    0,  -37,  -37,  -37,  -35,    0,  -35,  -35,  -35,  -35,
			    0,  -35,    0,  -35,    0,    0,  -35,    0,  -35,    0,
			  -35,  -35,   68,   67,   66,    0,    0,  -35,  -35,  -35,
			   68,   67,   66,    0,    0,    0,   65,    0,   68,   67,
			   66,   64,    0,   63,   65,    0,    0,    0,    0,   64,

			    0,   63,   65,    0,  129,   62,    0,   64,  102,   63,
			    0,    0,  181,   62,  180,    0,    0,    0,    0,    0,
			    0,   62,    0,  179,  178,  216,  215,  214,  213,    0,
			  212,    0,  211,    0,  177,  210,    0,    0,  176,  244,
			  209,    0,    0,    0,    0,    0,  208,  207,  206,  216,
			  215,  214,  213,    0,  212,    0,  211,    0,    0,  210,
			    0,    0,    0,    0,  209,    0,    0,    0,    0,    0,
			  208,  207,  206,   79,   78,   77,   76,   75,   74,   73,
			   72,   71,   70, yyDummy>>)
		end

	yycheck_template: SPECIAL [INTEGER] is
			-- Template for `yycheck'
		once
			Result := yyfixed_array (<<
			   10,    9,   28,  222,    0,   97,    9,   10,  100,    3,
			   31,    7,    6,   13,   16,    8,   16,   13,   14,   89,
			   90,   62,   63,    3,   17,   95,    8,   27,   28,    0,
			   24,   43,   93,  125,   41,   64,    0,   30,   99,   32,
			  259,   34,   36,   64,   24,   74,   31,   23,   31,   30,
			   62,   31,   43,   55,   30,   55,   36,  218,   84,   66,
			   67,   68,   58,   59,   60,   61,   68,   69,   68,   69,
			  111,  141,   31,    3,   76,   77,   78,   79,   80,   89,
			   90,   64,   75,  244,   84,   95,   82,   97,   23,  159,
			  100,   31,    3,   75,   24,   30,  257,    3,   71,   72,

			   31,   31,  128,  129,  145,  146,   36,  268,  149,  150,
			  171,  272,  153,   24,  116,  125,  116,  209,   24,    6,
			   43,    8,  214,   31,   64,   36,  122,   76,  128,  129,
			   36,  141,  193,   64,    7,  176,    9,  178,  179,   71,
			   72,   73,    6,   30,    8,   32,    7,   59,    9,  159,
			   56,   58,   39,   17,   30,   41,   64,  227,  190,  191,
			  156,  157,   61,   54,  256,    7,   30,    9,   32,  261,
			   22,   31,   33,   37,   38,   39,   18,   19,   64,   48,
			   66,   67,   68,  275,   71,   72,   73,   29,   75,  185,
			   31,   33,   43,   30,  190,  191,   71,   72,   73,  209,

			   54,  209,   64,   26,  214,   69,   70,   71,   72,   73,
			  212,   75,  212,   25,  210,  211,   54,  227,    0,  215,
			  216,    3,    4,    5,    6,   54,  222,  229,  230,   27,
			  230,   23,  228,   76,   23,   17,   18,   19,   20,   21,
			   22,   42,   24,   30,   30,   27,  256,  255,   30,   31,
			   23,  261,   22,   35,   36,   71,   72,   73,   40,   41,
			   42,   43,   31,  259,   35,  275,   48,   49,   50,   51,
			   52,   53,   27,   55,   56,   57,   58,   59,   60,   61,
			   62,    0,   64,   65,   66,   67,   68,   40,   72,   71,
			   72,   73,   74,    7,    6,    9,    8,   23,   28,   18,

			   19,   20,   21,   18,   18,   19,   30,   49,   27,   31,
			   31,   30,   31,   44,   45,   46,   35,   31,   30,   33,
			   22,   40,   41,   42,   43,   28,   35,   39,   23,   48,
			   49,   50,   51,   52,   53,   26,   55,   30,   57,   58,
			   59,   60,   61,   62,   30,   64,   65,   66,   67,   68,
			   47,   26,   71,   72,   73,   74,    6,   75,    8,   71,
			   72,   73,  211,   75,    6,   -1,    8,   17,   -1,  149,
			  150,  166,   -1,  153,    7,   17,    9,   18,   19,   20,
			   30,    6,   32,    8,   -1,   18,   24,   -1,   30,   39,
			   32,   41,   17,   -1,   -1,   33,  176,   39,  178,  179,

			   33,   39,   71,   72,   73,   30,   44,   32,   -1,   18,
			   19,   20,   21,   -1,   39,   -1,   66,   67,   68,   69,
			   70,   71,   72,   73,   -1,   75,   35,   69,   70,   71,
			   72,   73,   74,   75,    7,    8,    9,   10,   63,   -1,
			    6,   -1,    8,   -1,   69,   70,   71,   72,   73,   -1,
			   75,   17,    6,   -1,    8,   18,   19,   20,   21,   30,
			   -1,   -1,   -1,   17,   30,   -1,   32,   -1,   31,   -1,
			   -1,   -1,   43,   39,   -1,   -1,   30,   48,   32,   50,
			   51,   52,   53,   -1,   55,   39,   57,   -1,   -1,   60,
			   -1,   62,   -1,   64,   65,   18,   19,   20,   21,   -1,

			   71,   72,   73,   69,   70,   71,   72,   73,   74,   75,
			   30,   -1,   -1,   -1,   -1,   69,   70,   71,   72,   73,
			   -1,   75,   -1,   43,   -1,   -1,   -1,   -1,   48,   -1,
			   50,   51,   52,   53,   -1,   55,   30,   57,   -1,   -1,
			   60,   -1,   62,   -1,   64,   65,   -1,   -1,   -1,   43,
			   -1,   71,   72,   73,   48,   -1,   50,   51,   52,   53,
			   -1,   55,   -1,   57,   -1,   -1,   60,   -1,   62,   -1,
			   64,   65,    3,    4,    5,   -1,   -1,   71,   72,   73,
			    3,    4,    5,   -1,   -1,   -1,   17,   -1,    3,    4,
			    5,   22,   -1,   24,   17,   -1,   -1,   -1,   -1,   22,

			   -1,   24,   17,   -1,   35,   36,   -1,   22,   31,   24,
			   -1,   -1,    7,   36,    9,   -1,   -1,   -1,   -1,   -1,
			   -1,   36,   -1,   18,   19,   50,   51,   52,   53,   -1,
			   55,   -1,   57,   -1,   29,   60,   -1,   -1,   33,   64,
			   65,   -1,   -1,   -1,   -1,   -1,   71,   72,   73,   50,
			   51,   52,   53,   -1,   55,   -1,   57,   -1,   -1,   60,
			   -1,   -1,   -1,   -1,   65,   -1,   -1,   -1,   -1,   -1,
			   71,   72,   73,    7,    8,    9,   10,   11,   12,   13,
			   14,   15,   16, yyDummy>>)
		end

feature {NONE} -- Semantic value stacks

	yyvs1: SPECIAL [ANY]
			-- Stack for semantic values of type ANY

	yyvsc1: INTEGER
			-- Capacity of semantic value stack `yyvs1'

	yyvsp1: INTEGER
			-- Top of semantic value stack `yyvs1'

	yyspecial_routines1: KL_SPECIAL_ROUTINES [ANY]
			-- Routines that ought to be in SPECIAL [ANY]

	yyvs2: SPECIAL [STRING]
			-- Stack for semantic values of type STRING

	yyvsc2: INTEGER
			-- Capacity of semantic value stack `yyvs2'

	yyvsp2: INTEGER
			-- Top of semantic value stack `yyvs2'

	yyspecial_routines2: KL_SPECIAL_ROUTINES [STRING]
			-- Routines that ought to be in SPECIAL [STRING]

	yyvs3: SPECIAL [QFIS_FORMULA]
			-- Stack for semantic values of type QFIS_FORMULA

	yyvsc3: INTEGER
			-- Capacity of semantic value stack `yyvs3'

	yyvsp3: INTEGER
			-- Top of semantic value stack `yyvs3'

	yyspecial_routines3: KL_SPECIAL_ROUTINES [QFIS_FORMULA]
			-- Routines that ought to be in SPECIAL [QFIS_FORMULA]

	yyvs4: SPECIAL [QFIS_ATOMIC_FORMULA]
			-- Stack for semantic values of type QFIS_ATOMIC_FORMULA

	yyvsc4: INTEGER
			-- Capacity of semantic value stack `yyvs4'

	yyvsp4: INTEGER
			-- Top of semantic value stack `yyvs4'

	yyspecial_routines4: KL_SPECIAL_ROUTINES [QFIS_ATOMIC_FORMULA]
			-- Routines that ought to be in SPECIAL [QFIS_ATOMIC_FORMULA]

	yyvs5: SPECIAL [QFIS_QUANTIFIED_FORMULA]
			-- Stack for semantic values of type QFIS_QUANTIFIED_FORMULA

	yyvsc5: INTEGER
			-- Capacity of semantic value stack `yyvs5'

	yyvsp5: INTEGER
			-- Top of semantic value stack `yyvs5'

	yyspecial_routines5: KL_SPECIAL_ROUTINES [QFIS_QUANTIFIED_FORMULA]
			-- Routines that ought to be in SPECIAL [QFIS_QUANTIFIED_FORMULA]

	yyvs6: SPECIAL [QFIS_SEQUENCE]
			-- Stack for semantic values of type QFIS_SEQUENCE

	yyvsc6: INTEGER
			-- Capacity of semantic value stack `yyvs6'

	yyvsp6: INTEGER
			-- Top of semantic value stack `yyvs6'

	yyspecial_routines6: KL_SPECIAL_ROUTINES [QFIS_SEQUENCE]
			-- Routines that ought to be in SPECIAL [QFIS_SEQUENCE]

	yyvs7: SPECIAL [QFIS_ARGUMENTS]
			-- Stack for semantic values of type QFIS_ARGUMENTS

	yyvsc7: INTEGER
			-- Capacity of semantic value stack `yyvs7'

	yyvsp7: INTEGER
			-- Top of semantic value stack `yyvs7'

	yyspecial_routines7: KL_SPECIAL_ROUTINES [QFIS_ARGUMENTS]
			-- Routines that ought to be in SPECIAL [QFIS_ARGUMENTS]

	yyvs8: SPECIAL [QFIS_SEQUENCE_COMPARISON]
			-- Stack for semantic values of type QFIS_SEQUENCE_COMPARISON

	yyvsc8: INTEGER
			-- Capacity of semantic value stack `yyvs8'

	yyvsp8: INTEGER
			-- Top of semantic value stack `yyvs8'

	yyspecial_routines8: KL_SPECIAL_ROUTINES [QFIS_SEQUENCE_COMPARISON]
			-- Routines that ought to be in SPECIAL [QFIS_SEQUENCE_COMPARISON]

	yyvs9: SPECIAL [QFIS_ARITHMETIC_COMPARISON]
			-- Stack for semantic values of type QFIS_ARITHMETIC_COMPARISON

	yyvsc9: INTEGER
			-- Capacity of semantic value stack `yyvs9'

	yyvsp9: INTEGER
			-- Top of semantic value stack `yyvs9'

	yyspecial_routines9: KL_SPECIAL_ROUTINES [QFIS_ARITHMETIC_COMPARISON]
			-- Routines that ought to be in SPECIAL [QFIS_ARITHMETIC_COMPARISON]

	yyvs10: SPECIAL [QFIS_ARITHMETIC_COMPOUND]
			-- Stack for semantic values of type QFIS_ARITHMETIC_COMPOUND

	yyvsc10: INTEGER
			-- Capacity of semantic value stack `yyvs10'

	yyvsp10: INTEGER
			-- Top of semantic value stack `yyvs10'

	yyspecial_routines10: KL_SPECIAL_ROUTINES [QFIS_ARITHMETIC_COMPOUND]
			-- Routines that ought to be in SPECIAL [QFIS_ARITHMETIC_COMPOUND]

	yyvs11: SPECIAL [QFIS_INTEGER_TERM]
			-- Stack for semantic values of type QFIS_INTEGER_TERM

	yyvsc11: INTEGER
			-- Capacity of semantic value stack `yyvs11'

	yyvsp11: INTEGER
			-- Top of semantic value stack `yyvs11'

	yyspecial_routines11: KL_SPECIAL_ROUTINES [QFIS_INTEGER_TERM]
			-- Routines that ought to be in SPECIAL [QFIS_INTEGER_TERM]

	yyvs12: SPECIAL [INTEGER]
			-- Stack for semantic values of type INTEGER

	yyvsc12: INTEGER
			-- Capacity of semantic value stack `yyvs12'

	yyvsp12: INTEGER
			-- Top of semantic value stack `yyvs12'

	yyspecial_routines12: KL_SPECIAL_ROUTINES [INTEGER]
			-- Routines that ought to be in SPECIAL [INTEGER]

	yyvs13: SPECIAL [QFIS_REGEXP]
			-- Stack for semantic values of type QFIS_REGEXP

	yyvsc13: INTEGER
			-- Capacity of semantic value stack `yyvs13'

	yyvsp13: INTEGER
			-- Top of semantic value stack `yyvs13'

	yyspecial_routines13: KL_SPECIAL_ROUTINES [QFIS_REGEXP]
			-- Routines that ought to be in SPECIAL [QFIS_REGEXP]

	yyvs14: SPECIAL [QFIS_PROGRAM]
			-- Stack for semantic values of type QFIS_PROGRAM

	yyvsc14: INTEGER
			-- Capacity of semantic value stack `yyvs14'

	yyvsp14: INTEGER
			-- Top of semantic value stack `yyvs14'

	yyspecial_routines14: KL_SPECIAL_ROUTINES [QFIS_PROGRAM]
			-- Routines that ought to be in SPECIAL [QFIS_PROGRAM]

	yyvs15: SPECIAL [QFIS_ROUTINE]
			-- Stack for semantic values of type QFIS_ROUTINE

	yyvsc15: INTEGER
			-- Capacity of semantic value stack `yyvs15'

	yyvsp15: INTEGER
			-- Top of semantic value stack `yyvs15'

	yyspecial_routines15: KL_SPECIAL_ROUTINES [QFIS_ROUTINE]
			-- Routines that ought to be in SPECIAL [QFIS_ROUTINE]

	yyvs16: SPECIAL [QFIS_PRECONDITION]
			-- Stack for semantic values of type QFIS_PRECONDITION

	yyvsc16: INTEGER
			-- Capacity of semantic value stack `yyvs16'

	yyvsp16: INTEGER
			-- Top of semantic value stack `yyvs16'

	yyspecial_routines16: KL_SPECIAL_ROUTINES [QFIS_PRECONDITION]
			-- Routines that ought to be in SPECIAL [QFIS_PRECONDITION]

	yyvs17: SPECIAL [QFIS_POSTCONDITION]
			-- Stack for semantic values of type QFIS_POSTCONDITION

	yyvsc17: INTEGER
			-- Capacity of semantic value stack `yyvs17'

	yyvsp17: INTEGER
			-- Top of semantic value stack `yyvs17'

	yyspecial_routines17: KL_SPECIAL_ROUTINES [QFIS_POSTCONDITION]
			-- Routines that ought to be in SPECIAL [QFIS_POSTCONDITION]

	yyvs18: SPECIAL [QFIS_LIST_VARIABLES]
			-- Stack for semantic values of type QFIS_LIST_VARIABLES

	yyvsc18: INTEGER
			-- Capacity of semantic value stack `yyvs18'

	yyvsp18: INTEGER
			-- Top of semantic value stack `yyvs18'

	yyspecial_routines18: KL_SPECIAL_ROUTINES [QFIS_LIST_VARIABLES]
			-- Routines that ought to be in SPECIAL [QFIS_LIST_VARIABLES]

	yyvs19: SPECIAL [QFIS_LOCAL]
			-- Stack for semantic values of type QFIS_LOCAL

	yyvsc19: INTEGER
			-- Capacity of semantic value stack `yyvs19'

	yyvsp19: INTEGER
			-- Top of semantic value stack `yyvs19'

	yyspecial_routines19: KL_SPECIAL_ROUTINES [QFIS_LOCAL]
			-- Routines that ought to be in SPECIAL [QFIS_LOCAL]

	yyvs20: SPECIAL [QFIS_SIGNATURE]
			-- Stack for semantic values of type QFIS_SIGNATURE

	yyvsc20: INTEGER
			-- Capacity of semantic value stack `yyvs20'

	yyvsp20: INTEGER
			-- Top of semantic value stack `yyvs20'

	yyspecial_routines20: KL_SPECIAL_ROUTINES [QFIS_SIGNATURE]
			-- Routines that ought to be in SPECIAL [QFIS_SIGNATURE]

	yyvs21: SPECIAL [QFIS_LIST_DECLARATIONS]
			-- Stack for semantic values of type QFIS_LIST_DECLARATIONS

	yyvsc21: INTEGER
			-- Capacity of semantic value stack `yyvs21'

	yyvsp21: INTEGER
			-- Top of semantic value stack `yyvs21'

	yyspecial_routines21: KL_SPECIAL_ROUTINES [QFIS_LIST_DECLARATIONS]
			-- Routines that ought to be in SPECIAL [QFIS_LIST_DECLARATIONS]

	yyvs22: SPECIAL [QFIS_BODY]
			-- Stack for semantic values of type QFIS_BODY

	yyvsc22: INTEGER
			-- Capacity of semantic value stack `yyvs22'

	yyvsp22: INTEGER
			-- Top of semantic value stack `yyvs22'

	yyspecial_routines22: KL_SPECIAL_ROUTINES [QFIS_BODY]
			-- Routines that ought to be in SPECIAL [QFIS_BODY]

	yyvs23: SPECIAL [QFIS_INSTRUCTION]
			-- Stack for semantic values of type QFIS_INSTRUCTION

	yyvsc23: INTEGER
			-- Capacity of semantic value stack `yyvs23'

	yyvsp23: INTEGER
			-- Top of semantic value stack `yyvs23'

	yyspecial_routines23: KL_SPECIAL_ROUTINES [QFIS_INSTRUCTION]
			-- Routines that ought to be in SPECIAL [QFIS_INSTRUCTION]

	yyvs24: SPECIAL [QFIS_LIST [QFIS_INSTRUCTION]]
			-- Stack for semantic values of type QFIS_LIST [QFIS_INSTRUCTION]

	yyvsc24: INTEGER
			-- Capacity of semantic value stack `yyvs24'

	yyvsp24: INTEGER
			-- Top of semantic value stack `yyvs24'

	yyspecial_routines24: KL_SPECIAL_ROUTINES [QFIS_LIST [QFIS_INSTRUCTION]]
			-- Routines that ought to be in SPECIAL [QFIS_LIST [QFIS_INSTRUCTION]]

	yyvs25: SPECIAL [QFIS_CONDITIONAL]
			-- Stack for semantic values of type QFIS_CONDITIONAL

	yyvsc25: INTEGER
			-- Capacity of semantic value stack `yyvs25'

	yyvsp25: INTEGER
			-- Top of semantic value stack `yyvs25'

	yyspecial_routines25: KL_SPECIAL_ROUTINES [QFIS_CONDITIONAL]
			-- Routines that ought to be in SPECIAL [QFIS_CONDITIONAL]

	yyvs26: SPECIAL [QFIS_LOOP]
			-- Stack for semantic values of type QFIS_LOOP

	yyvsc26: INTEGER
			-- Capacity of semantic value stack `yyvs26'

	yyvsp26: INTEGER
			-- Top of semantic value stack `yyvs26'

	yyspecial_routines26: KL_SPECIAL_ROUTINES [QFIS_LOOP]
			-- Routines that ought to be in SPECIAL [QFIS_LOOP]

	yyvs27: SPECIAL [QFIS_INVARIANT]
			-- Stack for semantic values of type QFIS_INVARIANT

	yyvsc27: INTEGER
			-- Capacity of semantic value stack `yyvs27'

	yyvsp27: INTEGER
			-- Top of semantic value stack `yyvs27'

	yyspecial_routines27: KL_SPECIAL_ROUTINES [QFIS_INVARIANT]
			-- Routines that ought to be in SPECIAL [QFIS_INVARIANT]

	yyvs28: SPECIAL [QFIS_LIST [QFIS_FORMULA]]
			-- Stack for semantic values of type QFIS_LIST [QFIS_FORMULA]

	yyvsc28: INTEGER
			-- Capacity of semantic value stack `yyvs28'

	yyvsp28: INTEGER
			-- Top of semantic value stack `yyvs28'

	yyspecial_routines28: KL_SPECIAL_ROUTINES [QFIS_LIST [QFIS_FORMULA]]
			-- Routines that ought to be in SPECIAL [QFIS_LIST [QFIS_FORMULA]]

	yyvs29: SPECIAL [QFIS_CALL]
			-- Stack for semantic values of type QFIS_CALL

	yyvsc29: INTEGER
			-- Capacity of semantic value stack `yyvs29'

	yyvsp29: INTEGER
			-- Top of semantic value stack `yyvs29'

	yyspecial_routines29: KL_SPECIAL_ROUTINES [QFIS_CALL]
			-- Routines that ought to be in SPECIAL [QFIS_CALL]

	yyvs30: SPECIAL [QFIS_FUNCTION_DECLARATION]
			-- Stack for semantic values of type QFIS_FUNCTION_DECLARATION

	yyvsc30: INTEGER
			-- Capacity of semantic value stack `yyvs30'

	yyvsp30: INTEGER
			-- Top of semantic value stack `yyvs30'

	yyspecial_routines30: KL_SPECIAL_ROUTINES [QFIS_FUNCTION_DECLARATION]
			-- Routines that ought to be in SPECIAL [QFIS_FUNCTION_DECLARATION]

	yyvs31: SPECIAL [QFIS_AXIOM]
			-- Stack for semantic values of type QFIS_AXIOM

	yyvsc31: INTEGER
			-- Capacity of semantic value stack `yyvs31'

	yyvsp31: INTEGER
			-- Top of semantic value stack `yyvs31'

	yyspecial_routines31: KL_SPECIAL_ROUTINES [QFIS_AXIOM]
			-- Routines that ought to be in SPECIAL [QFIS_AXIOM]

feature {NONE} -- Constants

	yyFinal: INTEGER is 285
			-- Termination state id

	yyFlag: INTEGER is -32768
			-- Most negative INTEGER

	yyNtbase: INTEGER is 78
			-- Number of tokens

	yyLast: INTEGER is 682
			-- Upper bound of `yytable' and `yycheck'

	yyMax_token: INTEGER is 332
			-- Maximum token id
			-- (upper bound of `yytranslate'.)

	yyNsyms: INTEGER is 118
			-- Number of symbols
			-- (terminal and nonterminal)

feature -- User-defined features



feature {NONE}  -- Initialization

	make  (filename: STRING) is
			-- Create a new parser and parse file with name `filename'
      do
			make_scanner (filename)
			make_parser_skeleton
			create error_messages.make
			parse
		end


	make_with_string  (str: STRING) is
			-- Create a new parser and parse string `str'
      do
			make_scanner_with_string (str)
			make_parser_skeleton
			create error_messages.make
			parse
		end
	
	
	
feature -- Access

   error_messages: LINKED_LIST [STRING]
			-- Error messages reported so far
	
	parsed_program: QFIS_PROGRAM

	parsed_formula: QFIS_FORMULA

	
feature {NONE}  -- Error reporting

   report_error (a_message: STRING) is
          -- Store error messages in error_messages
      do
         error_messages.extend ("Syntax error on line: " + line_nb.out)
		end

end
