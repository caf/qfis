axiom forall (X, Y: SEQUENCE) (Y =/= e ==> (X ++ Y[1] ++ (Y<<2:0>>) == X ++ Y))

function max_of (SEQUENCE): INTEGER
axiom forall (S: SEQUENCE) (|S| = 1 ==> max_of[S] = S[1])
axiom forall (X: SEQUENCE; v: INTEGER) 
	(|X| > 0 and v <= max_of[X] ==> max_of[X ++ v] = max_of[X])
axiom forall (X: SEQUENCE; v: INTEGER) 
	(|X| > 0 and v >= max_of[X] ==> max_of[X ++ v] = v)

function sum_of (SEQUENCE): INTEGER
axiom sum_of[e] = 0
axiom forall (v: INTEGER) (sum_of[v] = v)
axiom forall (X: SEQUENCE; v: INTEGER) 
	(sum_of[X ++ v] = sum_of[X] + v)
	
axiom forall (X: SEQUENCE) (|X| > 0 ==> sum_of[X] <= |X|*max_of[X])


routine max (Sequence: SEQUENCE): (result: INTEGER)
require
	|Sequence| > 0
local
	Head, Tail: SEQUENCE
do
	Head := Sequence[1]
	Tail := Sequence<<2:0>>
	result := Sequence[1]
	until Tail == e 
	invariant
		_Head_not_empty: |Head| > 0
		_Head_Tail: Head ++ Tail == Sequence
		_max:			result = max_of [Head]
	loop
		if result < Tail[1] then
			result := Tail[1]
		end
		Head := Head ++ Tail[1]
		Tail := Tail<<2:0>>
	end
ensure
	 _max:	result == max_of [Sequence]
end


routine sum (Sequence: SEQUENCE): (result: INTEGER)
local
	Head, Tail: SEQUENCE
do
	if Sequence == e then
		result := 0
	else
		Head := Sequence[1]
		Tail := Sequence<<2:0>>
		result := Sequence[1]
		until Tail == e 
		invariant
			_Head_not_empty: |Head| > 0
			_Head_Tail: Head ++ Tail == Sequence
			_sum:			result = sum_of [Head]
		loop
			result := result + Tail[1]
			Head := Head ++ Tail[1]
			Tail := Tail<<2:0>>
		end
	end
ensure
	_sum:			result = sum_of [Sequence]
end

global last_sum, last_max: INTEGER

routine sum_and_max (Sequence: SEQUENCE)
require
	|Sequence| > 0
modify
	last_sum, last_max
do
	call last_sum := sum (Sequence)
	call last_max := max (Sequence)
ensure
	last_sum <= |Sequence| * last_max
end

