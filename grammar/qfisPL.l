%{
-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

class QFIS_PL_SCANNER
-- QFIS: Quantifier Free Integer Sequences

inherit

   YY_COMPRESSED_SCANNER_SKELETON

   QFIS_PL_TOKEN_CODES

create

   make_with_filename, make_with_string
%}

DIGIT [0-9]
ALPHA [a-zA-Z]
ALPHANUM {ALPHA}|{DIGIT}
SPECIAL ["_""#"]
ANYCHAR {ALPHANUM}|{SPECIAL}


%%


   -- Eats up single-line comments
"--".*\n                  line_nb := line_nb + 1


[ \t]+                  -- Discards blanks


\n                    line_nb := line_nb + 1


   -- keywords: LOGIC

  -- concatenation
"++"            { last_string_value := text; last_token := CATKW }
  -- sequence equality
"=="            { last_string_value := text; last_token := EQSEQUENCEKW }
  -- sequence inequality
"=/="           { last_string_value := text; last_token := NEQSEQUENCEKW }
  -- sequence length
"|"             { last_string_value := text; last_token := LENGTHKW }

  -- arithmetic plus
"+"             { last_string_value := text; last_token := PLUSKW }
  -- arithmetic minus
"-"             { last_string_value := text; last_token := MINUSKW }
  -- arithmetic times
"*"             { last_string_value := text; last_token := TIMESKW }
  -- arithmetic divide
"/"             { last_string_value := text; last_token := DIVIDEKW }

  -- arithmetic inequality
"/="            { last_string_value := text; last_token := NEQKW }
  -- arithmetic equality
"="             { last_string_value := text; last_token := EQKW }
  -- arithmetic less than
"<"             { last_string_value := text; last_token := LTKW }
  -- arithmetic less than or equal to
"<="            { last_string_value := text; last_token := LTEKW }
  -- arithmetic greater than
">"             { last_string_value := text; last_token := GTKW }
  -- arithmetic greater than or equal to
">="            { last_string_value := text; last_token := GTEKW }

  -- logic not
"not"           { last_string_value := text; last_token := NOTKW }
  -- logic and
"and"           { last_string_value := text; last_token := ANDKW }
  -- logic or
"or"            { last_string_value := text; last_token := ORKW }
  -- logic implies
"==>"           { last_string_value := text; last_token := IMPLIESKW }
  -- logic iff
"<==>"          { last_string_value := text; last_token := IFFKW }
  -- constrained
"in"            { last_string_value := text; last_token := INKW }
  -- range delimiter
":"            { last_string_value := text; last_token := COLONKW }

"forall"            { last_string_value := text ; last_token := FORALLKW }
"exist"             { last_string_value := text ; last_token := EXISTKW }
"old"               { last_string_value := text ; last_token := OLDKW }

  -- parentheses
"<<"                { last_string_value := text; last_token := OPENRANGEKW }
">>"                { last_string_value := text; last_token := CLOSEDRANGEKW }
"["                 { last_string_value := text; last_token := OPENBRACKETKW }
"]"                 { last_string_value := text; last_token := CLOSEDBRACKETKW }
"{"                 { last_string_value := text; last_token := CURLYOPENKW }
"}"                 { last_string_value := text; last_token := CURLYCLOSEDKW }
"("                 { last_string_value := text; last_token := OPENPARENKW }
")"                 { last_string_value := text; last_token := CLOSEDPARENKW }


   -- keywords: Programming Language
"local"             { last_string_value := text ; last_token := LOCALKW }
"global"            { last_string_value := text ; last_token := GLOBALKW }
"do"                { last_string_value := text ; last_token := DOKW }
"end"               { last_string_value := text ; last_token := ENDKW }
"BOOLEAN"           { last_string_value := text ; last_token := BOOLEAN }
"SEQUENCE"          { last_string_value := text ; last_token := SEQUENCE }
"INTEGER"           { last_string_value := text ; last_token := INTEGER }
"require"           { last_string_value := text ; last_token := REQUIREKW }
"ensure"            { last_string_value := text ; last_token := ENSUREKW }
"modify"            { last_string_value := text ; last_token := MODIFYKW }
"assert"            { last_string_value := text ; last_token := ASSERTKW }
"assume"            { last_string_value := text ; last_token := ASSUMEKW }
"havoc"             { last_string_value := text ; last_token := HAVOCKW }
"skip"              { last_string_value := text ; last_token := SKIPKW }
":="                { last_string_value := text ; last_token := ASSIGNKW }
"split"             { last_string_value := text ; last_token := SPLITKW }
"into"              { last_string_value := text ; last_token := INTOKW }
"until"             { last_string_value := text ; last_token := UNTILKW }
"loop"              { last_string_value := text ; last_token := LOOPKW }
"invariant"         { last_string_value := text ; last_token := INVARIANTKW }
"if"                { last_string_value := text ; last_token := IFKW }
"then"              { last_string_value := text ; last_token := THENKW }
"else"              { last_string_value := text ; last_token := ELSEKW }
"<?>"               { last_string_value := text ; last_token := NDCHOICEKW }
";"                 { last_string_value := text ; last_token := TERMINATORKW }
"call"              { last_string_value := text ; last_token := CALLKW }
"routine"           { last_string_value := text ; last_token := ROUTINEKW }
"function"          { last_string_value := text ; last_token := FUNCTIONKW }
"axiom"             { last_string_value := text ; last_token := AXIOMKW }


  -- regular expressions: empty string
"e"                 { last_string_value := text; last_token := EPSILONKW }
"."                 { last_string_value := text; last_token := RECATKW }
"U"                 { last_string_value := text; last_token := REUNIVERSEKW }


   -- True, False
"True"   { last_string_value := text; last_token := TRUEKW }
"False"   { last_string_value := text; last_token := FALSEKW }


  -- Comma: argument separator
","  { last_string_value := text ; last_token := COMMAKW }

  -- Position sign
"@"  { last_string_value := text ; last_token := POSITIONKW }

   -- identifiers
{ALPHA}{ANYCHAR}*"?" { last_string_value := text ; last_token := BOOLEANID }
[A-Z]{ANYCHAR}*     { last_string_value := text ; last_token := SEQUENCEID }
[a-z]{ANYCHAR}*     { last_string_value := text ; last_token := INTEGERID }
_{ANYCHAR}*":"  { last_string_value := text ; last_token := LABELID }

   -- numbers
{DIGIT}+   { last_string_value := text; last_token := NUMBER }

  -- quoted string
\"([^\"])+\"  { last_string_value := text.substring (2, text.count-1) ; last_token := DOUBLEQUOTEDSTRING }



%%

  line_nb: INTEGER
           -- Current line number

  scanned_file: PLAIN_TEXT_FILE
  scanned_file_to_string: STRING
  scanned_file_to_buffer: YY_BUFFER

  feature {NONE} -- Initialization

       make_with_filename (filename: STRING) is
             -- Create new scanner with `filename' as input file
          do
             line_nb := 1
             create scanned_file.make_open_read (filename)

             scanned_file.read_stream (scanned_file.count)

				 scanned_file_to_string := scanned_file.last_string.twin
             create scanned_file_to_buffer.make (scanned_file_to_string)
				 make_with_buffer (scanned_file_to_buffer)
--           scan
         end


       make_with_string (str: STRING) is
             -- Create new scanner for string `str'
          do
             line_nb := 1
             scanned_file_to_string := str
             create scanned_file_to_buffer.make (scanned_file_to_string)
			    make_with_buffer (scanned_file_to_buffer)
--             scan
         end

end
