%{
-- Copyright (C) 2011  Carlo A. Furia
--        caf@inf.ethz.ch  http://se.inf.ethz.ch/people/furia/
--                     
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

note

  description: "Parser for the quantifier-free fragment of the theory of integer sequences and uninterpreted functions and its programming language"

class QFIS_PL_PARSER

inherit

	  YY_PARSER_SKELETON
	    rename
	      make as make_parser_skeleton
       redefine
	      report_error
       end

	  QFIS_PL_SCANNER
	     rename
	       make as make_std_scanner,
	  make_with_filename as make_scanner,
	       make_with_string as make_scanner_with_string
	     export
	       {NONE} all
		  end

create

						 make, make_with_string

%}



     -- geyacc declarations

%token <STRING> CATKW           -- ++
%token <STRING> EQSEQUENCEKW    -- ==
%token <STRING> NEQSEQUENCEKW   -- =/=
%token <STRING> LENGTHKW        -- | 
%token <STRING> PLUSKW          -- +
%token <STRING> MINUSKW         -- -
%token <STRING> TIMESKW         -- *
%token <STRING> DIVIDEKW        -- /
%token <STRING> NEQKW           -- /=
%token <STRING> EQKW            -- =
%token <STRING> LTKW            -- <
%token <STRING> LTEKW           -- <=
%token <STRING> GTKW            -- >
%token <STRING> GTEKW           -- >= 
%token <STRING> NOTKW           -- not 
%token <STRING> ANDKW           -- and
%token <STRING> ORKW            -- or
%token <STRING> IMPLIESKW       -- ==>
%token <STRING> IFFKW           -- <==>
%token <STRING> INKW            -- in 
%token <STRING> COLONKW         -- :
%token <STRING> OPENRANGEKW     -- <<
%token <STRING> CLOSEDRANGEKW   -- >> 
%token <STRING> OPENBRACKETKW   -- [
%token <STRING> CLOSEDBRACKETKW -- ]
%token <STRING> CURLYOPENKW     -- {
%token <STRING> CURLYCLOSEDKW   -- }
%token <STRING> OPENPARENKW     -- (
%token <STRING> CLOSEDPARENKW   -- )
%token <STRING> EPSILONKW       -- e
%token <STRING> RECATKW         -- .
%token <STRING> REUNIVERSEKW    -- U
%token <STRING> COMMAKW         -- ,
%token <STRING> POSITIONKW      -- @
%token <STRING> FORALLKW        -- forall
%token <STRING> EXISTKW         -- exist
%token <STRING> OLDKW           -- old

%token <STRING> LOCALKW         -- local
%token <STRING> GLOBALKW        -- global
%token <STRING> DOKW            -- do
%token <STRING> ENDKW           -- end
%token <STRING> BOOLEAN         -- BOOLEAN
%token <STRING> SEQUENCE        -- SEQUENCE
%token <STRING> INTEGER         -- INTEGER
%token <STRING> REQUIREKW       -- require
%token <STRING> ENSUREKW        -- ensure
%token <STRING> MODIFYKW        -- modify
%token <STRING> ASSERTKW        -- assert
%token <STRING> ASSUMEKW        -- assume
%token <STRING> HAVOCKW         -- havoc
%token <STRING> SKIPKW          -- skip
%token <STRING> ASSIGNKW        -- :=
%token <STRING> SPLITKW         -- split
%token <STRING> INTOKW          -- into
%token <STRING> UNTILKW         -- until
%token <STRING> LOOPKW          -- loop
%token <STRING> INVARIANTKW     -- invariant
%token <STRING> IFKW            -- if
%token <STRING> THENKW          -- then
%token <STRING> ELSEKW          -- else
%token <STRING> NDCHOICEKW      -- <?> (nondeterministic choice)
%token <STRING> TERMINATORKW    -- ;
%token <STRING> CALLKW          -- call
%token <STRING> ROUTINEKW       -- routine
%token <STRING> FUNCTIONKW      -- function
%token <STRING> AXIOMKW         -- axiom


%token <STRING> TRUEKW  -- True
%token <STRING> FALSEKW  -- False

%token <STRING> SEQUENCEID      -- sequence identifier
%token <STRING> INTEGERID       -- integer identifier
%token <STRING> BOOLEANID       -- predicate/boolean identifier
%token <STRING> LABELID         -- label identifier
%token <STRING> NUMBER          -- number

%token <STRING> DOUBLEQUOTEDSTRING -- "..."

%left TERMINATORKW
%left IFFKW
%right IMPLIESKW 
%left FORALLKW EXISTSKW
%left ORKW
%left ANDKW
%left NOTKW
%left EQKW NEQKW LTKW LTEKW GTKW GTEKW EQSEQUENCEKW NEQSEQUENCEKW INKW
%left OPENRANGEKW CLOSEDRANGEKW
%left RECATKW
%left PLUSKW MINUSKW
%left TIMESKW DIVIDEKW
%left CATKW POSITIONKW


%type <QFIS_FORMULA> Formula
%type <QFIS_ATOMIC_FORMULA> AtomicFormula
%type <QFIS_QUANTIFIED_FORMULA> QuantifiedFormula
%type <QFIS_SEQUENCE> SequenceNT
%type <QFIS_ARGUMENTS> Arguments
%type <QFIS_SEQUENCE_COMPARISON> SequenceComparison
%type <QFIS_ARITHMETIC_COMPARISON> ArithmeticComparison
%type <QFIS_ARITHMETIC_COMPOUND> ArithmeticCompound
%type <QFIS_INTEGER_TERM> IntegerTerm
%type <INTEGER> IntegerValue
%type <QFIS_REGEXP> RegExp

%type <QFIS_PROGRAM> Program
%type <QFIS_ROUTINE> Routine
%type <QFIS_PRECONDITION> Precondition
%type <QFIS_POSTCONDITION> Postcondition
%type <QFIS_LIST_VARIABLES> Modify
%type <QFIS_LOCAL> Locals
%type <QFIS_SIGNATURE> Signature
%type <QFIS_LIST_DECLARATIONS> Declarations
%type <QFIS_LIST_VARIABLES> IdentifierList
%type <STRING> Identifier
%type <STRING> Type
%type <QFIS_LIST_VARIABLES> Types
%type <QFIS_BODY> Body
%type <QFIS_LIST [QFIS_INSTRUCTION]> Instructions
%type <QFIS_INSTRUCTION> Instruction
%type <QFIS_CONDITIONAL> Conditional
%type <QFIS_LIST [QFIS_INSTRUCTION]> Then
%type <QFIS_LIST [QFIS_INSTRUCTION]> Else
%type <QFIS_FORMULA> Condition
%type <QFIS_LOOP> Loop
%type <QFIS_INVARIANT> Invariant
%type <QFIS_LIST [QFIS_INSTRUCTION]> LoopBody
%type <QFIS_LIST [QFIS_FORMULA]> Assertions
%type <QFIS_CALL> RoutineCall
%type <STRING> RoutineName
%type <QFIS_FUNCTION_DECLARATION> LogicFunction
%type <QFIS_AXIOM> AxiomDeclaration
%type <QFIS_FORMULA> AnyFormula

	-- Grammar follows.
%%

input: Program { parsed_program := $1 }
  |    Formula { parsed_formula := $1 }
;

Program: Routine                      { create {QFIS_PROGRAM} $$.make ; $$.extend ($1) }
  | GLOBALKW Declarations             { create {QFIS_PROGRAM} $$.make ; $$.extend ($2) }
  | FUNCTIONKW LogicFunction          { create {QFIS_PROGRAM} $$.make ; $$.extend ($2) }
  | AXIOMKW AxiomDeclaration          { create {QFIS_PROGRAM} $$.make ; $$.extend ($2) }
  | Routine Program                   { $2.put_front ($1) ; $$ := $2 }
  | GLOBALKW Declarations Program     { $3.put_front ($2) ; $$ := $3 }
  | FUNCTIONKW LogicFunction Program  { $3.put_front ($2) ; $$ := $3 }
  | AXIOMKW AxiomDeclaration Program  { $3.put_front ($2) ; $$ := $3 }
;

AnyFormula: Formula { $$ := $1 }
  | QuantifiedFormula { $$ := $1 }
;

AxiomDeclaration: AnyFormula { create $$.make ($1) ; $$.line_number := $1.line_number }
;

LogicFunction: RoutineName COLONKW Type
                       { create $$.make ($1, $3) }
  | RoutineName COLONKW Type INKW DOUBLEQUOTEDSTRING
                       { create $$.make ($1, $3) ; $$.set_file_name ($5) }
  | RoutineName OPENPARENKW Types CLOSEDPARENKW COLONKW Type
                       { create $$.make ($1, $6) ; $$.set_domain ($3) }
  | RoutineName OPENPARENKW Types CLOSEDPARENKW COLONKW Type INKW DOUBLEQUOTEDSTRING
                       { create $$.make ($1, $6) ; $$.set_domain ($3) ; $$.set_file_name ($8) }
;

Routine: Signature Precondition Modify Locals Body Postcondition ENDKW
  { create $$.make ($1, $2, $3, $4, $5, $6) ; $$.line_number := $1.line_number }
;

Precondition:      { create $$.make ; $$.line_number := line_nb }
    |   REQUIREKW  { create $$.make ; $$.line_number := line_nb }
    |   REQUIREKW Assertions { create $$.make_from_list ($2) ; $$.line_number := $2.line_number }
;

Postcondition:     { create $$.make ; $$.line_number := line_nb }
    |   ENSUREKW   { create $$.make ; $$.line_number := line_nb }
    |   ENSUREKW Assertions  { create $$.make_from_list ($2) ; $$.line_number := $2.line_number }
;

Locals:            { create $$.make ; $$.line_number := line_nb }
    |   LOCALKW    { create $$.make ; $$.line_number := line_nb }
    |   LOCALKW Declarations { create $$.make_from_declarations ($2) ; $$.line_number := $2.line_number }
;

Modify:           { create $$.make ; $$.line_number := line_nb }
    |  MODIFYKW   { create $$.make ; $$.line_number := line_nb }
    |  MODIFYKW IdentifierList
                  { $$ := $2 ; $$.line_number := $2.line_number }
;

Signature: ROUTINEKW RoutineName  { create $$.make ($2, Void, Void) ; $$.line_number := line_nb }
    |      ROUTINEKW RoutineName OPENPARENKW Declarations CLOSEDPARENKW
 	                   { create $$.make ($2, $4, Void) ; $$.line_number := line_nb }
    |      ROUTINEKW RoutineName COLONKW OPENPARENKW Declarations CLOSEDPARENKW
	                   { create $$.make ($2, Void, $5) ; $$.line_number := line_nb }
    |      ROUTINEKW RoutineName OPENPARENKW Declarations CLOSEDPARENKW COLONKW OPENPARENKW Declarations CLOSEDPARENKW
	                   { create $$.make ($2, $4, $8) ; $$.line_number := line_nb }
;

RoutineName: BOOLEANID  { $$ := $1 }
   | SEQUENCEID         { $$ := $1 }
   | INTEGERID          { $$ := $1 }
;

Declarations: IdentifierList COLONKW Type
              { create $$.make ; if $3 ~ token_name (BOOLEAN) then $$.add_booleans ($1) elseif $3 ~ token_name (SEQUENCE) then $$.add_sequences ($1) elseif $3 ~ token_name (INTEGER) then $$.add_integers ($1) end ; $$.line_number := $1.line_number }
    |         Declarations TERMINATORKW IdentifierList COLONKW Type
	           { $$ := $1 ; if $5 ~ token_name (BOOLEAN) then $$.add_booleans ($3) elseif $5 ~ token_name (SEQUENCE) then $$.add_sequences ($3) elseif $5 ~ token_name (INTEGER) then $$.add_integers ($3) end ; $$.line_number := $1.line_number }
;

IdentifierList: Identifier  { create $$.make ; $$.extend ($1) ; $$.line_number := line_nb }
    |           Identifier COMMAKW IdentifierList
	                         { $$ := $3 ; $$.put_front ($1) ; $$.line_number := $3.line_number }
;

Type: BOOLEAN  { $$ := $1 }
  |   SEQUENCE { $$ := $1 }
  |   INTEGER  { $$ := $1 }
;

Types: Type    { create $$.make ; $$.extend ($1) }
  | Type COMMAKW Types
               { $3.put_front ($1) ; $$ := $3 }
;

Body: DOKW               { create $$.make ; $$.line_number := line_nb }
  |   DOKW Instructions  { create $$.make_with_instructions ($2) ; $$.line_number := $2.line_number }
;

Instructions: Instruction  { create $$.make ; $$.extend ($1) ; $$.line_number := $1.line_number }
  |           Instruction Instructions
                           { $$ := $2 ; $$.put_front ($1) ; $$.line_number := $1.line_number }
  |           Instruction TERMINATORKW Instructions
                           { $$ := $3 ; $$.put_front ($1) ; $$.line_number := $1.line_number }
;

Instruction: SKIPKW
                 { create {QFIS_SKIP} $$ ; $$.line_number := line_nb }
  |          ASSERTKW Formula
                 { create {QFIS_ASSERT} $$.make ($2) ; $$.line_number := $2.line_number }
  |          ASSUMEKW Formula
                 { create {QFIS_ASSUME} $$.make ($2) ; $$.line_number := $2.line_number }
  |          HAVOCKW IdentifierList
                 { create {QFIS_HAVOC} $$.make ($2) ; $$.line_number := $2.line_number }
  |          SPLITKW SequenceNT INTOKW IdentifierList
                 { create {QFIS_SPLIT} $$.make ($2, $4) ; $$.line_number := $2.line_number }
  |          SEQUENCEID ASSIGNKW SequenceNT
                 { create {QFIS_ASSIGN_SEQUENCE} $$.make ($1, $3) ; $$.line_number := $3.line_number }
  |          BOOLEANID ASSIGNKW Formula
                 { create {QFIS_ASSIGN_BOOLEAN} $$.make ($1, $3) ; $$.line_number := $3.line_number }
  |          INTEGERID ASSIGNKW IntegerTerm
                 { create {QFIS_ASSIGN_INTEGER} $$.make ($1, $3) ; $$.line_number := $3.line_number }
  |          Conditional
                 { $$ := $1 ; $$.line_number := $1.line_number }
  |          Loop
                 { $$ := $1 ; $$.line_number := $1.line_number }
  |          RoutineCall
                 { $$ := $1 ; $$.line_number := $1.line_number }
;

Conditional: IFKW Condition Then ENDKW       { create $$.make ($2, $3, Void) ; $$.line_number := $2.line_number }
  |          IFKW Condition Then Else ENDKW  { create $$.make ($2, $3, $4) ; $$.line_number := $2.line_number }
;

Then: THENKW { $$ := Void }
  |   THENKW Instructions { $$ := $2 ; $$.line_number := $2.line_number }
;

Else: ELSEKW { $$ := Void }
  |   ELSEKW Instructions { $$ := $2 ; $$.line_number := line_nb }
;

Condition: NDCHOICEKW { $$ := Void ; $$.line_number := line_nb }
|        Formula      { $$ := $1 ; $$.line_number := $1.line_number }
;

Loop: UNTILKW Condition Invariant LoopBody ENDKW { create $$.make ($2, $3, $4) ; $$.line_number := $2.line_number }
;

Invariant:        { create $$.make ; $$.line_number := line_nb }
   | INVARIANTKW  { create $$.make ; $$.line_number := line_nb }
   | INVARIANTKW Assertions { create $$.make_from_list ($2) ; $$.line_number := $2.line_number }
;

LoopBody: LOOPKW { create $$.make ; $$.line_number := line_nb }
  |   LOOPKW Instructions { $$ := $2 ; $$.line_number := $2.line_number }
;

Assertions: Formula { create $$.make ; $$.extend ($1) ; $$.line_number := $1.line_number }
|         LABELID Formula { $2.set_label ($1) ; create $$.make ; $$.extend ($2) ; $$.line_number := $2.line_number }
  |         Assertions TERMINATORKW Formula
                            { $1.extend ($3) ; $$ := $1 }
  |         Assertions TERMINATORKW LABELID Formula
                            { $4.set_label ($3) ; $1.extend ($4) ; $$ := $1 }
  |         Assertions LABELID Formula
                            { $3.set_label ($2) ; $1.extend ($3) ; $$ := $1 }
;

RoutineCall: CALLKW RoutineName
       { create {QFIS_CALL_COMMAND} $$.make ($2, Void) ; $$.line_number := line_nb }
  |          CALLKW RoutineName OPENPARENKW IdentifierList CLOSEDPARENKW
		 { create {QFIS_CALL_COMMAND} $$.make ($2, $4) ; $$.line_number := $4.line_number }
  |          CALLKW IdentifierList ASSIGNKW RoutineName
       { create {QFIS_CALL_FUNCTION} $$.make ($4, Void, $2) ; $$.line_number := $2.line_number }
  |          CALLKW IdentifierList ASSIGNKW RoutineName OPENPARENKW IdentifierList CLOSEDPARENKW
       { create {QFIS_CALL_FUNCTION} $$.make ($4, $6, $2) ; $$.line_number := $2.line_number }
;




Formula: AtomicFormula
{ $$ := $1 ; $$.line_number := $1.line_number }
  |  NOTKW Formula
             { create {QFIS_NOT_FORMULA} $$.make ($2) ; $$.line_number := $2.line_number }
  |  BOOLEANID OPENPARENKW Arguments CLOSEDPARENKW
  { create {QFIS_PREDICATE_FORMULA} $$.make ($1, $3) ; $$.line_number := $3.line_number }
  |  OPENPARENKW Formula CLOSEDPARENKW
             { $$ := $2 ; $$.line_number := $2.line_number }
  |  Formula ANDKW Formula
             { create {QFIS_AND_FORMULA} $$.make ($1, $3) ; $$.line_number := $1.line_number }
  |  Formula ORKW Formula
             { create {QFIS_OR_FORMULA} $$.make ($1, $3) ; $$.line_number := $1.line_number }
  |  Formula IMPLIESKW Formula
             { create {QFIS_IMPLIES_FORMULA} $$.make ($1, $3) ; $$.line_number := $1.line_number }
  |  Formula IFFKW Formula
             { create {QFIS_IFF_FORMULA} $$.make ($1, $3) ; $$.line_number := $1.line_number }
;

QuantifiedFormula: FORALLKW OPENPARENKW Declarations CLOSEDPARENKW OPENPARENKW AnyFormula CLOSEDPARENKW     { create {QFIS_FORALL_FORMULA} $$.make ($3, $6) ; $$.line_number := $6.line_number }
| EXISTKW OPENPARENKW Declarations CLOSEDPARENKW OPENPARENKW AnyFormula CLOSEDPARENKW
               { create {QFIS_EXIST_FORMULA} $$.make ($3, $6) ; $$.line_number := $6.line_number }
;

AtomicFormula: TRUEKW { create {QFIS_BOOLEAN_CONSTANT} $$.make (True) ; $$.line_number := line_nb }
  |  FALSEKW          { create {QFIS_BOOLEAN_CONSTANT} $$.make (False) ; $$.line_number := line_nb }
  |  BOOLEANID        { create {QFIS_BOOLEAN_VARIABLE} $$.make ($1) ; $$.line_number := line_nb }
  |  OLDKW BOOLEANID 
     { create {QFIS_BOOLEAN_VARIABLE} $$.make ($2) ; $$.set_old ; $$.line_number := line_nb }
  |  SequenceNT SequenceComparison SequenceNT
                    { $2.set ($1, $3) ; $$ := $2 ; $$.line_number := $1.line_number }
  |  IntegerTerm ArithmeticComparison IntegerTerm
                    { $2.set ($1, $3) ; $$ := $2 ; $$.line_number := $1.line_number }
  |  SequenceNT INKW CURLYOPENKW RegExp CURLYCLOSEDKW
                    { create {QFIS_REGEXP_CONSTRAINT} $$.make($1, $4, True) ; $$.line_number := $1.line_number }
  |  SequenceNT NOTKW INKW CURLYOPENKW RegExp CURLYCLOSEDKW
                    { create {QFIS_REGEXP_CONSTRAINT} $$.make($1, $5, False) ; $$.line_number := $1.line_number }
;

SequenceComparison: EQSEQUENCEKW
                    { create {QFIS_SEQUENCE_EQ} $$.make (Void, Void) ; $$.line_number := line_nb }
  | NEQSEQUENCEKW
						  { create {QFIS_SEQUENCE_NEQ} $$.make (Void, Void) ; $$.line_number := line_nb }
;

ArithmeticComparison: EQKW
          { create {QFIS_ARITHMETIC_EQ} $$.make (Void, Void) ; $$.line_number := line_nb }
  | NEQKW
          { create {QFIS_ARITHMETIC_NEQ} $$.make (Void, Void) ; $$.line_number := line_nb }
  | LTKW
          { create {QFIS_ARITHMETIC_LT} $$.make (Void, Void) ; $$.line_number := line_nb }
  | LTEKW
          { create {QFIS_ARITHMETIC_LTE} $$.make (Void, Void) ; $$.line_number := line_nb }
  | GTKW
          { create {QFIS_ARITHMETIC_GT} $$.make (Void, Void) ; $$.line_number := line_nb }
  | GTEKW
          { create {QFIS_ARITHMETIC_GTE} $$.make (Void, Void) ; $$.line_number := line_nb }
;

SequenceNT: SEQUENCEID  { create {QFIS_VARIABLE} $$.make ($1) ; $$.line_number := line_nb }
  |  OLDKW SEQUENCEID 
     { create {QFIS_VARIABLE} $$.make ($2) ; $$.set_old ; $$.line_number := line_nb }
  |  EPSILONKW        { create {QFIS_SEQUENCE_EMPTY} $$.make ; $$.line_number := line_nb }
  |  IntegerTerm
                      { create {QFIS_SEQUENCE_SINGLETON} $$.make ($1) ; $$.line_number := $1.line_number }
  |  SEQUENCEID OPENPARENKW Arguments CLOSEDPARENKW
                      { create {QFIS_FUNCTION} $$.make ($1, $3) ; $$.line_number := $3.line_number }
  |  OPENPARENKW SequenceNT CLOSEDPARENKW
                      { $$ := $2 ; $$.line_number := $2.line_number }
  |  SequenceNT OPENRANGEKW IntegerValue COLONKW IntegerValue CLOSEDRANGEKW
                      { create {QFIS_RANGE} $$.make ($1, $3, $5) ; $$.line_number := $1.line_number }
  |  SequenceNT POSITIONKW IntegerValue
                      { create {QFIS_RANGE} $$.make ($1, $3, $3) ; $$.line_number := $1.line_number }
  |  SequenceNT CATKW SequenceNT
                      { create {QFIS_CAT_COMPOUND} $$.make ($1, $3) ; $$.line_number := $1.line_number }
;

Identifier: INTEGERID  { $$ := $1 }
  |         BOOLEANID  { $$ := $1 }
  |         SEQUENCEID { $$ := $1 }
;

IntegerValue: NUMBER     { $$ := $1.to_integer }
  |  MINUSKW NUMBER { $$ := $2.to_integer ; $$ := - $$ }
;

RegExp: EPSILONKW
      { create {QFIS_RE_EPSILON} $$ }
  |  IntegerValue
      { create {QFIS_RE_INTEGER} $$.make ($1) }
  |  REUNIVERSEKW
      { create {QFIS_RE_UNIVERSE} $$ }
  |  NOTKW RegExp
      { create {QFIS_RE_NOT} $$.make ($2) }
  |  OPENPARENKW RegExp CLOSEDPARENKW
      { $$ := $2 }
  |  RegExp RECATKW RegExp
      { create {QFIS_RE_CAT} $$.make ($1, $3) }
  |  RegExp TIMESKW
      { create {QFIS_RE_STAR} $$.make ($1) }
  |  RegExp PLUSKW
      { create {QFIS_RE_PLUS} $$.make ($1) }
  |  RegExp ANDKW RegExp
      { create {QFIS_RE_AND} $$.make ($1, $3) }
  |  RegExp ORKW RegExp
      { create {QFIS_RE_OR} $$.make ($1, $3) }
;

Arguments: SequenceNT
              { create $$.make_empty ; $$.extend ($1) ; $$.line_number := $1.line_number }
  |  Formula
              { create $$.make_empty ; $$.extend ($1) ; $$.line_number := $1.line_number }
  |  SequenceNT COMMAKW Arguments
              { $3.put_front ($1) ; $$ := $3 ; $$.line_number := $1.line_number }
  |  Formula COMMAKW Arguments
              { $3.put_front ($1) ; $$ := $3 ; $$.line_number := $1.line_number }
;

IntegerTerm: IntegerValue
              { create {QFIS_CONSTANT} $$.make ($1) ; $$.line_number := line_nb }
  |  INTEGERID
              { create {QFIS_INTEGER_VARIABLE} $$.make ($1) ; $$.line_number := line_nb }
  |  OLDKW INTEGERID 
     { create {QFIS_INTEGER_VARIABLE} $$.make ($2) ; $$.set_old ; $$.line_number := line_nb }
  |  Identifier OPENBRACKETKW Arguments CLOSEDBRACKETKW
              { create {QFIS_INTEGER_FUNCTION} $$.make ($1, $3) ; $$.line_number := $3.line_number }
  |  OPENPARENKW SequenceNT CLOSEDPARENKW OPENBRACKETKW IntegerValue CLOSEDBRACKETKW
              { create {QFIS_POSITION_INTEGER_FUNCTION} $$.make ($2, $5) ; $$.line_number := $2.line_number }
  |  LENGTHKW SequenceNT LENGTHKW
              { create {QFIS_LENGTH} $$.make ($2) ; $$.line_number := $2.line_number }
  |  ArithmeticCompound
              { $$ := $1 ; $$.line_number := $1.line_number }
;


ArithmeticCompound: IntegerTerm TIMESKW IntegerTerm
                     { create {QFIS_TIMES_COMPOUND} $$.make ($1, $3) ; $$.line_number := $1.line_number }
  | IntegerTerm DIVIDEKW IntegerTerm
                     { create {QFIS_DIVIDE_COMPOUND} $$.make ($1, $3) ; $$.line_number := $1.line_number }
  | IntegerTerm PLUSKW IntegerTerm
                     { create {QFIS_PLUS_COMPOUND} $$.make ($1, $3) ; $$.line_number := $1.line_number }
  | IntegerTerm MINUSKW IntegerTerm
                     { create {QFIS_MINUS_COMPOUND} $$.make ($1, $3) ; $$.line_number := $3.line_number }
;

%%

feature {NONE}  -- Initialization

	make  (filename: STRING) is
			-- Create a new parser and parse file with name `filename'
      do
			make_scanner (filename)
			make_parser_skeleton
			create error_messages.make
			parse
		end


	make_with_string  (str: STRING) is
			-- Create a new parser and parse string `str'
      do
			make_scanner_with_string (str)
			make_parser_skeleton
			create error_messages.make
			parse
		end
	
	
	
feature -- Access

   error_messages: LINKED_LIST [STRING]
			-- Error messages reported so far
	
	parsed_program: QFIS_PROGRAM

	parsed_formula: QFIS_FORMULA

	
feature {NONE}  -- Error reporting

   report_error (a_message: STRING) is
          -- Store error messages in error_messages
      do
         error_messages.extend ("Syntax error on line: " + line_nb.out)
		end

end
