#!/bin/bash

LEX="qfisPL.l"
GRAMMAR="qfisPL.y"

# generate token codes and parser class
geyacc -t qfis_pl_token_codes $GRAMMAR > qfis_pl_parser.e
# generate scanner class
gelex $LEX > qfis_pl_scanner.e

mv qfis_pl_scanner.e qfis_pl_parser.e qfis_pl_token_codes.e ../src/qfis_services/
